<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("Контакты");
?>

    <div class="b-content__infopage b-content__delivery" style="margin-top: 50px;">
        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/leftmenu.php')?>
        <div class="b-delivery-wrapper">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/tablinks.php')?>

            <div class="b-content__delivery-section" style="margin-left: 12px;">
                 <h1 class="b-delivery__header">Контакты</h1>
                <div class="b-delivery-wrapper"
                     style="border: 1px solid #e8e8e8;padding-top: 10px;margin-bottom: 100px;">
                    <div class="b-content__tablinks tabs-contact"
                         style="padding-left: 20px;padding-bottom: 10px;border-bottom: 1px solid #e8e8e8;border-radius:3px;">
                        <a href="#" class="b-content__currenttab"><span
                                style="font-size:15px;">Адрес магазина</span></a> <a
                            href="#"><span style="font-size:15px;">Адрес склада самовывоза</span></a>
                    </div>
                    <div class="b-content__delivery-cnt">
                        <div class="b-content__delivery-section visible-section" style="margin-top:30px;margin-left: 20px;margin-bottom: 40px;">
                            <div style="font-size: 15px;">
                                <h2 style="font-size:18px;">Адрес магазина в г. Москва.</h2>

                                <p style="margin-top: 6px;">
                                    <span style="color:#858585;font-family: Arial, Helvetica, sans-serif !important;">Адрес:</span>
                                    г. Москва
                                </p>
                            </div>

                            <h2 style="font-size:18px;margin-top: 26px;">Как добраться</h2>

                            <div style="padding-left: 25px;">
                                <a href="#" style="color:#8f90b6 !important;" class="link-hideblock">На метро -
                                    описание</a>
                            </div>
                        </div>

                        <div class="b-content__delivery-section" style="margin-top:30px;margin-left: 20px;margin-bottom: 40px;">
                            <div style="font-size: 15px;">
                                <h2 style="font-size:18px;">Адрес пункта самовывоза в г. Москва<a href="#" style="color: #666699;margin-left: 20px;border-bottom: 1px dotted #666699;margin-top: 13px;">Выбрать другой город</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        $( document).ready( function(){
            var $menu_elems = $( '.b-content__leftmenu-uplevel').children( 'li').find( 'h4' );
            $menu_elems.on( 'click', function(){
                $( this).closest( 'li' ).toggleClass( 'opened' );
            } );
            var $faq_elems = $( '.b-content__delivery-faq').find( 'li' );
            $faq_elems.on( 'click', function(){
                $( this ).toggleClass( 'active' );
            } );

            $('.link-hideblock').click(function(){
                if ($(this).hasClass('open')){
                    $(this).removeClass('open');
                    $(this).css('color','#8f90b6')
                }
                else{
                    $(this).addClass('open');
                    $(this).css('color','#303030')
                }
                return false;
            });

        } );
        $('.tabs-contact a').click(function(){
            if (!$(this).hasClass('b-content__currenttab')){
                $('.tabs-contact a.b-content__currenttab').removeClass('b-content__currenttab');
                $(this).addClass('b-content__currenttab');
                $('.b-content__delivery-cnt .b-content__delivery-section.visible-section').removeClass("visible-section");
                $('.b-content__delivery-cnt .b-content__delivery-section').eq($(this).index()).addClass("visible-section");
            }
            return false;
        });

    </script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>