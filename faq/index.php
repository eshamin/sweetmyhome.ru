<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("Часто задаваемые вопросы по работе интернет магазина");
?>
<div class="content-wrapper">
    <div class="b-content__help">
        <h1>Помощь</h1>
        <p>Здесь исчезают вопросы и появляются ответы</p>
        <ul>
            <li>
                <ul>
                    <li>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/help_icon_1.png" alt=""/>
                        <h2>Оформление заказа</h2>
                        <a href="#">Как оформить заказ?</a>
                        <a href="#">Как заказать кухню?</a>
                        <a href="#">Как удобно искать товары?</a>
                        <a href="#">Что с моим заказом?</a>
                        <a href="#">Телепортатор?</a>
                        <a href="#">Заказ в 1 клик</a>
                        <a href="#">Популярные вопросы</a>
                    </li>
                    <li>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/help_icon_2.png" alt=""/>
                        <h2>Оплата</h2>
                        <a href="#">Способы оплаты</a>
                        <a href="#">Кредит</a>
                        <a href="#">Накопленные баллы</a>
                    </li>
                    <li>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/help_icon_3.png" alt=""/>
                        <h2>Доставка и услуги</h2>
                        <a href="#">Доставка по всей России</a>
                        <a href="#">Самовывоз</a>
                        <a href="#">Сборка</a>
                        <a href="#">Дополнительные услуги</a>
                        <a href="#">Подъем</a>
                    </li>
                    <li class="clear"></li>
                </ul>
                <div class="clear"></div>
            </li>
            <li>
                <ul>
                    <li>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/help_icon_4.png" alt=""/>
                        <h2>Гарантии и Возвраты</h2>
                        <a href="#">А если брак?</a>
                        <a href="#">Гарантии</a>
                        <a href="#">Возврат и обмен товара?</a>
                    </li>
                    <li>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/help_icon_5.png" alt=""/>
                        <h2>Акции и предложения</h2>
                        <a href="#">Акции</a>
                        <a href="#">Дисконтные карты</a>
                        <a href="#">Уцененные товары</a>
                        <a href="#">Промокоды</a>
                        <a href="#">Подписка</a>
                    </li>
                    <li>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/help_icon_6.png" alt=""/>
                        <h2>О нас</h2>
                        <a href="#">Адреса и контакты</a>
                        <a href="#">О фабрике</a>
                        <a href="#">Статьи</a>
                        <a href="#">Видео о нас</a>
                        <a href="#">О магазине</a>
                        <a href="#">Обратная связь</a>
                    </li>
                    <li class="clear"></li>
                </ul>
                <div class="clear"></div>
            </li>
            <li>
                <ul>
                    <li>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/help_icon_7.png" alt=""/>
                        <h2>Дилерам и партнерам</h2>
                        <a href="#">Хотите стать дилером?</a>
                        <a href="#">Хотите предложить продукцию?</a>
                        <a href="#">Хотите предложить услуги?</a>
                    </li>
                    <li>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/help_icon_8.png" alt=""/>
                        <h2>Юр. лицам и ИП</h2>
                        <a href="#">Как заказать товар?</a>
                        <a href="#">Как оплатить товар?</a>
                        <a href="#">Как получить товар?</a>
                    </li>
                    <li class="clear"></li>
                </ul>
                <div class="clear"></div>
            </li>
            <li class="clear"></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>