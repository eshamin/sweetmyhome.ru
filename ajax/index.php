<?
define("STOP_STATISTICS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("sale") || !CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")/* || !CModule::IncludeModule("subscribe")*/) {
    return;
}

switch ($_POST['action']) {
    case 'add2basket':
        if (!$_POST['quantity']) {
            $_POST['quantity'] = 1;
        }
        $added = false;
        if (is_array($_POST['itemId'])) {
            foreach ($_POST['itemId'] as $itemId) {
                $itemId = intval($itemId['id']);
                if (!Add2BasketByProductID(intval($itemId), intval($_POST['quantity']))) {
                    $added = false;
                }
            }
            $added = true;
        } else {
            $added = Add2BasketByProductID(intval($_POST['itemId']), intval($_POST['quantity']));
        }
        if ($added) {
            $arRes['CODE'] = 'SUCCESS';
            $resBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                ),
                false,
                false,
                array()
            );

            $totalQuantity = 0;
            $totalPrice = 0;
            while ($basketItem = $resBasketItems->GetNext()) {
                if ($basketItem['CAN_BUY'] == 'Y') {
                    //$arFields['PROPS'] = array();
                    $totalQuantity += $basketItem['QUANTITY'];
                    $totalPrice += $basketItem['QUANTITY'] * $basketItem['PRICE'];
                    if (intval($_POST['itemId']) == $basketItem['PRODUCT_ID']) {
                        $arProduct = CIBlockElement::GetByID($basketItem['PRODUCT_ID'])->Fetch();
                        $arFileTmp = CFile::ResizeImageGet(
                            CFile::GetFileArray($arProduct['DETAIL_PICTURE']),
                            array("width" => 62, "height" => 50),
                            BX_RESIZE_IMAGE_EXACT,
                            true,
                            false
                        );
                        $arRes['ITEM_PRICE'] = FormatCurrency($basketItem['QUANTITY'] * $basketItem['PRICE'], 'RUB');
                        $arRes['ITEM_QUANTITY'] = intval($basketItem['QUANTITY']);
                        $arRes['ITEM_NAME'] = $basketItem['NAME'];
                        $arRes['ITEM_DISCOUNT_PRICE'] = FormatCurrency($basketItem['PRICE'], 'RUB');
                        $arRes['ITEM_IMG'] = "<img src='{$arFileTmp['src']}' />";
                    }
                    /*$product = CIBlockElement::GetList(array(), array('ID' => $basketItem['PRODUCT_ID']), false, false, array('PROPERTY_ARTICLE'))->Fetch();
                    if ($product) {
                        $arFields['PROPS'][] = array('NAME' => 'Артикул', 'CODE' => 'ARTICLE', 'VALUE' => $product['PROPERTY_ARTICLE_VALUE']);
                        CSaleBasket::Update($basketItem['ID'], $arFields);
                    }*/
                } else {
                    CSaleBasket::Delete($basketItem['ID']);
                }
            }
            $arRes['QUANTITY'] = $totalQuantity;
            $arRes['PRICE'] = FormatCurrency($totalPrice, 'RUB');
        } else {
            $arRes['CODE'] = 'FAIL';
            $arRes['MESSAGE'] = 'Продукт не добавлен в корзину';
        }
        break;
    case 'updateBasket':
        if (CSaleBasket::Update(intval($_POST['itemId']), array('QUANTITY' => intval($_POST['quantity'])))) {
            $arRes['CODE'] = 'SUCCESS';
            $resBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                ),
                false,
                false,
                array()
            );

            $totalQuantity = 0;
            $totalPrice = 0;
            while ($basketItem = $resBasketItems->GetNext()) {
                if ($basketItem['CAN_BUY'] == 'Y') {
                    $arFields['PROPS'] = array();
                    $totalQuantity += $basketItem['QUANTITY'];
                    $totalPrice += $basketItem['QUANTITY'] * $basketItem['PRICE'];
                    if (intval($_POST['itemId']) == $basketItem['ID']) {
                        $arRes['ITEM_PRICE'] = FormatCurrency($basketItem['QUANTITY'] * $basketItem['PRICE'], 'RUB');
                    }
                    /*$product = CIBlockElement::GetList(array(), array('ID' => $basketItem['PRODUCT_ID']), false, false, array('PROPERTY_ARTICLE'))->Fetch();
                    if ($product) {
                        $arFields['PROPS'][] = array('NAME' => 'Артикул', 'CODE' => 'ARTICLE', 'VALUE' => $product['PROPERTY_ARTICLE_VALUE']);
                        CSaleBasket::Update($basketItem['ID'], $arFields);
                    }*/
                } else {
                    CSaleBasket::Delete($basketItem['ID']);
                }
            }
            $arRes['QUANTITY'] = $totalQuantity;
            $arRes['PRICE'] = FormatCurrency($totalPrice, 'RUB');
        } else {
            $arRes['CODE'] = 'FAIL';
            $arRes['MESSAGE'] = 'Продукт не добавлен в корзину';
        }
        break;
    case 'deleteFromBasket':
        if (CSaleBasket::Delete(intval($_POST['itemId']))) {
            $arRes['CODE'] = 'SUCCESS';
            $resBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                ),
                false,
                false,
                array()
            );

            $totalQuantity = 0;
            $totalPrice = 0;
            while ($basketItem = $resBasketItems->GetNext()) {
                $totalQuantity += $basketItem['QUANTITY'];
                $totalPrice += $basketItem['QUANTITY'] * $basketItem['PRICE'];
            }
            $arRes['QUANTITY'] = $totalQuantity;
            $arRes['PRICE'] = FormatCurrency($totalPrice, 'RUB');
        } else {
            $arRes['CODE'] = 'FAIL';
            $arRes['MESSAGE'] = 'Не удалось удалить товар из корзины';
        }
        break;
    case 'getProducts':
        $arFilter = unserialize($_POST['filter']);
        $arFilter['IBLOCK_ID'] = CATALOG_IB;
        $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
        $GLOBALS['arFilter'] = $arFilter;
        $GLOBALS['arrFilter'] = $arFilter;

        ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "getProductsList",
            array(
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "Y",
                "ADD_TO_BASKET_ACTION" => "ADD",
                "SHOW_CLOSE_POPUP" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_COMPARE" => "Сравнить",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => CATALOG_IB,
                "SECTION_ID" => intval($_POST['sectionId']),
                "SECTION_CODE" => "",
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "ELEMENT_SORT_FIELD" => "sort",
                "ELEMENT_SORT_ORDER" => "desc",
                "ELEMENT_SORT_FIELD2" => "id",
                "ELEMENT_SORT_ORDER2" => "desc",
                "FILTER_NAME" => "arFilter",
                "INCLUDE_SUBSECTIONS" => "Y",
                "SHOW_ALL_WO_SECTION" => "Y",
                "SECTION_URL" => "/catalog/#SECTION_CODE#/",
                "DETAIL_URL" => "/catalog/#SECTION_CODE#/#ELEMENT_CODE#/",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "BROWSER_TITLE" => "-",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "-",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "Y",
                "SET_STATUS_404" => "N",
                "PAGE_ELEMENT_COUNT" => "30",
                "LINE_ELEMENT_COUNT" => "4",
                "PROPERTY_CODE" => array("MODUL", "SER", "WEIGHT", "SALES", "SIZE1", "SIZE2", "SIZE3", "COLORTEXT", "DOPS", "MATERIAL", "COMMENT", "SLEEPSIZE", "LEADER", "SERIATWO", "RELATED_ELEMENTS", "MORE_PHOTO", "SIZE", "STOCK"),
                "OFFERS_LIMIT" => "5",
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "BASKET_URL" => "/personal/cart/",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => array(
                ),
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "HIDE_NOT_AVAILABLE" => "N",
                "CONVERT_CURRENCY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );
        $arRes['PRODUCTS_LIST'] = ob_get_contents();
        ob_end_flush();

        ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "getProductsCard",
            array(
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "Y",
                "ADD_TO_BASKET_ACTION" => "ADD",
                "SHOW_CLOSE_POPUP" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_COMPARE" => "Сравнить",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => CATALOG_IB,
                "SECTION_ID" => intval($_POST['sectionId']),
                "SECTION_CODE" => "",
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "ELEMENT_SORT_FIELD" => "sort",
                "ELEMENT_SORT_ORDER" => "desc",
                "ELEMENT_SORT_FIELD2" => "id",
                "ELEMENT_SORT_ORDER2" => "desc",
                "FILTER_NAME" => "arrFilter",
                "INCLUDE_SUBSECTIONS" => "Y",
                "SHOW_ALL_WO_SECTION" => "Y",
                "SECTION_URL" => "/catalog/#SECTION_CODE#/",
                "DETAIL_URL" => "/catalog/#SECTION_CODE#/#ELEMENT_CODE#/",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "BROWSER_TITLE" => "-",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "-",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "Y",
                "SET_STATUS_404" => "N",
                "PAGE_ELEMENT_COUNT" => "30",
                "LINE_ELEMENT_COUNT" => "4",
                "PROPERTY_CODE" => array("MODUL", "SER", "WEIGHT", "SALES", "SIZE1", "SIZE2", "SIZE3", "COLORTEXT", "DOPS", "MATERIAL", "COMMENT", "SLEEPSIZE", "LEADER", "SERIATWO", "RELATED_ELEMENTS", "MORE_PHOTO", "SIZE", "STOCK"),
                "OFFERS_LIMIT" => "5",
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "BASKET_URL" => "/personal/cart/",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => array(
                ),
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "HIDE_NOT_AVAILABLE" => "N",
                "CONVERT_CURRENCY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );
        $arRes['PRODUCTS_CARD'] = ob_get_contents();
        ob_end_flush();

        break;
    case 'subscribe':
        $arFields = Array(
            "USER_ID" => ($USER->IsAuthorized() ? $USER->GetID() : false),
            "FORMAT" => "html",
            "EMAIL" => $_POST['email'],
            "ACTIVE" => "Y",
            "RUB_ID" => array(2, 3)
        );
        $subscr = new CSubscription;

        $ID = $subscr->Add($arFields);
        if ($ID > 0) {
            CSubscription::Authorize($ID);
            $arRes['CODE'] = 'SUCCESS';
        } else {
            $arRes['CODE'] = 'FAIL';
            $arRes['MESSAGE'] = "Error adding subscription: ".$subscr->LAST_ERROR."<br>";
        }
        break;
    case 'add2favorites':
        $arrItemID = array();

        if (!$USER->IsAuthorized()) {
            $arElements = unserialize($APPLICATION->get_cookie('favorites'));
            if (!in_array($_POST['itemId'], $arElements)) {
                $arElements[] = $_POST['itemId'];
            }
            $APPLICATION->set_cookie("favorites", serialize($arElements));
        } else {
            $idUser = $USER->GetID();
            $rsUser = CUser::GetByID($idUser);
            $arUser = $rsUser->Fetch();
            $arElements = unserialize($arUser['UF_FAVORITES']);
            if (!in_array($_POST['itemId'], $arElements)) {
                $arElements[] = $_POST['itemId'];
            }
            $USER->Update($idUser, Array("UF_FAVORITES" => serialize($arElements)));
        }

        $arRes['CODE'] = 'SUCCESS';
        $arRes['QUANTITY'] = count($arElements);
        break;
    case 'getCity':
        $cityName = $_POST['cityName'];

        if (isset($_SESSION['LOCATION_ID']) || isset($_COOKIE['LOCATION_ID'])) {
            $cityId = ($_SESSION['LOCATION_ID'] >0 ) ? $_SESSION['LOCATION_ID'] : $_COOKIE['LOCATION_ID'];
            $arRes['cityName'] = CSaleLocation::GetList(array(), array('ID' => $_SESSION['LOCATION_ID']))->Fetch();
            $arRes['cityName'] = $arRes['cityName']['CITY_NAME'];
        } else {
            $arCity = CSaleLocation::GetList(array(), array('%CITY_NAME%' => $cityName))->Fetch();
            $arRes['cityName'] = $arCity['NAME'];
            $cityId = $arCity['ID'];
            setcookie('LOCATION_ID', $cityId, time() + 365 * 24 * 3600);
            $_SESSION['LOCATION_ID'] = $cityId;
            $arRes['SHOW_LOCATION'] = true;
        }

        $arRes['CODE'] = 'SUCCESS';
        break;
    case 'getCities':
        $letter = $_POST['letter'];

        $resCity = CSaleLocation::GetList(array(), array('COUNTRY_LID' => 'ru', 'REGION_LID' => 'ru', 'CITY_LID' => 'ru', 'CITY_NAME%' => $letter));
        $arRes['CITIES'] = '';
        while ($arCity = $resCity->Fetch()) {
            if (strlen($letter) == 1 && strtoupper(substr($arCity['CITY_NAME'], 0, 1)) == strtoupper($letter) ||
                strlen($letter) > 1 && stristr($arCity['CITY_NAME'], $letter)) {
                $arRes['CITIES'] .= "<li class='g-left'><a href='?location={$arCity['ID']}'>{$arCity['CITY_NAME']}</a></li>\n";
            }
        }

        $arRes['CODE'] = 'SUCCESS';
        break;
    case 'addFastOrder':
        $dbBasketItems = CSaleBasket::GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
            array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
            false,
            false,
            array()
        );

        while ($arBasketItem = $dbBasketItems->Fetch()) {
            if ($arBasketItem['CAN_BUY'] != 'N' || $arBasketItem['DELAY'] != 'Y') {
                $totalPrice += $arBasketItem['QUANTITY'] * $arBasketItem['PRICE'];
            }
        }

        $user = CUser::GetByLogin('fastorder')->Fetch();
        $arFields = array(
            "LID" => SITE_ID,
            "PERSON_TYPE_ID" => 1,
            "PAYED" => "N",
            "CANCELED" => "N",
            "STATUS_ID" => "N",
            "PRICE" => $totalPrice,
            "CURRENCY" => "RUB",
            'USER_ID' => $user['ID'],
            'PAY_SYSTEM_ID' => 1,
            'DELIVERY_ID' => 4
        );

        if ($order = CSaleOrder::Add($arFields)) {
            CSaleBasket::OrderBasket($order);

            $arFieldsProp = array(
                "ORDER_ID" => $order,
                "ORDER_PROPS_ID" => 1,
                "NAME" => "Как вас зовут?",
                "VALUE" => $_POST['name']
            );
            CSaleOrderPropsValue::Add($arFieldsProp);

            $arFieldsProp = array(
                "ORDER_ID" => $order,
                "ORDER_PROPS_ID" => 3,
                "NAME" => "Телефон",
                "VALUE" => $_POST['mobile']
            );
            CSaleOrderPropsValue::Add($arFieldsProp);

            $_SESSION['ORDER_ID'] = $order;
            $arRes['CODE'] = 'SUCCESS';
            $arRes['MESSAGE'] = $order;
        } else {
            $arRes['CODE'] = 'FAIL';
            $arRes['MESSAGE'] = "Произошла ошибка";
        }
        break;
    case 'addOneClickOrder':
        $user = CUser::GetByLogin('fastorder')->Fetch();

        $product_id = intval($_POST['productId']);
        $arProduct = CCatalogProduct::GetByIDEx($product_id);

        $arFields = array(
            "LID" => SITE_ID,
            "PERSON_TYPE_ID" => 1,
            "PAYED" => "N",
            "CANCELED" => "N",
            "STATUS_ID" => "N",
            "PRICE" => $arProduct['PRICES'][1]['PRICE'],
            "CURRENCY" => "RUB",
            'USER_ID' => $user['ID'],
            'PAY_SYSTEM_ID' => 1,
            'DELIVERY_ID' => 4
        );

        if ($order = CSaleOrder::Add($arFields)) {
            $arProps = array();
            $arFields = array(
                "PRODUCT_ID" => $arProduct['ID'],
                "PRICE" => $arProduct['PRICES'][1]['PRICE'],
                "CURRENCY" => "RUB",
                "WEIGHT" => $arProduct['PRODUCT']['WEIGHT'],
                "QUANTITY" => 1,
                "DELAY" => "N",
                "LID" => $arProduct['LID'],
                "CAN_BUY" => "Y",
                "ORDER_ID" => $order,
                "NAME" => $arProduct['NAME'],
                "MODULE" => "catalog",
                "NOTES" => "",
                "DETAIL_PAGE_URL" => $arProduct['DETAIL_PAGE_URL'],
            );
            CSaleBasket::Add($arFields);

            $arFieldsProp = array(
                "ORDER_ID" => $order,
                "ORDER_PROPS_ID" => 3,
                "NAME" => "Телефон",
                "VALUE" => $_POST['mobile']
            );
            CSaleOrderPropsValue::Add($arFieldsProp);

            $_SESSION['ORDER_ID'] = $order;
            $arRes['CODE'] = 'SUCCESS';
            $arRes['ORDER_ID'] = $order;
        } else {
            $arRes['CODE'] = 'FAIL';
            $arRes['MESSAGE'] = "Произошла ошибка";
        }
        break;
    case 'addFeedback':
            $arProps['PLUS'] = htmlspecialchars($_POST['positive']);
            $arProps['MINUS'] = htmlspecialchars($_POST['negative']);
            $arProps['COMMENT'] = htmlspecialchars($_POST['comments']);
            $arProps['FIO'] = htmlspecialchars($_POST['username']);
            $arProps['EMAIL'] = htmlspecialchars($_POST['usermail']);
            $arProps['PRODUCT_ID'] = htmlspecialchars($_POST['productId']);
            $arProps['RATING'] = htmlspecialchars($_POST['rating']);

            if ($USER->IsAuthorized()) {
                $arProps['USER_ID'] = $USER->GetID();
            }

            $arProps['COMMENT'] = htmlspecialchars($_POST['comments']);
            $arFields['NAME'] = htmlspecialchars($_POST['productName']);
            $arFields['ACTIVE'] = 'N';
            $arFields['PROPERTY_VALUES'] = $arProps;
            $arFields['IBLOCK_ID'] = 8;

            $el = new CIBlockElement;
            if ($el->Add($arFields)) {
                $arRes['FEEDBACK_ADDED'] = 'true';
                $arRes['CODE'] = 'SUCCESS';
            } else {
                $arRes['FEEDBACK_ADDED'] = 'false';
                $arRes['CODE'] = 'FAIL';
                $arRes['MESSAGE'] = $el->LAST_ERROR;
            }

            $product = CIBlockElement::GetList(array(), array('ID' => $arProps['PRODUCT_ID'], 'IBLOCK_ID' => 5), false, false, array('ID', 'PROPERTY_vote_sum', 'PROPERTY_vote_count', 'PROPERTY_exist_comment'))->Fetch();
            if ($_POST['rating'] > 0) {
                $vote_sum = $product['PROPERTY_VOTE_SUM_VALUE'] + $_POST['rating'];
                $vote_count = $product['PROPERTY_VOTE_COUNT_VALUE'] + 1;
                $rating = round($vote_sum / $vote_count, 2);

                CIBlockElement::SetPropertyValues($arProps['PRODUCT_ID'], 5, $vote_sum, 'vote_sum');
                CIBlockElement::SetPropertyValues($arProps['PRODUCT_ID'], 5, $vote_count, 'vote_count');
                CIBlockElement::SetPropertyValues($arProps['PRODUCT_ID'], 5, $rating, 'rating');
            }

            $exist_comment = ((strlen($_POST['positive']) > 0 || strlen($_POST['negative']) > 0 || strlen($_POST['comments']) > 0) && !$product['PROPERTY_EXIST_COMMENT_VALUE']) ? 754 : '';
            CIBlockElement::SetPropertyValues($arProps['PRODUCT_ID'], 5, $exist_comment, 'exist_comment');
        break;
    default:
        $arRes['CODE'] = 'FAIL';
        $arRes['MESSAGE'] = 'Неизвестное действие';
}

$APPLICATION->RestartBuffer();
header('Content-Type: application/json; charset='.LANG_CHARSET);
echo CUtil::PhpToJSObject($arRes);
die();
?>