<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Информация об оплате");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("Информация об оплате");
?>
    <div class="b-content__infopage b-content__payway b-content__delivery">
    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/leftmenu.php')?>
        <div class="b-payway">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/tablinks.php')?>
            <h1 class="b-delivery__header">Способы оплаты</h1>

            <p>
                После оформления заказа с вами свяжется наш менеджер и уточнит состав заказа и удобное время доставки.<br/>
                Помните, что в любой момент вы можете отказаться от заказа после оплаты
            </p>
            <ul class="b-payway__items">
                <li class="b-payway__item">
                    <div class="g-left b-payway__img">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway1.png" alt=""/>
                    </div>
                    <div class="g-left b-payway__info">
                        <h2 class="b-payway__info-header">Наличными курьеру</h2>
                        <p>
                            Оплата заказа производится курьеру в момент доставки после проверки вами комплектности
                            товара. И когда вы убедитесь в соответствующем качестве, вам будет предложено расписаться в
                            накладной за прием товара и расплатиться наличными. После оформления заказа с вами свяжется
                            наш менеджер и уточнит состав заказа и удобное время доставки.
                        </p>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="b-payway__item">
                    <div class="g-left b-payway__img">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway2.png" alt=""/>
                    </div>
                    <div class="g-right b-payway__info">
                        <h2 class="b-payway__info-header">Банковские карты <a class="b-payway__info-header--link" href="#">Как заплатить</a></h2>
                        <div class="b-paysys">
                            <ul>
                                <li class="b-paysys__item">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pay-visa.png" alt=""/>
                                    &nbsp;Visa, Visa Electron
                                </li>
                                <li class="b-paysys__item">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pay-master.png" alt=""/>
                                    &nbsp;Master Card
                                </li>
                                <li class="b-paysys__item">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pay-maestro.png" alt=""/>
                                    &nbsp;Maestro
                                </li>
                            </ul>
                        </div>
                        <div class="b-payway__comission">
                            Комиссия при оплате - <span>0%</span> • Комиссия в случае возврата - <span>0%</span>
                        </div>
                        <p>Для оплаты заказа банковской картой: Visa, MasterCard или Maestro вам необходимо добавить в
                            корзину понравившиеся товары, затем перейти в корзину, ввести контактные данные и выбрать
                            способ оплаты "Банковские карты", затем подтвердить заказ. После подтверждения заказа вы
                            увидите страницу с подробными данными о вашем заказе и кнопкой "Оплатить банковской картой".
                            Далее вам необходимо ввести реквизиты банковской карты и подтвердить оплату</p>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="b-payway__item">
                    <div class="g-left b-payway__img">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway3.png" alt=""/>
                    </div>
                    <div class="g-right b-payway__info">
                        <h2 class="b-payway__info-header">Электронные деньги <a class="b-payway__info-header--link" href="#">Как заплатить</a></h2>
                        <div class="b-paysys">
                            <ul>
                                <li class="b-paysys__item">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pay-yamoney.png" alt=""/>
                                    &nbsp;Яндекс.Деньги
                                </li>
                                <li class="b-paysys__item">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pay-webmoney.png" alt=""/>
                                    &nbsp;WebMoney
                                </li>
                            </ul>
                        </div>
                        <div class="b-payway__comission">
                            Комиссия при оплате - <span>0%</span> • Комиссия в случае возврата - <span>0%</span>
                        </div>
                        <p>
                            Для оплаты заказа электронными деньгами: Яндекс деньги или WebMoney (WMR) вам необходимо
                            добавить в корзину понравившиеся товары, затем перейти в корзину, ввести контактные данные
                            и выбрать способ оплаты "Электронные деньги", затем подтвердить заказ.
                            После подтверждения заказа вы увидите страницу с подробными данными о вашем заказе и кнопкой
                            "Оплатить электронные деньги".
                            Далее вы будете перенаправлены в личный кабинет Яндекс деньги или WebMoney, где вы сможете
                            оплатить ваш заказ.
                        </p>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="b-payway__item">
                    <div class="g-left b-payway__img">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway4.png" alt=""/>
                    </div>
                    <div class="g-right b-payway__info">
                        <h2 class="b-payway__info-header">Терминалы <a class="b-payway__info-header--link" href="#">Как заплатить</a></h2>
                        <div class="b-content__payway-morethan">
                            <p>Более <a href="#">170 тысяч пунктов</a> оплаты по всей России</p>
                        </div>
                        <div class="b-payway__comission">
                            Комиссия при оплате - <span>0% и более в зависимости от пункта оплаты</span> • Комиссия в
                            случае возврата - <span>0%</span>
                        </div>
                        <p>
                            Для оплаты заказа электронными деньгами: Яндекс деньги или WebMoney (WMR) вам необходимо
                            добавить в корзину понравившиеся товары, затем перейти в корзину, ввести контактные данные
                            и выбрать способ оплаты "Электронные деньги", затем подтвердить заказ.
                            После подтверждения заказа вы увидите страницу с подробными данными о вашем заказе и кнопкой
                            "Оплатить электронные деньги".
                            Далее вы будете перенаправлены в личный кабинет Яндекс деньги или WebMoney, где вы сможете
                            оплатить ваш заказ.
                        </p>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="b-payway__item">
                    <div class="g-left b-payway__img">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway5.png" alt=""/>
                    </div>
                    <div class="g-right b-payway__info">
                        <h2 class="b-payway__info-header">Баланс телефона <a class="b-payway__info-header--link" href="#">Как заплатить</a></h2>
                        <div class="b-paysys">
                            <ul>
                                <li class="b-paysys__item">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pay-beeline.png" alt=""/>
                                    &nbsp;Билайн
                                </li>
                                <li class="b-paysys__item">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pay-megafon.png" alt=""/>
                                    &nbsp;Мегафон
                                </li>
                                <li class="b-paysys__item">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pay-mts.png" alt=""/>
                                    &nbsp;МТС
                                </li>
                            </ul>
                        </div>
                        <div class="b-payway__comission">
                            Комиссия при оплате - <span>0%</span> • Комиссия в случае возврата - <span>0%</span>
                        </div>
                        <p>
                            Для оплаты заказа с помощью баланса телефона операторами: Мегафон, МТС, Билайн вам необходимо
                            добавить в корзину понравившиеся товары, затем перейти в корзину, ввести контактные данные
                            и выбрать способ оплаты "Баланс телефона", затем подтвердить заказ.
                            После подтверждения заказа вы увидите страницу с подробными данными о вашем заказе и кнопкой
                            "Оплатить с Баланса телефона".
                            Далее вы будете проинструктированы как оплатить ваш заказ.<br/>
                            После оформления заказа с вами свяжется наш менеджер и уточнит состав заказа и удобное время
                            доставки. Помните, что в любой момент вы можете отказаться от заказа после оплаты. По вашей
                            просьбе мы вернем вам оплаченную сумму в полном объеме без комиссий.
                        </p>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="b-payway__item">
                    <div class="g-left b-payway__img">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway6.png" alt=""/>
                    </div>
                    <div class="g-right b-payway__info">
                        <h2 class="b-payway__info-header">Банковский перевод на реквизиты</h2>
                        <div class="b-payway__comission">
                            Комиссия при оплате - <span>от 1 до 3% в зависимости от вашего банка</span> • Комиссия в
                            случае возврата - <span>0%</span>
                        </div>
                        <p>
                            Для оплаты заказа через банк с помощью банковского перевода на реквизиты вам необходимо
                            добавить в корзину понравившиеся товары, затем перейти в корзину, ввести контактные данные
                            и выбрать способ оплаты "Банковский перевод на реквизиты", затем подтвердить заказ. После
                            подтверждения заказа вы увидите страницу с подробными данными о вашем заказе и кнопкой
                            "Оплатить через Банковский перевод ". При нажатии на кнопку появятся реквизиты для оплаты
                            заказа в банке. Оплатить данный счет вы сможете как физическое лицо в любом банке.<br/>
                            Зачисление денежных средств обычно происходит в срок от 1 до 3-х рабочих дней. После
                            зачисления денежных средств мы отправим заказ выбранным вами способом.
                        </p>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="b-payway__item">
                    <div class="g-left b-payway__img">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway7.png" alt=""/>
                    </div>
                    <div class="g-right b-payway__info">
                        <h2 class="b-payway__info-header">Безналичный расчет</h2>
                        <div class="b-payway__comission">
                            Комиссия при оплате - <span>от 1 до 3% в зависимости от вашего банка</span> • Комиссия в
                            случае возврата - <span>0%</span>
                        </div>
                        <p>
                            Комиссий при данном способе оплаты не взимается. Данный способ оплаты здя оплаты заказов
                            юридическими лицами и индивидуальными предпринимателями. Для оплаты заказа безналичным
                            расчетом по выставленному счету вам необходимо добавить в корзину понравившиеся товары,
                            затем перейти в корзину, ввести контактные данные и выбрать способ оплаты "Безналичный расчет",
                            затем подтвердить заказ. После подтверждения заказа вы увидите страницу для ввода реквизитов
                            вашей организации для выставления счета.<br/> После оформления заказа с вами свяжется наш
                            менеджер и уточнит состав заказа и удобное время доставки.
                        </p>
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>