<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Информация о доставке");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("Информация о доставке");
?>
    <div class="b-content__infopage b-content__delivery">
        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/leftmenu.php')?>

        <div class="b-delivery-wrapper">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/tablinks.php')?>

            <p>Мы осуществляем доставку в более 8 000 населенных пунктов по всей России удобным способом на ваш выбор
                при оформлении заказа</p>
            <p>
                <span style="color: #ee105a;">Доставка в пределах МКАД БЕСПЛАТНО*</span>
            </p>
            <p>
                <span style="font-size: 8pt;">* при заказе от 39&nbsp;990&nbsp;руб. При заказе до 39&nbsp;990&nbsp;стоимость доставки в пределах МКАД 590 руб. Доставка за МКАД оплачивается 20 руб/за 1км до 30 км, далее 25 руб/за 1км свыше 30 км от МКАД&nbsp;до вашего дома.</span>
            </p>

            <div class="b-delivery g-nomargin">
                <h1 class="b-delivery__header">Заказать доставку очень легко</h1>

                <div class="b-delivery-easy">
                    <ul>
                        <li class="b-delivery-easy__item">
                            <div class="b-delivery-easy__item-div">
                                <p>Выберите любой понравившийся товар и оформите его покупку</p>
                                <span>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-easy-1.png" alt=""/>
                                </span>
                            </div>
                        </li>
                        <li class="b-delivery-easy__item">
                            <div class="b-delivery-easy__item-div">
                                <p>Мы автоматически предложим вам подходящий вид доставки</p>
                                <span>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-easy-2.png" alt=""/>
                                </span>
                            </div>
                        </li>
                        <li class="b-delivery-easy__item">
                            <div class="b-delivery-easy__item-div">
                                <p>Введите ваш адрес и выберите удобные дату и время</p>
                                <span>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-easy-3.png" alt=""/>
                                </span>
                            </div>
                        </li>
                        <li class="b-delivery-easy__item">
                            <div class="b-delivery-easy__item-div">
                                <p>С вами свяжется наш специалист и подтвердит время доставки</p>
                                <span>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-easy-4.png" alt=""/>
                                </span>
                            </div>
                        </li>
                        <li class="b-delivery-easy__item">
                            <div class="b-delivery-easy__item-div">
                                <p>Получите товар у нашего курьера в назначенное время</p>
                                <span>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-easy-5.png" alt=""/>
                                </span>
                            </div>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="b-delivery">
                <h2 class="b-delivery__header">Способы доставки</h2>

                <div class="b-content__delivery-way">
                    <table>
                        <thead>
                        <tr>
                            <td><h3>Способ доставки</h3></td>
                            <td>
                                <h3>Наши курьеры</h3>
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-way-1.png" alt=""/>

                                <p><a href="#">Подробнее</a></p>
                            </td>
                            <td>
                                <h3>Курьеры служб</h3>
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-way-2.png" alt=""/>

                                <p><a href="#">Подробнее</a></p>
                            </td>
                            <td>
                                <h3>Постамат</h3>
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-way-3.png" alt=""/>

                                <p><a href="#">Подробнее</a></p>
                            </td>
                            <td>
                                <h3>Пункт выдачи</h3>
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-way-4.png" alt=""/>

                                <p><a href="#">Подробнее</a></p>
                            </td>
                            <td>
                                <h3>Транспортная компания</h3>
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-way-5.png" alt=""/>

                                <p><a href="#">Подробнее</a></p>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><h3>Тарифы и сроки</h3></td>
                            <td><p>0 руб. до 0 дней<br/> Ежедневно<br/> с 10:00 до 23:59</p></td>
                            <td><p>350 руб. до 0 дней<br/> Ежедневно<br/> с 10:00 до 23:59</p></td>
                            <td><p>370 руб. от 1 до 0 дней<br/> Ежедневно<br/> с 10:00 до 23:59</p></td>
                            <td><p>380 руб. от 1 до 0 дней<br/> Ежедневно<br/> с 10:00 до 23:59</p></td>
                            <td><p>1030 руб. от 2 до 3 дней<br/> с ПН по ПТ<br/> с 09:00 до 18:00 </p></td>
                        </tr>
                        <tr>
                            <td><h3>Охват Доставка до двери Самовывоз</h3></td>
                            <td><p>Охват - Москва и область<br/> До двери - есть<br/> Самовывоз - есть </p></td>
                            <td><p>Охват - вся Россия<br/>До двери - есть<br/> Самовывоз - нет</p></td>
                            <td><p>Охват - вся Россия<br/>До двери - есть<br/> Самовывоз - нет</p></td>
                            <td><p>Охват - вся Россия<br/>До двери - есть<br/> Самовывоз - нет</p></td>
                            <td><p>Охват - вся Россия,<br/> кроме Москвы<br/>До двери - есть<br/> Самовывоз - нет</p>
                            </td>
                        </tr>
                        <tr>
                            <td><h3>Вес и объем груза</h3></td>
                            <td><p>Вес: без ограничения<br/>Объем: без ограничения<br/>В пределах Москвы и московской
                                    области</p></td>
                            <td><p>Вес: до 50 кг<br/>Объем: до 0,7 м3<br/>По всей России</p></td>
                            <td><p>Вес: до 50 кг<br/>Объем: до 0,7 м3<br/>По всей России</p></td>
                            <td><p>Вес: до 50 кг<br/>Объем: до 0,7 м3<br/>По всей России</p></td>
                            <td><p>Вес: без ограничения<br/>Объем: без ограничения<br/>По всей России кроме Москвы</p>
                            </td>
                        </tr>
                        <tr>
                            <td><h3>Способы оплаты</h3></td>
                            <td><p>Любой способ оплаты<br/><a href="#">Подробнее&nbsp;об&nbsp;оплате</a></p></td>
                            <td><p>Любой способ оплаты<br/><a href="#">Подробнее&nbsp;об&nbsp;оплате</a></p></td>
                            <td><p>Любой способ оплаты<br/><a href="#">Подробнее&nbsp;об&nbsp;оплате</a></p></td>
                            <td><p>Любой способ оплаты<br/><a href="#">Подробнее&nbsp;об&nbsp;оплате</a></p></td>
                            <td><p>Любой способ оплаты кроме наличными по факту доставки<br/><a href="#">Подробнее&nbsp;об&nbsp;оплате</a>
                                </p></td>
                        </tr>
                        <tr>
                            <td><h3>Подъем на этаж</h3></td>
                            <td><p>Услуга оказывается с лифтом или без лифта в пределах Москвы и московской области</p>
                            </td>
                            <td><p>Услуга оказывается с лифтом или без лифта по всей России</p></td>
                            <td><p>Самовывоз Услуга не оказывается</p></td>
                            <td><p>Самовывоз Услуга не оказывается</p></td>
                            <td><p>Услуга оказывается с лифтом или без лифта по всей России кроме Москвы</p></td>
                        </tr>
                        <tr>
                            <td><h3>Сборка</h3></td>
                            <td><p>Услуга оказывается в пределах Москвы и московской области <br/>Сборка мебели - 10% от
                                    стоимости<br/>Сборка кухонь - 12% от стоимости</p></td>
                            <td><p>Услуга оказывается в пределах Москвы и московской области <br/>Сборка мебели - 10% от
                                    стоимости<br/>Сборка кухонь - 12% от стоимости</p></td>
                            <td><p>Услуга оказывается в пределах Москвы и московской области <br/>Сборка мебели - 10% от
                                    стоимости<br/>Сборка кухонь - 12% от стоимости</p></td>
                            <td><p>Услуга оказывается в пределах Москвы и московской области <br/>Сборка мебели - 10% от
                                    стоимости<br/>Сборка кухонь - 12% от стоимости</p></td>
                            <td><p>Услуга не оказывается</p></td>
                        </tr>
                        <tr>
                            <td><h3>Доп. услуги</h3></td>
                            <td><p>В пределах Москвы и московской области возможен <a href="#">следующий перечень
                                        услуг</a></p></td>
                            <td><p>В пределах Москвы и московской области возможен <a href="#">следующий перечень
                                        услуг</a></p></td>
                            <td><p>В пределах Москвы и московской области возможен <a href="#">следующий перечень
                                        услуг</a></p></td>
                            <td><p>В пределах Москвы и московской области возможен <a href="#">следующий перечень
                                        услуг</a></p></td>
                            <td><p>В пределах Москвы и московской области возможен <a href="#">следующий перечень
                                        услуг</a></p></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="b-delivery">
                <h2 class="b-delivery__header">Преимущества</h2>

                <div class="b-delivery-payway">
                    <ul>
                        <li class="pb30 b-delivery-payway__item">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-adv-1.png" alt=""/>

                            <p>Доставка по Москве<br/> в день заказа</p>
                        </li>
                        <li class="pb30 b-delivery-payway__item">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-adv-2.png" alt=""/>

                            <p>Доставка в 8 000<br/> пунктов по России</p>
                        </li>
                        <li class="pb30 b-delivery-payway__item">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-adv-3.png" alt=""/>

                            <p>Привезем в удобное<br/> время</p>
                        </li>
                        <li class="b-delivery-payway__item">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-adv-4.png" alt=""/>

                            <p>Доставка 365 дней<br/> в году</p>
                        </li>
                        <li class="b-delivery-payway__item">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-adv-5.png" alt=""/>

                            <p>Более 40 служб<br/> доставки</p>
                        </li>
                        <li class="b-delivery-payway__item">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/static/del-adv-6.png" alt=""/>

                            <p>Собственный<br/> транспорт</p>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="b-delivery">
                <h2 class="b-delivery__header">Вопросы</h2>

                <div class="b-delivery-faq">
                    <ul>
                        <li class="b-delivery-faq__item">
                            <h3><span>Не могу разобраться с тарифами помогите?</span></h3>
                            <p>Закажите бесплатный звонок нашего специалиста прямо сейчас и наш менеджер проконсультирует по всем вопросам. +7 () -__-__</p>
                        </li>
                        <li class="b-delivery-faq__item">
                            <h3><span>В какое время привезут мебель если я живу в Москве?</span></h3>
                            <p>Закажите бесплатный звонок нашего специалиста прямо сейчас и наш менеджер проконсультирует по всем вопросам. +7 () -__-__</p>
                        </li>
                        <li class="b-delivery-faq__item">
                            <h3><span>Как мне рассчитать стоимость доставки если я живу за город?</span></h3>
                            <p>Закажите бесплатный звонок нашего специалиста прямо сейчас и наш менеджер проконсультирует по всем вопросам. +7 () -__-__</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var $menu_elems = $('.b-content__leftmenu-uplevel').children('li').find('h4');
            $menu_elems.on('click', function () {
                $(this).closest('li').toggleClass('opened');
            });
            var $faq_elems = $('.b-content__delivery-faq').find('li');
            $faq_elems.on('click', function () {
                $(this).toggleClass('active');
            });
        });
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>