<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Информация о возврате");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("Информация о возврате");
?>
    <div class="b-content__infopage b-content__delivery">
        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/leftmenu.php')?>
        <div class="b-delivery-wrapper">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/tablinks.php')?>

            <div class="b-content__delivery-section" style="padding-left: 12px;">
                <h1 class="b-delivery__header">Наши преимущества</h1>
                <div class="b-delivery">
                    <ul>
                        <li>
                            <div style="float: left;width: 20%;">
                                <span style="display: block;margin-left:43px;margin-top:32px;">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pct.gif" alt="">
                                </span>
                                <p style="margin-top: 15px;margin-left: 8px;font-weight:bold;">Весь товар официально сертифицирован</p>
                            </div>
                        </li>
                        <li>
                            <div style="float: left;width: 20%;">
                                <span style="display: block;margin-left:12px;margin-top:32px;">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/yandex.gif" alt="">
                                </span>
                                <p style="margin-top: 20px;margin-left: 2px;font-weight:bold;">Весь товар официальносертифицирован</p>
                            </div>
                        </li>
                        <li>
                            <div style="float: left;width: 20%;">
                                <span style="display: block;margin-left:28px;margin-top:30px;">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/garanty.gif" alt="">
                                </span>
                                <p style="margin-top: 19px;margin-left: -9px;font-weight:bold;">Мы не работаем с некачественным товаром</p>
                            </div>
                        </li>
                        <li>
                            <div style="float: left;width: 20%;">
                                <span style="display: block;margin-left:35px;margin-top:29px;">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/ofdiler.gif" alt="">
                                </span>
                                <p style="margin-top: 13px;margin-left: -2px;font-weight:bold;">Качество нашего сервиса подтверждено статусами </p>
                            </div>
                        </li>
                        <li>
                            <div style="float: left;width: 20%;">
                                <span style="display: block;margin-left:51px;margin-top:22px;">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/100.png" alt="">
                                </span>
                                <p style="margin-top: 10px;margin-left: 36px;font-weight:bold;">100% гарантия<br> возврата</p>
                            </div>
                        </li>
                    </ul>
                    <div class="g-clear"></div>
                </div>

                <div class="b-delivery">
                    <ul class="b-payway__items">
                        <li class="b-payway__item">
                            <div class="g-left b-payway__img">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/top2.gif" alt="">
                            </div>
                            <div class="g-right b-payway__info">
                                <h2 class="b-payway__info-header">Гарантия 2 года</h2>
                                <p>
                                    Гарантия на всю продукцию составляет 2 года.
                                </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li class="b-payway__item">
                            <div class="g-left b-payway__img">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/circle.gif" alt="">
                            </div>
                            <div class="g-right b-payway__info">
                                <h2 class="b-payway__info-header">Возврат или обмен</h2>
                                <p>
                                    Возврат или обмен товара возможен, если сохранена упаковка, товарный вид и потребительские
                                    свойства товара, а также товарный и кассовый чеки. В этом случае мы гарантируем вам
                                    возврат стоимости товара за вычетом стоимости доставки.
                                </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li class="b-payway__item">
                            <div class="g-left b-payway__img">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/find.png" alt="">
                            </div>
                            <div class="g-right b-payway__info">
                                <h2 class="b-payway__info-header">Обнаружение брака</h2>
                                <p>
                                    В случае обнаружения брака вам необходимо позвонить по телефону Начальнику отдела
                                    сборок и рекламаций, указанному в накладной. Затем вам необходимо передать необходимую
                                    информацию по бракованной детали или товару. И наша служба бесплатно в кратчайший срок
                                    заменит бракованные детали.
                                </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                    </ul>
                    <div class="g-clear"></div>
                </div>
            </div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>