<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>
<div class="content-wrapper b-basket-wrapper">
    <?if (!$_GET['ORDER_ID']) {?>
        <div class="b-makeorder__tablink">
            <a href="javascript:void(0)" class="b-makeorder__currenttab" onclick="selectOrder(this)"><h1>Оформление заказа</h1></a>
            <a class="fast" href="javascript:void(0)" onclick="selectOrder(this)"><h1>Быстрый заказ</h1><span> — от вас потребуется только имя и телефон для связи</span></a>
        </div>
        <div class="b-makeorder__tabs">
    <?} else {?>
        <div class="b-basket__line"></div>
    <?}?>
<?$APPLICATION->IncludeComponent(
    "sweetmyhome:sale.order.ajax",
    "createOrder",
    Array(
	    "PAY_FROM_ACCOUNT" => "N",	// Позволять оплачивать с внутреннего счета
		"COUNT_DELIVERY_TAX" => "N",	// Рассчитывать налог для доставки
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",	// Позволять оплачивать с внутреннего счета только в полном объеме
		"ALLOW_AUTO_REGISTER" => "Y",	// Оформлять заказ с автоматической регистрацией пользователя
		"SEND_NEW_USER_NOTIFY" => "Y",	// Отправлять пользователю письмо, что он зарегистрирован на сайте
		"DELIVERY_NO_AJAX" => "Y",	// Рассчитывать стоимость доставки сразу
		"TEMPLATE_LOCATION" => "popup",	// Шаблон местоположения
		"PROP_1" => "",	// Не показывать свойства для типа плательщика "Физическое лицо" (s1)
		"PATH_TO_BASKET" => "/personal/cart/",	// Страница корзины
		"PATH_TO_PERSONAL" => "/personal/order/",	// Страница персонального раздела
		"PATH_TO_PAYMENT" => "/personal/order/payment/",	// Страница подключения платежной системы
		"PATH_TO_ORDER" => "/personal/order/make/",
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"DELIVERY2PAY_SYSTEM" => "p2d",
		"SHOW_ACCOUNT_NUMBER" => "Y"
	),
	false
);?>
    <?if (!$_GET['ORDER_ID']) {?>
        </div>
    <?}?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>