<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>
<div class="content-wrapper b-basket-wrapper">
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket",
	"sweetMyHome",
	array(
		"COMPONENT_TEMPLATE" => "sweetMyHome",
		"COLUMNS_LIST" => array(
			0  => "NAME",
			1  => "PRICE",
			2  => "QUANTITY",
			3  => "",
			4  => "",
			5  => "",
			6  => "",
			7  => "",
			8  => "",
			9  => "",
			10 => "",
		),
		"PATH_TO_ORDER" => "/personal/cart/",
		"HIDE_COUPON" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"USE_PREPAYMENT" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"ACTION_VARIABLE" => "action"
	),
	false
);?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>