<?require($_SERVER['DOCUMENT_ROOT']."/bitrix/header.php");?>
<div class="content">
    <div class="content-wrapper">
        <?$APPLICATION->IncludeComponent(
			"bitrix:catalog.compare.result",
			"sweetMyHomeCompare", Array(
				"TEMPLATE_THEME" => "blue",	// Цветовая тема
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"NAME" => "CATALOG_COMPARE_LIST",	// Уникальное имя для списка сравнения
				"IBLOCK_TYPE" => "",	// Тип инфоблока
				"IBLOCK_ID" => "19",	// Инфоблок
				"FIELD_CODE" => array("NAME", "DETAIL_PICTURE"),	// Поля
				"PROPERTY_CODE" => array(	// Свойства
					0 => "",
					1 => "",
					2 => "",
					3 => "",
					4 => "",
					5 => "",
					6 => "",
					7 => "",
					8 => "",
					9 => "",
					10 => "",
					11 => "",
					12 => "",
				),
				"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
				"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
				"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
				"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
				"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
				"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
				"PRICE_CODE" => array(	// Тип цены
					0 => "BASE",
				),
				"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
				"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
				"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
				"DISPLAY_ELEMENT_SELECT_BOX" => "N",	// Выводить список элементов инфоблока
				"ELEMENT_SORT_FIELD_BOX" => "name",	// По какому полю сортируем список элементов
				"ELEMENT_SORT_ORDER_BOX" => "asc",	// Порядок сортировки списка элементов
				"ELEMENT_SORT_FIELD_BOX2" => "id",	// Поле для второй сортировки списка элементов
				"ELEMENT_SORT_ORDER_BOX2" => "desc",	// Порядок второй сортировки списка элементов
				"HIDE_NOT_AVAILABLE" => "N",	// Не отображать в списке товары, которых нет на складах
				"CONVERT_CURRENCY" => "Y",	// Показывать цены в одной валюте
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			),
			false
		);?>
    </div>
</div>
<?require($_SERVER['DOCUMENT_ROOT']."/bitrix/footer.php");?>