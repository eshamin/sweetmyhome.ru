<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)) {?>
    <ul class="menu">

    <?
    foreach($arResult as $arItem) {
        if ($arItem['PARAMS']['IS_PARENT']) {
        ?>
            <li class="blue">
                <a href="#"><?=$arItem['TEXT']?></a><span class="ico ico-angle-down"></span>
                <ul class="b-dropright-catalog b-top-menu__item">
        <?
        } else {
        ?>
            <li>
                <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            </li>
        <?
        }

    }?>
            </ul>
        </li>
    </ul>
<?}?>