<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)) {?>
    <ul class="features">

    <?
    foreach($arResult as $arItem) {
    ?>
        <li>
            <a href="<?=$arItem['LINK']?>">
                <span class="ico <?=$arItem['PARAMS']['CLASS']?>"></span><em><?=$arItem['TEXT']?></em>
            </a>
        </li>
    <?}?>

    </ul>
<?}?>