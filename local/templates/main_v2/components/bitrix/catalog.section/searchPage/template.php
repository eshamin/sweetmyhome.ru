<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) {
    $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
    $strMainID = $this->GetEditAreaId($arItem['ID']);
    ?>
    <div class="catalogGoodsBlock lines b-products__list b-products__list--list b-box-group">
        <? foreach ($arResult['ITEMS'] as $key => $arItem) { ?>
            <!--        каталог списком                -->
            <section class="b-product b-box  b-product_list-item-w-foto" id="<? echo $strMainID; ?>">
                <div class="b-box__i">
                    <div class="b-product__right">
                        <div class="b-product__right__wrap">
                            <span class="js_gtm_helper" style="display: none"></span>

                            <? if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                                <div class="b-product__price">
                                    <span class="b-price b-price_size4">
                                        <span
                                            class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] ?></span>
                                        <span class="b-price__sign"></span>
                                    </span>
                                    <del class="b-price b-price_old">
                                        <span class="num"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?></span>
                                        <span class="sign"></span>
                                    </del>
                                </div>
                            <? } else { ?>
                                <div class="b-product__price">
                                <span class="b-price b-price_size4">
                                    <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?></span>
                                    <span class="b-price__sign"></span></span>
                                </div>
                            <?
                            } ?>

                            <div
                                class="b-product-status b-product-status_theme_normal b-product-status_instock b-product__status ">
                                <span class="b-pseudolink js-qtip-click-handle js-avail-click">
                                    <? if ($arItem['PROPERTIES']['STOCK']['VALUE'] == 'Y') { ?>В наличии<?
                                    } ?>
                                </span>
                            </div>

                        </div>

                        <div
                            class="btn-group btn-group_theme_normal btn-group-vertical b-product__actions js-rutarget-actions">
                            <a href="javascript: void(0)" class="btn add-to-favorites" item-id="<?= $arItem['ID'] ?>"
                               title="Добавить в избранное">
                                <span class="ico ico-favorite"></span>
                                <span class="btn__i">В&nbsp;избранное</span>
                            </a>
                            <a href="<?= $arItem['COMPARE_URL'] ?>" class="btn add-to-compare" title="К сравнению">
                                <span class="ico ico-compare"></span>
                                <span class="btn__i">К&nbsp;сравнению</span>
                            </a>
                            <a class="btn btn btn-primary add2basket" item-id="<?= $arItem['ID'] ?>"
                               href="<?= $arItem['ADD_URL'] ?>" style="margin-left: 0!important;">Купить</a>
                        </div>

                    </div>
                    <?
                    $arFileTmp = CFile::ResizeImageGet(
                        $arItem['DETAIL_PICTURE'],
                        array("width" => 235, "height" => 148),
                        BX_RESIZE_IMAGE_EXACT,
                        true,
                        false
                    );
                    ?>
                    <a target="_blank" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                       class="must_be_href double-hover js-gtm-product-click">
                        <span class="b-img2 b-img2_size b-img2_size_lg b-product__img" style="margin-right: 36px;">
                            <img src="<?= $arFileTmp['src'] ?>" class="b-img2__img" style="vertical-align: top;">
                        </span>
                    </a>

                    <div class="b-product__center">
                        <div class="b-product__title b-product__list">
                            <span class="b-truncate helper-block">
                                <a target="_blank" class="must_be_href js-gtm-product-click"
                                   href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
                            </span>
                        </div>
                        <div class="b-product__descr">
                            <? if ($arItem['PROPERTIES']['SIZE1']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE']) { ?>
                                Длина: <?= $arItem['PROPERTIES']['SIZE1']['VALUE'] ?>, Глубина: <?= $arItem['PROPERTIES']['SIZE3']['VALUE'] ?>, Высота: <?= $arItem['PROPERTIES']['SIZE2']['VALUE'] ?>
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                                <?
                            } ?>
                            <? if ($arItem['PROPERTIES']['COLORTEXT']['VALUE']) { ?>
                                Цвет: <?= $arItem['PROPERTIES']['COLORTEXT']['VALUE'] ?><img
                                src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                                <?
                            } ?>
                            <? if ($arItem['PROPERTIES']['SER']['VALUE']) { ?>
                                Серия: <?= $arItem['PROPERTIES']['SER']['VALUE'] ?><img
                                src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                                <?
                            } ?>
                            <? if ($arItem['PROPERTIES']['SLEEPSIZE']['VALUE']) { ?>
                                Спальное место: <?= $arItem['PROPERTIES']['SLEEPSIZE']['VALUE'] ?> см<img
                                    src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                                <?
                            } ?>
                            <? if ($arItem['PROPERTIES']['MATERIAL']['VALUE']) { ?>
                                Материал: <?= $arItem['PROPERTIES']['MATERIAL']['VALUE'] ?><img
                                src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                                <?
                            } ?>
                            <? if ($arItem['PROPERTIES']['WEIGHT']['VALUE']) { ?>
                                Вес: <?= $arItem['PROPERTIES']['WEIGHT']['VALUE'] ?> кг
                                <?
                            } ?>
                        </div>
                        <?
                        $resComments = CIBlockSection::GetList(
                            array('DATE_CREATED' => 'DESC'),
                            array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID_VALUE' => $arItem['ID']),
                            false,
                            false,
                            array('PROPERTY_RATING'));
                        $totalRating = 0;
                        $totalCount = 0;
                        while ($comment = $resComments->Fetch()) {
                            $totalRating += $comment['PROPERTY_RATING_VALUE'];
                            $totalCount++;
                        }

                        if ($totalCount > 0) {
                            $totalRating = floor($totalRating / $totalCount);
                        }
                        ?>
                        <div class="b-product__group" style="margin-top: 24px;">
                            <div class="b-product__art">Артикул <span class="num"><?= $arItem['ID'] ?></span></div>
                            <div class="b-stars-wrap g-clickable b-product__stars-wrap _small">
                                <? if ($totalRating > 0) { ?>
                                    <div class="b-small-stars _s4 g-clickable js-qtip-click-handle">
                                        <i class="s1"></i>
                                        <i class="s2"></i>
                                        <i class="s3"></i>
                                        <i class="s4"></i>
                                        <i class="s5"></i>
                                    </div>
                                    <span class="pseudolink count"><?= $totalCount; ?></span>
                                    <?
                                } ?>
                            </div>
                            <div class="b-label-group b-label-group_size_xs b-product__label2-group">
                                <span class="b-label b-label_info">Выгодная покупка</span>
                                <span class="b-label b-label_info">Акция</span>
                                <? if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                                    <span class="b-label b-label_info">Суперцена</span>
                                    <?
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?
        }
        if ($arResult['NAV_RESULT']->nSelectedCount > 30) {
            $addCount = 30;
            if ($arResult['NAV_RESULT']->nSelectedCount < 60) {
                $addCount = $arResult['NAV_RESULT']->nSelectedCount - 30;
            }            
            ?>
            <span
                class="btn-product-more btn-product-more-list b-box b-box_theme_normal b-box_hoverable js-show-more-parent b-box_wide b-product_list-item-w-foto">
                <span class="b-box__i">
                    <span class="ico ico-plus"></span>
                    <span class="btn-product-more__text js-show-more "><span>Показать еще <?=$addCount?></span></span>
                    <span class="text-sm text-muted">Показано <span
                            class="total-show-count">30</span> товаров из <span><?= $arResult['NAV_RESULT']->nSelectedCount ?></span></span>
                </span>
            </span>
        <? } ?>
    </div>
    <!--        \каталог списком                -->
    <!--        каталог плиткой                -->
    <ul class="b-products__list b-products__list--tale" data-item-minwidth="230">
    <?
    foreach ($arResult['ITEMS'] as $key => $arItem) {
        ?>

        <li class="b-products__item b-products__item--ha g-col-4" id="<? echo $strMainID; ?>">
            <div class="b-products__item-container">
                <?
                $arFileTmp = CFile::ResizeImageGet(
                    $arItem['DETAIL_PICTURE'],
                    array("width" => 235, "height" => 148),
                    BX_RESIZE_IMAGE_EXACT,
                    true,
                    false
                );
                ?>
                <a target="_blank" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="">
                    <!--                        <span class="b-img2 b-img2_size_lg b-product__img center-block">-->
                    <img src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>" class="b-product__image">
                    <!--                        </span>-->
                </a>
                <div class="b-product__label-group b-labels">
                    <div class="b-labels__item b-labels__item--action g-clear g-left">Акция!</div>
                    <div class="b-labels__item b-labels__item--benefit g-clear g-left">Выгодная покупка</div>
                    <? if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                        <div class="b-labels__item b-labels__item--bestprice g-clear g-left">Суперцена</div>
                        <?
                    } ?>
                </div>

                <?
                $resComments = CIBlockSection::GetList(
                    array('DATE_CREATED' => 'DESC'),
                    array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID_VALUE' => $arItem['ID']),
                    false,
                    false,
                    array('PROPERTY_RATING'));
                $totalRating = 0;
                $totalCount = 0;
                while ($comment = $resComments->Fetch()) {
                    $totalRating += $comment['PROPERTY_RATING_VALUE'];
                    $totalCount++;
                }

                if ($totalCount > 0) {
                    $totalRating = floor($totalRating / $totalCount);
                }
                ?>
                <div class="b-stars-wrap b-product__stars-wrap g-clickable pull-left _small">

                    <? if ($totalRating > 0) { ?>
                        <div class="b-small-stars _s<?= $totalRating ?> g-clickable js-qtip-click-handle">
                            <i class="s1"></i>
                            <i class="s2"></i>
                            <i class="s3"></i>
                            <i class="s4"></i>
                            <i class="s5"></i>
                        </div>
                        <span class="pseudolink count"><?= $totalCount; ?></span>
                        <?
                    } ?>
                    <a href="javascript: void(0)" class="btn add-to-favorites" item-id="<?= $arItem['ID'] ?>"
                       title="Добавить в избранное">
                        <span class="ico ico-favorite"></span>
                    </a>
                    <a href="<?= $arItem['COMPARE_URL'] ?>" class="btn add-to-compare" title="К сравнению">
                        <span class="ico ico-compare"></span>
                    </a>
                </div>
                <div class="b-product__title b-product__title--nomargin">
                    <a target="_blank" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                       class="must_be_href js-gtm-product-click">
                        <?= (strlen($arItem['NAME']) > 90) ? substr($arItem['NAME'], 0, 90) : $arItem['NAME'] ?>
                    </a>
                    <? if ($arItem['PROPERTIES']['SIZE1']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE']) { ?>
                        <p class="muted">Длина <?= $arItem['PROPERTIES']['SIZE1']['VALUE'] ?> •
                            Глубина <?= $arItem['PROPERTIES']['SIZE3']['VALUE'] ?> •
                            Высота <?= $arItem['PROPERTIES']['SIZE2']['VALUE'] ?></p>
                        <?
                    }
                    ?>
                </div>

                <div class="b-product__bottom">
                    <? if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                        <div class="b-product__price">
                        <span class="b-price b-price_theme_normal b-price_size4">
                            <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] ?></span>
                            <span class="b-price__sign"></span>
                        </span>
                            <del class="b-price b-price_theme_normal b-price_old">
                                <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?>&nbsp;</span>
                                <span class="b-price__sign"></span>
                            </del>
                        </div>
                    <? } else { ?>
                        <div class="b-product__price">
                        <span class="b-price b-price_theme_normal b-price_size4">
                            <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?>&nbsp;</span>
                            <span class="b-price__sign"></span>
                        </span>
                        </div>
                    <?
                    } ?>
                    <div
                        class="btn-group  btn-group_theme_normal btn-group-btnleft b-product__actions js-rutarget-actions">
                        <a class="btn btn-y btn-28 add2basket" item-id="<?= $arItem['ID'] ?>" href="<?= $arItem['ADD_URL'] ?>">Купить</a>
                    </div>
                    <div class="b-product__group">
                        <div
                            class="b-product-status b-product-status_theme_normal b-product-status_instock b-product__status pull-right">
                                <span class="b-pseudolink js-qtip-click-handle js-avail-click">
                                    <? if ($arItem['CAN_BUY']) { ?>В наличии<?
                                    } ?>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <?
    }
    if ($arResult['NAV_RESULT']->nSelectedCount > 30) {
        $addCount = 30;
        if ($arResult['NAV_RESULT']->nSelectedCount < 60) {
            $addCount = $arResult['NAV_RESULT']->nSelectedCount - 30;
        }        
        ?>
        <li
            class="b-products__item g-col-4 btn-product-more btn-product-more-card b-box b-box_theme_normal b-box_hoverable js-show-more-parent b-box_w240 b-box_h410 l-line-item">
            <span class="b-box__i">
                <span class="ico ico-plus"></span>
                <span class="btn-product-more__text js-show-more "><span>Показать еще <?=$addCount?></span></span>
                <span class="text-sm text-muted">Показано <span class="total-show-count">30</span> товаров из <span
                        id="total-count-items"><?= $arResult['NAV_RESULT']->nSelectedCount ?></span></span>
            </span>
        </li>
        </ul>
        <!--        \каталог плиткой                -->
        <?
    }
} ?>
<div id="good-added" style="display: none">
    <div class="good-added">
        <h2>Товар добавлен в корзину</h2>
        <table class="b-basket">
            <tbody>
            <tr>
                <td class="b-basket__img" id="item_img"><img src="" alt=""/></td>
                <td class="b-basket__name">
                    <h4 id="item_name"></h4>
                </td>
                <td class="b-basket__price"><span id="item_discount_price"></span></td>
                <td class="b-basket__summ" style="text-align: center">
                    <span id="item_count"></span> <span> шт.</span>
                </td>
                <td class="b-basket__summ">
                    <span id="item_price"></span>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="good-added__elems">
            <a class="js-button js-button-yellow left" href="/personal/cart">Перейти в корзину</a>
            <a class="left" href="javascript:void(0)">или продолжить покупки</a>

            <p>Всего <span class="quantityTotalBasket"></span> на сумму <span class="priceTotalBasket"></span></p>

            <div class="clear"></div>
        </div>
    </div>
</div>

