<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ITEMS'])) {

    foreach ($arResult['ITEMS'] as $key => $arItem) {
        ?>
        <li class="b-products__item b-products__item--ha g-col-4" id="<? echo $strMainID; ?>">
            <div class="b-products__item-container">
                <?
                $arFileTmp = CFile::ResizeImageGet(
                    $arItem['DETAIL_PICTURE'],
                    array("width" => 235, "height" => 148),
                    BX_RESIZE_IMAGE_EXACT,
                    true,
                    false
                );
                ?>
                <a target="_blank" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="">
                    <!--                        <span class="b-img2 b-img2_size_lg b-product__img center-block">-->
                    <img src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>" class="b-product__image">
                    <!--                        </span>-->
                </a>
                <div class="b-product__label-group b-labels">
                    <div class="b-labels__item b-labels__item--action g-clear g-left">Акция!</div>
                    <div class="b-labels__item b-labels__item--benefit g-clear g-left">Выгодная покупка</div>
                    <? if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                        <div class="b-labels__item b-labels__item--bestprice g-clear g-left">Суперцена</div>
                        <?
                    } ?>
                </div>

                <?
                $resComments = CIBlockSection::GetList(
                    array('DATE_CREATED' => 'DESC'),
                    array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID_VALUE' => $arItem['ID']),
                    false,
                    false,
                    array('PROPERTY_RATING'));
                $totalRating = 0;
                $totalCount = 0;
                while ($comment = $resComments->Fetch()) {
                    $totalRating += $comment['PROPERTY_RATING_VALUE'];
                    $totalCount++;
                }

                if ($totalCount > 0) {
                    $totalRating = floor($totalRating / $totalCount);
                }
                ?>
                <div class="b-stars-wrap b-product__stars-wrap g-clickable pull-left _small">

                    <? if ($totalRating > 0) { ?>
                        <div class="b-small-stars _s<?= $totalRating ?> g-clickable js-qtip-click-handle">
                            <i class="s1"></i>
                            <i class="s2"></i>
                            <i class="s3"></i>
                            <i class="s4"></i>
                            <i class="s5"></i>
                        </div>
                        <span class="pseudolink count"><?= $totalCount; ?></span>
                        <?
                    } ?>
                    <a href="javascript: void(0)" class="btn add-to-favorites" item-id="<?= $arItem['ID'] ?>"
                       title="Добавить в избранное">
                        <span class="ico ico-favorite"></span>
                    </a>
                    <a href="<?= $arItem['COMPARE_URL'] ?>" class="btn add-to-compare" title="К сравнению">
                        <span class="ico ico-compare"></span>
                    </a>
                </div>
                <div class="b-product__title b-product__title--nomargin">
                    <a target="_blank" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                       class="must_be_href js-gtm-product-click">
                        <?= (strlen($arItem['NAME']) > 90) ? substr($arItem['NAME'], 0, 90) : $arItem['NAME'] ?>
                    </a>
                    <? if ($arItem['PROPERTIES']['SIZE1']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE']) { ?>
                        <p class="muted">Длина <?= $arItem['PROPERTIES']['SIZE1']['VALUE'] ?> •
                            Глубина <?= $arItem['PROPERTIES']['SIZE3']['VALUE'] ?> •
                            Высота <?= $arItem['PROPERTIES']['SIZE2']['VALUE'] ?></p>
                        <?
                    }
                    ?>
                </div>

                <div class="b-product__bottom">
                    <? if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                        <div class="b-product__price">
                        <span class="b-price b-price_theme_normal b-price_size4">
                            <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] ?></span>
                            <span class="b-price__sign"></span>
                        </span>
                            <del class="b-price b-price_theme_normal b-price_old">
                                <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?>&nbsp;</span>
                                <span class="b-price__sign"></span>
                            </del>
                        </div>
                    <? } else { ?>
                        <div class="b-product__price">
                        <span class="b-price b-price_theme_normal b-price_size4">
                            <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?>&nbsp;</span>
                            <span class="b-price__sign"></span>
                        </span>
                        </div>
                    <?
                    } ?>
                    <div
                        class="btn-group  btn-group_theme_normal btn-group-btnleft b-product__actions js-rutarget-actions">
                        <a class="btn btn-y btn-28 add2basket" item-id="<?= $arItem['ID'] ?>" href="<?= $arItem['ADD_URL'] ?>">Купить</a>
                    </div>
                    <div class="b-product__group">
                        <div
                            class="b-product-status b-product-status_theme_normal b-product-status_instock b-product__status pull-right">
                                <span class="b-pseudolink js-qtip-click-handle js-avail-click">
                                    <? if ($arItem['CAN_BUY']) { ?>В наличии<?
                                    } ?>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    <?
    }
}
?>