<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<div class="b-breadcrumbs">';

$num_items = count($arResult);
for($index = 0, $itemSize = $num_items; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
		$strReturn .= '<a class="b-breadcrumbs__link" href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'&nbsp;&rarr;&nbsp;</a>';
	else
		$strReturn .= '<span class="b-breadcrumbs__current">'.$title.'&nbsp;<span class="b-breadcrumbs__current-arrow"></span>&nbsp;</span>';
}

$strReturn .= '</div>';

return $strReturn;
?>