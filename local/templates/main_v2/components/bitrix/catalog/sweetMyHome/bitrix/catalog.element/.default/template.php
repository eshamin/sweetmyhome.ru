<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="b-goods" id="b-goods">
    <h1 class="b-goods__header"><?=$arResult['NAME']?></h1>
    <ul class="b-goods__header-bottom">
        <?if ($arResult['PROPERTIES']['SIZE1']['VALUE'] || $arResult['PROPERTIES']['SIZE3']['VALUE'] || $arResult['PROPERTIES']['SIZE3']['VALUE']) {?>
            <li class="b-goods__header-bottom--item">Длина: <?=$arResult['PROPERTIES']['SIZE1']['VALUE']?>, Глубина: <?=$arResult['PROPERTIES']['SIZE3']['VALUE']?>, Высота: <?=$arResult['PROPERTIES']['SIZE2']['VALUE']?></li>
        <?}?>
        <? if ($arResult['PROPERTIES']['COLORTEXT']['VALUE']) {?>
            <li class="b-goods__header-bottom--item">Цвет: <?=$arResult['PROPERTIES']['COLORTEXT']['VALUE']?></li>
        <?}?>
        <? if ($arResult['PROPERTIES']['SER']['VALUE']) {?>
            <li class="b-goods__header-bottom--item">Серия: <?=$arResult['PROPERTIES']['SER']['VALUE']?></li>
        <?}?>
        <? if ($arResult['PROPERTIES']['SLEEPSIZE']['VALUE']) {?>
            <li class="b-goods__header-bottom--item">Спальное место: <?=$arResult['PROPERTIES']['SLEEPSIZE']['VALUE']?> см</li>
        <?}?>
        <? if ($arResult['PROPERTIES']['MATERIAL']['VALUE']) {?>
            <li class="b-goods__header-bottom--item">Материал: <?=implode('<br />', $arResult['PROPERTIES']['MATERIAL']['VALUE'])?></li>
        <?}?>
        <? if ($arResult['PROPERTIES']['WEIGHT']['VALUE']) {?>
            <li class="b-goods__header-bottom--item">Вес: <?=$arResult['PROPERTIES']['WEIGHT']['VALUE']?> кг</li>
        <?}?>
    </ul>
    <div class="clear"></div>
    <div class="b-goods__info-top-panel">
        <ul class="b-top-panel">
            <li class="b-top-panel__art">Арт. <span><?=$arResult['ID']?></span></li>
            <?
            $totalRating = 0;
            $totalCount = 0;
            foreach($arResult['ALL_COMMENTS'] as $comment) {
                if ($comment['PROPERTY_RATING_VALUE'] > 0) {
                    $totalRating += $comment['PROPERTY_RATING_VALUE'];
                    $totalCount++;
                }
            }

            if ($totalCount > 0) {
                $totalRating = floor($totalRating / $totalCount);
            }
            ?>
            <li class="b-top-panel__feedback">
                <?if ($totalRating > 0) {?>
                    Оценка покупателей
                    <span class="b-top-panel__feedback-stars">
                        <?for ($i = 0; $i < $totalRating; $i++) {?>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                        <?}?>
                        <?for ($i = $totalRating; $i < 5; $i++) {?>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                        <?}?>
                    </span>
                <?}?>
                <a href="#b-feedbacks" class="b-top-panel__feedback-span"><?=$totalCount?> отзывов</a>
            </li>
            <li class="b-top-panel__bestby"><a href="#">Выгодная попкупка</a></li>
            <li class="b-top-panel__action"><a href="#">Акция</a></li>
            <li class="b-top-panel__warranty"><img src="<?=SITE_TEMPLATE_PATH?>/img/rostest.png" alt="Ростест"/><span>Гарантия 2 года</span></li>
        </ul>
    </div>
    <div class="b-goods__good">
        <div class="b-goods__good-inner">
            <div class="b-goods__good-photo">
                <a href="javascript:void(0)" class="b-goods__good-photo-zoom"></a>
                <div class="b-goods__good-photo-main">
                    <a href="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="cloud-zoom" rel="position: 'right', adjustX: -14, adjustY:0, softFocus:false" style="position: relative; display: block;">
                        <img class="catalog-img-new" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" style="display: block;">
                    </a>
                </div>
                <div class="b-goods__good-photo-thumbs">
                    <ul>
                        <?
                        $arFilter = array(array("name" => "sharpen", "precision" => 30));
                        $arFileTmp = CFile::ResizeImageGet(
                            $arResult['DETAIL_PICTURE'],
                            array("width" => 70, "height" => 70),
                            BX_RESIZE_IMAGE_EXACT,
                            true,
                            $arFilter
                        );
                        ?>
                        <li class="b-goods__good-photo-thumbs-current">
                            <a href="javascript:void(0)" data-img="<?=$arResult['DETAIL_PICTURE']['SRC']?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                        </li>
                        <?foreach ($arResult['PROPERTIES']['MOREPHOTO']['VALUE'] as $file) {
                            $arFileTmp = CFile::ResizeImageGet(
                                CFile::GetFileArray($file),
                                array("width" => 70, "height" => 70),
                                BX_RESIZE_IMAGE_EXACT,
                                true,
                                $arFilter
                            );
                            ?>
                            <li>
                                <a href="javascript:void(0)" data-img="<?=CFile::GetPath($file)?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                            </li>
                        <?}?>
                    </ul>
                    <a class="b-accessories__slider-button b-accessories__slider-button-prev" href="javascript:void(0)"></a>
                    <a class="b-accessories__slider-button b-accessories__slider-button-next" href="javascript:void(0)"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="b-goods__good-info">
                <div class="b-goods__good-info-description">
                    <div class="b-goods__good-info-description-scrollable">
                        <div>
                            <?=($arResult['PREVIEW_TEXT_TYPE'] == 'text') ? htmlspecialchars_decode($arResult['PREVIEW_TEXT']) : $arResult['PREVIEW_TEXT']?>
                        </div>
                    </div>
                <a href="#b-params"><span class="b-goods__good-span-more-info"></span></a>
                </div>
                <div class="b-goods__good-info-params">
                    <ul>
                        <li><span class="first-child"><span>Цвет</span></span><span><?=$arResult['PROPERTIES']['COLORTEXT']['VALUE']?></span></li>
                        <li><span class="first-child"><span>Вес</span></span><span><?=$arResult['PROPERTIES']['WEIGHT']['VALUE']?></span></li>
                        <li><span class="first-child"><span>Серия</span></span><span><?=$arResult['PROPERTIES']['SER']['VALUE']?></span></li>
                        <li><span class="first-child"><span>Длина</span></span><span><?=$arResult['PROPERTIES']['SIZE1']['VALUE']?></span></li>
                        <li><span class="first-child"><span>Высота</span></span><span><?=$arResult['PROPERTIES']['SIZE3']['VALUE']?></span></li>
                        <li><span class="first-child"><span>Глубина</span></span><span><?=$arResult['PROPERTIES']['SIZE2']['VALUE']?></span></li>
                    </ul>
                </div>
                <a class="js-button js-button-gray" href="#b-params">Подробные характеристики</a>

                <?
                $lastComment = $arResult['MAIN_COMMENT'];
                if ($lastComment) {
                ?>
                <div class="b-goods__good-feedback">
                    <div class="b-goods__good-feedback-bubble">
                        <span class="bubble_top"></span>
                        <div class="coment-block">
                            <p><?=$lastComment['PROPERTY_COMMENT_VALUE']['TEXT']?>
                            </p>
                        </div>
                        <span class="bubble-marker"></span>
                        <span class="bubble-corner"></span>
                        <span class="bubble_bottom">
                        	<span class="b-goods__good-span-more-info"></span>
                        </span>
                    </div>
                    <p><?=$lastComment['PROPERTY_FIO_VALUE']?></p>
                    <a class="js-button js-button-gray" href="#b-feedbacks">Все отзывы</a>
                </div>
                <?}?>
            </div>
        </div>
        <div class="b-goods__good-more-about">
            <div class="b-goods__good-more">
                <div class="b-goods__good-more-price">
                    <span><?=$arResult['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']?></span>
                    <?if ($arResult['MIN_PRICE']['VALUE'] > $arResult['MIN_PRICE']['DISCOUNT_VALUE']) {?>
                        <span class="muted"><s><?=$arResult['MIN_PRICE']['PRINT_VALUE']?></s></span>
                    <?}?>
                    <div class="b-goods__good-more-price-ico b-goods__good-more-price-ico3">
                        <div class="clear"></div>
                        <div class="soc_bloc" style="margin-bottom: 0px;height: 115px;">
                            <div class="soc_icon"></div>
                            <div class="soc" id="ya_share1"><span class="b-share">
                                <a class="b-share__handle">
                                    <span class="b-share__text">Делитесь с друзьями!</span>
                                </a>
                                <a rel="nofollow" target="_blank" title="ВКонтакте" class="b-share__handle b-share__link b-share-btn__vkontakte" href="https://share.yandex.net/go.xml?service=vkontakte&amp;url=http%3A%2F%2Fakmmos.ru%2Fkatalog-mebeli%2FKrovati%2Fspalnya-akm-krovat-160kh200-sm-liana-ortoped-osnovanie-170kh86kh203-venge-dub-mlechnyy-akm%2F&amp;title=" data-service="vkontakte">
                                    <span class="b-share-icon b-share-icon_vkontakte"></span>
                                </a>
                                <span class="b-share__hr"></span>
                                <a rel="nofollow" target="_blank" title="Facebook" class="b-share__handle b-share__link b-share-btn__facebook" href="https://share.yandex.net/go.xml?service=facebook&amp;url=http%3A%2F%2Fakmmos.ru%2Fkatalog-mebeli%2FKrovati%2Fspalnya-akm-krovat-160kh200-sm-liana-ortoped-osnovanie-170kh86kh203-venge-dub-mlechnyy-akm%2F&amp;title=" data-service="facebook">
                                    <span class="b-share-icon b-share-icon_facebook"></span>
                                </a>
                                <span class="b-share__hr"></span>
                                <a rel="nofollow" target="_blank" title="Twitter" class="b-share__handle b-share__link b-share-btn__twitter" href="https://share.yandex.net/go.xml?service=twitter&amp;url=http%3A%2F%2Fakmmos.ru%2Fkatalog-mebeli%2FKrovati%2Fspalnya-akm-krovat-160kh200-sm-liana-ortoped-osnovanie-170kh86kh203-venge-dub-mlechnyy-akm%2F&amp;title=" data-service="twitter">
                                    <span class="b-share-icon b-share-icon_twitter"></span>
                                </a>
                                <span class="b-share__hr"></span>
                                <a rel="nofollow" target="_blank" title="Мой Мир" class="b-share__handle b-share__link b-share-btn__moimir" href="https://share.yandex.net/go.xml?service=moimir&amp;url=http%3A%2F%2Fakmmos.ru%2Fkatalog-mebeli%2FKrovati%2Fspalnya-akm-krovat-160kh200-sm-liana-ortoped-osnovanie-170kh86kh203-venge-dub-mlechnyy-akm%2F&amp;title=" data-service="moimir">
                                    <span class="b-share-icon b-share-icon_moimir"></span>
                                </a>
                                <span class="b-share__hr"></span>
                                <a rel="nofollow" target="_blank" title="Одноклассники" class="b-share__handle b-share__link b-share-btn__odnoklassniki" href="https://share.yandex.net/go.xml?service=odnoklassniki&amp;url=http%3A%2F%2Fakmmos.ru%2Fkatalog-mebeli%2FKrovati%2Fspalnya-akm-krovat-160kh200-sm-liana-ortoped-osnovanie-170kh86kh203-venge-dub-mlechnyy-akm%2F&amp;title=" data-service="odnoklassniki">
                                    <span class="b-share-icon b-share-icon_odnoklassniki"></span>
                                </a>
                                <span class="b-share__hr"></span>
                                <a rel="nofollow" target="_blank" title="Google Plus" class="b-share__handle b-share__link b-share-btn__gplus" href="https://share.yandex.net/go.xml?service=gplus&amp;url=http%3A%2F%2Fakmmos.ru%2Fkatalog-mebeli%2FKrovati%2Fspalnya-akm-krovat-160kh200-sm-liana-ortoped-osnovanie-170kh86kh203-venge-dub-mlechnyy-akm%2F&amp;title=" data-service="gplus">
                                    <span class="b-share-icon b-share-icon_gplus"></span>
                                </a>
                                <span class="b-share__hr"></span>
                                <a rel="nofollow" target="_blank" title="LiveJournal" class="b-share__handle b-share__link b-share-btn__lj" href="https://share.yandex.net">
                                    <span class="b-share-icon b-share-icon_lj"></span>
                                </a>
                                </span>
                            </div>
                            <div class="mail_icon">
                                <span class="bsharetext">
                                    <a href="#">Отправить ссылку почтой</a>
                                </span>
                            </div>
                            <div class="print_icon">
                                <span class="bsharetext">
                                    <a href="#">Версия для печати</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <a href="<?=$arResult['COMPARE_URL']?>" class="b-goods__good-more-price-ico b-goods__good-more-price-ico2"></a>
                    <a href="javascript: void(0)" item-id="<?=$arResult['ID']?>" class="b-goods__good-more-price-ico b-goods__good-more-price-ico1 add-to-favorites"></a>
                </div>
                <div class="clear"></div>
                <div class="b-goods__good-more-buy">
                    <a class=" btn btn-y add2basket" item-id="<?=$arResult['ID']?>" href="<?=$arResult['ADD_URL']?>">Купить</a>
                    <div class="js-digital-spinner">
                        <input type="text" value="1" class="onlyDigits" id="qnt2basket<?=$arResult['ID']?>"/>
                        <span class="js-digital-spinner-up"></span>
                        <span class="js-digital-spinner-down"></span>
                    </div>
                    <div class="b-goods__good-more-oneclick">
                        <p>Купить в один клик</p>
                        <div>+7 <input type="text" id="oneclick-phone"/><a item-id="<?=$arResult['ID']?>" class="btn btn-y js-button-small" id="order-oneclick" href="javascript: void(0)">Оk</a></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="b-goods__good-more-delivery">
                    <!--<p style="color:#3b7d4f;">Доставка сегодня</p>-->
                    <!--<p>Доставка в <a href="javascript:void(0)">г.Москва</a> бесплатно</p>-->
                    <!--<a href="#">Подробнее о доставке</a>-->
                    <div class="b-makeorder__tablinks_2">
                        <a href="javascript:void(0)" class="b-makeorder__currenttab">Доставка</a>
                        <a href="javascript:void(0)">Оплата</a>
                        <a href="javascript:void(0)">Гарантия</a>
                    </div>
                    <div class="b-makeorder__tabs b-makeorder__tabs-delivery b-makeorder__tabs-current">
                        <p>Доставка в 8.000 населенных пунктов России</p>
                        <p>Статус наличия на складе <a href="javascript:void(0)">• <?=$arResult['PROPERTY_STATUS_VALUE']?></a></p>
                        <ul>
                            <li><p>- <a href="/delivery">Наши курьеры</a>  <span>• 0 руб. • до 0 дней</span></p></li>
                            <li><p>- <a href="/delivery">Курьеры служб</a>   <span>• 350 руб. • от 0 дней</span></p></li>
                            <li><p>- <a href="/delivery">Постамат</a>  <span>• 370 руб. • от 1 до 2 дней</span></p></li>
                            <li><p>- <a href="/delivery">Пункт выдачи</a>  <span>• 380 руб. • от 1 до 2 дней</span></p></li>
                            <li><p>- <a href="/delivery">Трансп. компания</a>  <span>• 1030 руб. • от 2 до 3 дней</span></p></li>
                        </ul>
                        <a href="/delivery">Подробнее о доставке</a>
                    </div>
                    <div class="b-makeorder__tabs b-makeorder__tabs-pay">
                        <h4>Возможны следующие варианты оплаты</h4>
                        <ul>
                            <li>- Наличными курьеру</li>
                            <li>- Онлайн оплата банковской картой</li>
                            <li>- Электронные деньги</li>
                            <li>- Интернет-банкинг</li>
                            <li>- Баланс телефона</li>
                            <li>- Банковским переводом на реквизиты</li>
                            <li>- Безналичным расчетом</li>
                        </ul>
                        <a href="/payment">Подробнее о способах оплаты</a>
                    </div>
                    <div class="b-makeorder__tabs b-makeorder__tabs-warranty">
                        <h4>Почему нам доверяют более 50 000 клиентов</h4>
                        <ul>
                            <li>- 2 года гарантии на мебель</li>
                            <li>- 1 год гарантии на другие товары</li>
                            <li>- Гарантия возврата денег</li>
                            <li>- Замена бракованных товаров в кратчайший срок</li>
                            <li>- Все товары сертифицированы</li>
                        </ul>
                        <a href="/returns">Подробнее о гарантии и возврате</a>
                    </div>
                </div>
                <?if ($arResult['SALE_PRODUCT']) {?>
                    <div class="delimiter"></div>
                    <div class="b-goods__good-more-discount">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/more-ico1.png" alt=""/>
                        <a href="#b-discount">Есть такой же за 11 470 руб.</a>
                        <p>Причина уценки — Отремонтированный</p>
                    </div>
                <?}?>
                <?if (count($arResult['SAME_PRODUCTS']) > 0) {?>
                    <div class="delimiter"></div>
                    <div class="b-goods__good-more-colors">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/more-ico2.png" alt=""/>
                        <a href="#b-colors">Цвета и модификации</a>
                        <div>
                            <?foreach ($arResult['SAME_PRODUCTS'] as $sameProduct) {
                                if ($sameProduct['ID'] == $arResult['ID']) {
                                    $class = 'class="b-goods__good-more-colors-current" ';
                                } else {
                                    $class = '';
                                }
                                ?>
                                <span>
                                    <a href="<?=$sameProduct['DETAIL_PAGE_URL']?>" title="<?=$sameProduct['NAME']?>" style="cursor:pointer;">
                                        <label style="background-image:url('/colors/<?=$sameProduct['PICTURE']?>.jpg');display: inline-block;background-position: 0% center;background-size: 18px;width:18px;height:18px;border-radius: 3px;cursor: pointer;">
                                    </a>
                                </span>
                            <?}?>
                            <a href="#b-colors">Посмотреть все</a>
                        </div>
                    </div>
                <?}?>
                <div class="delimiter"></div>
                <div class="b-goods__good-more-credit" style="display: none;">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/more-ico3.png" alt=""/>
                    <a href="javascript:void(0)">Товар доступен в кредит</a>
                    <p>Ежемесячный платеж от 877 руб</p>
                </div>
            </div>
            <div class="clear"></div>
            <?if (count($arResult['SIMILAR_PRODUCTS']) > 0) {?>
                <div class="b-goods__more-like">
                    <h3>Похожие товары</h3>
                    <div class="b-goods__more-like-slider">
                        <ul>
                            <?foreach ($arResult['SIMILAR_PRODUCTS'] as $similarProduct) {?>
                                <li>
                                    <?
                                    $arFileTmp = CFile::ResizeImageGet(
                                        CFile::GetFileArray($similarProduct['DETAIL_PICTURE']),
                                        array("width" => 90, "height" => 90),
                                        BX_RESIZE_IMAGE_EXACT,
                                        true,
                                        false
                                    );
                                    ?>
                                    <a class="product-small-slider-img" href="<?=$similarProduct['DETAIL_PAGE_URL']?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                                    <p><a href="<?=$similarProduct['DETAIL_PAGE_URL']?>"><?=(strlen($similarProduct['NAME']) > 50) ? substr($similarProduct['NAME'], 0, 50) . ' ...' : $similarProduct['NAME']?></a></p>
                                    <p><?=FormatCurrency($similarProduct['CATALOG_PRICE_1'], 'RUB')?></p>
                                </li>
                            <?}?>
                        </ul>
                        <a class="b-goods__more-like-slider-button b-goods__more-like-slider-button-prev" href="javascript:void(0)"></a>
                        <a class="b-goods__more-like-slider-button b-goods__more-like-slider-button-next" href="javascript:void(0)"></a>
                    </div>
                    <a href="#b-discount">Ещё похожие товары</a>
                </div>
                <div class="clear"></div>
            <?}?>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?if (count($arResult['ACCESSORIES']) > 0) {?>
    <!-- Лучшие аксессуары -->
    <div class="b-accessories">
        <h2>Лучшие аксессуары</h2>
        <div class="clear"></div>
        <ul class="b-accessories__header-bottom">
            <?$class = 'class="b-accessories__header-bottom-current"';?>
            <?foreach ($arResult['ACCESSORIES_SECTIONS'] as $accSection) {?>
                <li <?=$class?>><a href="javascript:void(0)"><?=$accSection?></a></li>
                <?$class = '';?>
            <?}?>
        </ul>
        <div class="clear"></div>
        <?
        $class = 'b-accessories__slider-current';
        $display = ''
        ?>
        <div class="b-accessories__slider-container">
        <?foreach ($arResult['ACCESSORIES'] as $secId => $accessories) {?>
            <div class="b-accessories__slider <?=$class?>" <?=$display?>>
                <div class="b-accessories__slider-wrapper">
                    <ul>
                        <li>
                            <?
                            $arFileTmp = CFile::ResizeImageGet(
                                $arResult['DETAIL_PICTURE'],
                                array("width" => 183, "height" => 147),
                                BX_RESIZE_IMAGE_EXACT,
                                true,
                                false
                            );
                            ?>
							<a href="<?=$arResult['DETAIL_PAGE_URL']?>">
                            <img src="<?=$arFileTmp['src']?>" alt=""/>
                            <p><?=$arResult['NAME']?></p>
							</a>
                            <span><?=$arResult['PRICES']['BASE']['PRINT_VALUE']?></span>
                            <!-- id у чекбокса и поле for у label должны совпадать и быть уникальными, поэтому к ним вместо _1 надо подцеплять какой-то уникальный нумер(артикул, id товара?) -->
                            <input type="checkbox" id="b-accessories__slider_<?=$arResult['ID']?>" class="custom_chb" checked="checked" value="<?=$arResult['ID']?>"/>
                            <label for="b-accessories__slider_<?=$arResult['ID']?>" class="custom_chb_button_label custom_chb_label">Беру</label>
                        </li>
                        <?
                            $class = '';
                            $display = 'style="display:none"';
                            foreach ($accessories as $accessory) {
                                $arFileTmp = CFile::ResizeImageGet(
                                    CFile::GetFileArray($accessory['DETAIL_PICTURE']),
                                    array("width" => 183, "height" => 147),
                                    BX_RESIZE_IMAGE_EXACT,
                                    true,
                                    false
                                );
                                ?>
                                <li>
									<a href="<?=$accessory['DETAIL_PAGE_URL']?>">
                                    <img src="<?=$arFileTmp['src']?>" alt=""/>
                                    <p><?=$accessory['NAME']?></p>
									</a>
                                    <span><?=FormatCurrency($accessory['CATALOG_PRICE_1'], 'RUB')?></span>

                                    <input type="checkbox" id="b-accessories__slider_<?=$accessory['ID']?>" class="custom_chb" value="<?=$accessory['ID']?>"/>
                                    <label for="b-accessories__slider_<?=$accessory['ID']?>" class="custom_chb_button_label custom_chb_label" item-id="<?=$accessory['ID']?>">Беру</label>
                                </li>
                        <?}?>
                    </ul>
                </div>
                <a class="b-accessories__slider-button b-accessories__slider-button-prev" href="javascript:void(0)"></a>
                <a class="b-accessories__slider-button b-accessories__slider-button-next" href="javascript:void(0)"></a>
                <div class="b-accessories__right-panel">
                    <p>Вы выбрали <span id="quantityAccessories">1</span> товар на сумму <span id="priceAccessories"><?=$arResult['PRICES']['BASE']['PRINT_VALUE']?></span> </p>
                    <a href="/personal/order/make" class="js-button js-button-yellow" id="buy-accessories">Купить</a>
                    <p>Отметьте интересующие аксессуары и купите в один клик</p>
                    <a href="/personal/cart">Посмотреть выделенное</a>
                </div>
            </div>
        <?}?>
        </div>
    </div>
<?}?>

<!-- Характеристики и отзывы -->
<div class="b-params" id="b-params">
    <div class="b-params__tabs-links">
        <a href="javascript:void(0)" class="b-params-tabs-link b-params-tabs-link-params b-params-tabs-link-current"><img src="<?=SITE_TEMPLATE_PATH?>/img/goodtab1.png" alt=""/>Характеристики</a>
        <a href="javascript:void(0)" class="b-params-tabs-link b-params-tabs-link-feedbacks"><img src="<?=SITE_TEMPLATE_PATH?>/img/goodtab2.png" alt=""/>Отзывы  <span><?=count($arResult['SHOW_COMMENTS'])?></span></a>
        <!--<a href="javascript:void(0)" class="b-params-tabs-link b-params-tabs-link-videos"><img src="<?=SITE_TEMPLATE_PATH?>/img/goodtab3.png" alt=""/>Видео  <span>4</span></a>-->
        <!--<a href="javascript:void(0)" class="b-params-tabs-link b-params-tabs-link-discussion"><img src="<?=SITE_TEMPLATE_PATH?>/img/goodtab4.png" alt=""/>Обсуждения  <span>4</span></a>-->
    </div>
    <div class="b-params__tabs-tabs">
        <div class="b-params__tabs-tab b-params__tabs-tab-current">
            <div class="b-params__tabs-tab-padd">
                <h2>Характеристики товара</h2>
                <div class="b-goods__good-info-params">
                    <h3>Основные</h3>
                    <ul>
                        <?if ($arResult['PROPERTIES']['COLORTEXT']['VALUE']) {?>
                            <li><span class="first-child"><span>Цвет</span></span><span><?=$arResult['PROPERTIES']['COLORTEXT']['VALUE']?></span></li>
                        <?}?>
                        <?if ($arResult['PROPERTIES']['WEIGHT']['VALUE']) {?>
                            <li><span class="first-child"><span>Вес</span></span><span><?=$arResult['PROPERTIES']['WEIGHT']['VALUE']?></span></li>
                        <?}?>
                        <?if ($arResult['PROPERTIES']['SER']['VALUE']) {?>
                            <li><span class="first-child"><span>Серия</span></span><span><?=$arResult['PROPERTIES']['SER']['VALUE']?></span></li>
                        <?}?>
                        <?if ($arResult['PROPERTIES']['MATERIAL']['VALUE']) {?>
                            <li><span class="first-child"><span>Материал</span></span><span><?=implode('<br />', $arResult['PROPERTIES']['MATERIAL']['VALUE'])?></span></li>
                        <?}?>
                        <?if ($arResult['PROPERTIES']['DOP']['VALUE']) {?>
                            <li><span class="first-child"><span>Включено в цену</span></span><span><?=$arResult['PROPERTIES']['DOP']['VALUE']?></span></li>
                        <?}?>
                        <?if ($arResult['PROPERTIES']['COMMENT']['VALUE']) {?>
                            <li><span class="first-child"><span>Включено в цену</span></span><span><?=$arResult['PROPERTIES']['COMMENT']['VALUE']?></span></li>
                        <?}?>
                    </ul>
                </div>
                <?if ($arResult['PROPERTIES']['SIZE1']['VALUE'] || $arResult['PROPERTIES']['SIZE3']['VALUE'] || $arResult['PROPERTIES']['SIZE3']['VALUE']) {?>
                    <div class="b-goods__good-info-params">
                        <h3>Размеры</h3>
                        <ul>
                            <?if ($arResult['PROPERTIES']['SIZE1']['VALUE']) {?>
                                <li><span class="first-child"><span>Длина</span></span><span><?=$arResult['PROPERTIES']['SIZE1']['VALUE']?></span></li>
                            <?}?>
                            <?if ($arResult['PROPERTIES']['SIZE3']['VALUE']) {?>
                                <li><span class="first-child"><span>Высота</span></span><span><?=$arResult['PROPERTIES']['SIZE3']['VALUE']?></span></li>
                            <?}?>
                            <?if ($arResult['PROPERTIES']['SIZE2']['VALUE']) {?>
                                <li><span class="first-child"><span>Глубина</span></span><span><?=$arResult['PROPERTIES']['SIZE2']['VALUE']?></span></li>
                            <?}?>
                        </ul>
                    </div>
                <?}?>
                <div class="b-goods__good-info-params">
                    <?=$arResult['DETAIL_TEXT']?>
                </div>
            </div>
        </div>

        <div class="b-params__tabs-tab">
            <div class="b-feedbacks" id="b-feedbacks-tab">
                <div class="b-params__tabs-tab-padd">
                    <h2>Отзывы и оценки покупателей</h2>
                    <div class="b-feedbacs__main">
                        <!--<div class="b-feedbacs__sortby">
                            <span>Сортировать по:</span>
                            <a href="javascript:void(0)">Дате</a>
                            <a class="b-feedbacs__sortby-current js-button js-button-gray" href="javascript:void(0)">Количеству звезд</a>
                        </div>-->
                        <div class="b-feedbacks__feedbacks-feedback">
                            <ul>
                                <?foreach ($arResult['SHOW_COMMENTS'] as $comment) {?>
                                    <li>
                                        <div class="b-feedbacks__feedback-info">
                                            <span class="b-top-panel__feedback-stars">
                                                <?for ($i = 0; $i < $comment['PROPERTY_RATING_VALUE']; $i++) {?>
                                                    <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                                <?}?>
                                                <?for ($i = $comment['PROPERTY_RATING_VALUE']; $i < 5; $i++) {?>
                                                    <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                                <?}?>
                                            </span>
                                            <h4><?=$comment['PROPERTY_FIO_VALUE']?></h4>
                                            <p><?=$comment['DATE_CREATED']?></p>
                                        </div>
                                        <div class="b-feedbacks__feedback-text">
                                            <p class="b-feedbacks__feedback-text-main">
                                                <?=$comment['PROPERTY_COMMENT_VALUE']['TEXT']?>
                                            </p>
                                            <p class="b-feedbacks__feedback-text-plus"><?=$comment['PROPERTY_PLUS_VALUE']['TEXT']?></p>
                                            <p class="b-feedbacks__feedback-text-minus"><?=$comment['PROPERTY_MINUS_VALUE']['TEXT']?></p>
                                        </div>
                                    </li>
                                <?}?>
                            </ul>
                            <a href="javscript:void(0)" class="js-button-gray js-button">Все отзывы</a>
                        </div>
                        <div class="b-feedbacks__right">
                            <a id="feedback-create-bt" class="btn btn-y js-popup" data-id="feedback-create-bt" data-callback="putSrars" href="javascript:void(0)">Добавить отзыв</a>
                            <h4>Оценка пользователей</h4>
                            <span class="b-top-panel__feedback-stars-big">
                                <?for ($i = 0; $i < $totalRating; $i++) {?>
                                    <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold-big"></span>
                                <?}?>
                                <?for ($i = $totalRating; $i < 5; $i++) {?>
                                    <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray-big"></span>
                                <?}?>
                            </span>
                            <p><?=getNumEnding($arResult['RATING_COMMENTS'], array('оценка', 'оценки', 'оценок'))?></p>
                            <h4>Отзывов с оценкой</h4>
                            <ul class="b-top-panel__feedback-stars-list">
                                <li>
                                    <span class="b-top-panel__feedback-stars">
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                    </span>
                                    <span><?=getNumEnding($arResult['STAT_COMMENTS'][1]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                                </li>
                                <li>
                                    <span class="b-top-panel__feedback-stars">
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                    </span>
                                    <span><?=getNumEnding($arResult['STAT_COMMENTS'][2]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                                </li>
                                <li>
                                    <span class="b-top-panel__feedback-stars">
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                    </span>
                                    <span><?=getNumEnding($arResult['STAT_COMMENTS'][3]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                                </li>
                                <li>
                                    <span class="b-top-panel__feedback-stars">
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                    </span>
                                    <span><?=getNumEnding($arResult['STAT_COMMENTS'][4]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                                </li>
                                <li>
                                    <span class="b-top-panel__feedback-stars">
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                    </span>
                                    <span><?=getNumEnding($arResult['STAT_COMMENTS'][5]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="b-params__tabs-tab">3</div>-->
        <!--<div class="b-params__tabs-tab">4</div>-->
        <div class="clear"></div>
    </div>
</div>

<?if (count($arResult['SAME_PRODUCTS']) > 0) {?>
    <!-- Цвета и модификации -->
    <div class="b-colors" id="b-colors" style="max-height: 440px;<?if (count($arResult['SAME_PRODUCTS']) > 5) {?>overflow-y:scroll;<?}?>">
        <h2>Цвета и модификации</h2>
        <table class="b-colors__table">
            <thead>
            <tr>
                <th style="width:7%;">Фото</th>
                <th style="width:25%;">Название</th>
                <th style="width:9%;">Цена</th>
                <th style="width:10%;">Цвет</th>
                <th style="width:9%;">Рейтинг</th>
                <th style="width:14%;">Размеры</th>
                <th style="width:10%;">Серия</th>
                <th style="width:9%;"> </th>
                <th style="width:7%;"> </th>
            </tr>
            </thead>
            <tbody>
            <?foreach ($arResult['SAME_PRODUCTS'] as $sameProduct) {
                $arFileTmp = CFile::ResizeImageGet(
                    CFile::GetFileArray($sameProduct['DETAIL_PICTURE']),
                    array("width" => 52, "height" => 41),
                    BX_RESIZE_IMAGE_EXACT,
                    true,
                    false
                );
                ?>
                <tr>
                    <td><a href="<?=$sameProduct['DETAIL_PAGE_URL']?>"><img src="<?=$arFileTmp['src']?>" alt="" style="width: 52px; height: 41px;" /></a></td>
                    <td><a href="<?=$sameProduct['DETAIL_PAGE_URL']?>"><?=$sameProduct['NAME']?></a></td>
                    <td><span style="font-weight: bold;font-size:16px;"><?=FormatCurrency($sameProduct['CATALOG_PRICE_1'], 'RUB')?></span></td>
                    <td class="b-colors__td-colors"><img src='/colors/<?=$sameProduct['PICTURE']?>.jpg' alt="" style="display: inline-block;background-position: 0% center;background-size: 18px;width:18px;height:18px;border-radius: 3px;cursor: pointer;" /><span><?=$sameProduct['PROPERTY_COLORTEXT_VALUE']?></span></td>
                    <td>
                        <?
                        $resComments = CIBlockSection::GetList(
                            array('DATE_CREATED' => 'DESC'),
                            array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID_VALUE' => $sameProduct['ID']),
                            false,
                            false,
                            array('PROPERTY_RATING'));
                        $totalSameRating = 0;
                        $totalSameCount = 0;
                        while ($comment = $resComments->Fetch()) {
                            $totalSameRating += $comment['PROPERTY_RATING_VALUE'];
                            $totalSameCount++;
                        }

                        if ($totalSameCount > 0) {
                            $totalSameRating = floor($totalSameRating / $totalSameCount);
                        } else {
                            $totalSameRating = 1;
                        }
                        ?>
                        <?if ($totalSameRating > 0) {?>
                            <span class="b-top-panel__feedback-stars">
                            <?for ($i = 0; $i < $totalSameRating; $i++) {?>
                                <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <?}?>
                            <?for ($i = $totalSameRating; $i < 5; $i++) {?>
                                <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                            <?}?>
                            </span>
                        <?}?>
                    </td>
                    <td>Дл. <?=$sameProduct['PROPERTY_SIZE1_VALUE']?>, Гл. <?=$sameProduct['PROPERTY_SIZE2_VALUE']?>,  Выс. <?=$sameProduct['PROPERTY_SIZE3_VALUE']?></td>
                    <td><?=$sameProduct['PROPERTY_SER_VALUE']?></td>
                    <td><a class="js-button btn btn-y add2basket" item-id="<?=$sameProduct['ID']?>" href="/catalog/compare?action=ADD2BASKET&id=<?=$sameProduct['ID']?>">Купить</a></td>
                    <td>
                        <a href="/catalog/compare?action=ADD_TO_COMPARE_LIST&id=<?=$sameProduct['ID']?>" class="b-goods__good-more-price-ico b-goods__good-more-price-ico2"></a>
                        <a href="javascript:void(0)" item-id="<?=$sameProduct['ID']?>" class="b-goods__good-more-price-ico b-goods__good-more-price-ico1 add-to-favorites"></a>
                    </td>
                </tr>
            <?}?>
            </tbody>
        </table>
    </div>
<?}?>

<?if (count($arResult['SIMILAR_PRODUCTS']) > 0) {?>
    <!-- Похожие товары -->
    <div class="b-discount" id="b-discount">
        <h2>Похожие товары</h2>

        <div class="clear"></div>
        <div class="b-accessories__slider">
            <div class="b-accessories__slider-wrapper">
                <ul>
                    <?foreach ($arResult['SIMILAR_PRODUCTS'] as $similarProduct) {
                        $arFileTmp = CFile::ResizeImageGet(
                            CFile::GetFileArray($similarProduct['DETAIL_PICTURE']),
                            array("width" => 183, "height" => 147),
                            BX_RESIZE_IMAGE_EXACT,
                            true,
                            false
                        );
                        ?>
                        <li>
                            <a href="<?=$similarProduct['DETAIL_PAGE_URL']?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                            <a href="<?=$similarProduct['DETAIL_PAGE_URL']?>"><p><?=$similarProduct['NAME']?></p></a>
                            <span><?=FormatCurrency($similarProduct['CATALOG_PRICE_1'], 'RUB')?></span>
                            <a class="compare" href="/catalog/compare?action=ADD_TO_COMPARE_LIST&id=<?=$similarProduct['ID']?>"><span>Сравнить</span></a>
                        </li>
                    <?}?>
                </ul>
            </div>
            <a class="b-accessories__slider-button b-accessories__slider-button-prev" href="javascript:void(0)"></a>
            <a class="b-accessories__slider-button b-accessories__slider-button-next" href="javascript:void(0)"></a>
        </div>
    </div>
<?}?>
<?if (count($arResult['BANNERS']) > 0) {?>
    <!-- Баннеры -->
    <div class="content-block-slider">
        <div class="flexslider">
            <ul class="slides">
            <?
            $class = 'b-banners__banner-current';
            foreach ($arResult['BANNERS'] as $banner) {
            ?>
                <li class="b-banners__banner <?=$class?>"><img src="<?=CFile::GetPath($banner['PREVIEW_PICTURE'])?>" /></li>
            <?
                $class = '';
            }
            ?>
            </ul>
        </div>
    </div>
<!--        <div class="b-banners__links-wrapper">-->
<!--            <ul class="b-banners__links">-->
<!--                --><?//
//                $class = 'b-banners__link-current';
//                foreach ($arResult['BANNERS'] as $banner) {
//                ?>
<!--                    <li class="b-banners__link --><?//=$class?><!--"></li>-->
<!--                --><?//
//                    $class = '';
//                }
//                ?>
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->
<?}?>
<?if (count($arResult['VIEWED_PRODUCTS']) > 0) {?>
    <!-- Вы уже смотрели -->
    <div class="b-yousaw" id="b-yousaw">
        <h2>Вы уже смотрели</h2>
        <div class="b-goods__more-like-slider">
            <ul>
                <?foreach ($arResult['VIEWED_PRODUCTS'] as $viewedProduct) {
                    $arFileTmp = CFile::ResizeImageGet(
                        CFile::GetFileArray($viewedProduct['DETAIL_PICTURE']),
                        array("width" => 89, "height" => 89),
                        BX_RESIZE_IMAGE_EXACT,
                        true,
                        false
                    );
                    ?>
                    <li>
                        <div>
                            <a href="<?=$viewedProduct['DETAIL_PAGE_URL']?>"><img src="<?=$arFileTmp['src']?>" alt="" /></a>
                            <p><a href="<?=$viewedProduct['DETAIL_PAGE_URL']?>"><?=(strlen($viewedProduct['NAME']) > 50) ? substr($viewedProduct['NAME'], 0, 50) . ' ...' : $viewedProduct['NAME']?></p></a>
                            <p><?=FormatCurrency($viewedProduct['CATALOG_PRICE_1'], 'RUB')?>&nbsp;</p>
                        </div>
                    </li>
                <?}?>
            </ul>
            <a class="b-goods__more-like-slider-button b-goods__more-like-slider-button-prev" href="javascript:void(0)"></a>
            <a class="b-goods__more-like-slider-button b-goods__more-like-slider-button-next" href="javascript:void(0)"></a>
        </div>
        <div class="clear"></div>
    </div>
<?}?>
<?if (count($arResult['ALSO_PRODUCTS']) > 0) {?>
    <div class="b-alsobuy" id="b-alsobuy">
        <h2>Люди, купившие этот товар, также купили</h2>
        <div class="clear"></div>
        <div class="b-accessories__slider">
            <div class="b-accessories__slider-wrapper">
                <ul>
                    <?foreach ($arResult['ALSO_PRODUCTS'] as $alsoProduct) {
                        $arFileTmp = CFile::ResizeImageGet(
                            CFile::GetFileArray($alsoProduct['DETAIL_PICTURE']),
                            array("width" => 183, "height" => 147),
                            BX_RESIZE_IMAGE_EXACT,
                            true,
                            false
                        );
                        ?>
                        <li>
                            <a href="<?=$alsoProduct['DETAIL_PAGE_URL']?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                            <p><a href="<?=$alsoProduct['DETAIL_PAGE_URL']?>"><?=$alsoProduct['NAME']?></a></p>
                            <span><?=FormatCurrency($alsoProduct['CATALOG_PRICE_1'], 'RUB')?></span>
                        </li>
                    <?}?>
                </ul>
            </div>
            <a class="b-accessories__slider-button b-accessories__slider-button-prev" href="javascript:void(0)"></a>
            <a class="b-accessories__slider-button b-accessories__slider-button-next" href="javascript:void(0)"></a>
        </div>
        <div class="clear"></div>
    </div>
<?}?>

    <div class="b-feedbacks" id="b-feedbacks">
        <h2>Отзывы и оценки покупателей</h2>
        <div class="b-feedbacs__main">
            <!--<div class="b-feedbacs__sortby">
                <span>Сортировать по:</span>
                <a href="javascript:void(0)">Дате</a>
                <a class="b-feedbacs__sortby-current js-button js-button-gray" href="javascript:void(0)">Количеству звезд</a>
            </div>-->
            <div class="b-feedbacks__feedbacks-feedback">
                <ul>
                    <?foreach(array_slice($arResult['SHOW_COMMENTS'], 0, 3) as $comment) {?>
                        <li>
                            <div class="b-feedbacks__feedback-info">
                                <span class="b-top-panel__feedback-stars">
                                    <?for ($i = 0; $i < $comment['PROPERTY_RATING_VALUE']; $i++) {?>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                    <?}?>
                                    <?for ($i = $comment['PROPERTY_RATING_VALUE']; $i < 5; $i++) {?>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                    <?}?>
                                </span>
                                <h4><?=$comment['PROPERTY_FIO_VALUE']?></h4>
                                <p><?=$comment['DATE_CREATED']?></p>
                            </div>
                            <div class="b-feedbacks__feedback-text">
                                <p class="b-feedbacks__feedback-text-main">
                                    <?=$comment['PROPERTY_COMMENT_VALUE']['TEXT']?>
                                </p>
                                <p class="b-feedbacks__feedback-text-plus"><?=$comment['PROPERTY_PLUS_VALUE']['TEXT']?></p>
                                <p class="b-feedbacks__feedback-text-minus"><?=$comment['PROPERTY_MINUS_VALUE']['TEXT']?></p>
                            </div>
                        </li>
                    <?}?>
                </ul>
                <?if (count($arResult['SHOW_COMMENTS']) > 0) {?>
                    <a href="javscript:void(0)" class="js-button-gray js-button">Все отзывы</a>
                <?} else {?>
                    Еще нет отзывов
                <?}?>
            </div>
            <div class="b-feedbacks__right">
                <a id="feedback-create" class="btn btn-y js-popup" data-id="feedback-create" data-callback="putSrars" data-good-id="<?=$arResult['ID']?>" data-good-name="<?=$arResult['NAME']?>"  href="javascript:void(0)">Добавить отзыв</a>
                <h4>Оценка пользователей</h4>
                <span class="b-top-panel__feedback-stars-big">
                    <?for ($i = 0; $i < $totalRating; $i++) {?>
                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold-big"></span>
                    <?}?>
                    <?for ($i = $totalRating; $i < 5; $i++) {?>
                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray-big"></span>
                    <?}?>
                </span>
                <p><?=getNumEnding($arResult['RATING_COMMENTS'], array('оценка', 'оценки', 'оценок'))?></p>
                <h4>Отзывов с оценкой</h4>
                <ul class="b-top-panel__feedback-stars-list">
                    <li>
                        <span class="b-top-panel__feedback-stars">
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                        </span>
                        <span><?=getNumEnding($arResult['STAT_COMMENTS'][1]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                    </li>
                    <li>
                        <span class="b-top-panel__feedback-stars">
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                        </span>
                        <span><?=getNumEnding($arResult['STAT_COMMENTS'][2]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                    </li>
                    <li>
                        <span class="b-top-panel__feedback-stars">
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                        </span>
                        <span><?=getNumEnding($arResult['STAT_COMMENTS'][3]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                    </li>
                    <li>
                        <span class="b-top-panel__feedback-stars">
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                        </span>
                        <span><?=getNumEnding($arResult['STAT_COMMENTS'][4]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                    </li>
                    <li>
                        <span class="b-top-panel__feedback-stars">
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                        </span>
                        <span><?=getNumEnding($arResult['STAT_COMMENTS'][5]['COUNT'], array('оценка', 'оценки', 'оценок'))?></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="good-added" style="display: none">
    <div class="good-added">
        <h2>Товар добавлен в корзину</h2>
        <table class="b-basket">
            <tbody>
            <tr>
                <td class="b-basket__img" id="item_img"><img src="<?=$arFileTmp['src']?>" alt=""/></td>
                <td class="b-basket__name">
                    <h4 id="item_name"><?=$arResult['NAME']?></h4>
                </td>
                <td class="b-basket__price"><span id="item_discount_price"><?=$arResult['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']?></span></td>
                <td class="b-basket__summ" style="text-align: center">
                    <span id="item_count"></span> <span> шт.</span>
                </td>
                <td class="b-basket__summ">
                    <span id="item_price"></span>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="good-added__elems">
            <a  class="js-button js-button-yellow left" href="/personal/cart">Перейти в корзину</a>
            <a class="left" href="javascript:void(0)">или продолжить покупки</a>
            <p>Всего <span class="quantityTotalBasket"></span> на сумму <span class="priceTotalBasket"></span></p>
            <div class="clear"></div>
        </div>
        <?if (count($arResult['SIMILAR_PRODUCTS']) > 0) {?>
            <div class="delimiter-yellow"></div>
            <div class="b-goods__more-like">
                <h3>Похожие товары</h3>
                <div class="clear"></div>
                <div class="b-accessories__slider">
                    <div class="b-accessories__slider-wrapper">
                        <ul>
                            <?foreach ($arResult['SIMILAR_PRODUCTS'] as $similarProduct) {
                                $arFileTmp = CFile::ResizeImageGet(
                                    CFile::GetFileArray($similarProduct['DETAIL_PICTURE']),
                                    array("width" => 183, "height" => 147),
                                    BX_RESIZE_IMAGE_EXACT,
                                    true,
                                    false
                                );
                                ?>
                                <li>
                                    <a href="<?=$similarProduct['DETAIL_PAGE_URL']?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                                    <a href="<?=$similarProduct['DETAIL_PAGE_URL']?>"><p><?=$similarProduct['NAME']?></p></a>
                                    <span><?=FormatCurrency($similarProduct['CATALOG_PRICE_1'], 'RUB')?></span>
                                </li>
                            <?}?>
                        </ul>
                    </div>
                    <a class="b-accessories__slider-button b-accessories__slider-button-prev" href="javascript:void(0)"></a>
                    <a class="b-accessories__slider-button b-accessories__slider-button-next" href="javascript:void(0)"></a>
                </div>
            </div>
            <div class="clear"></div>
        <?}?>
    </div>
</div>
    <div id="acc-added" style="display: none">
        <div class="good-added">
            <h2>Товары добавлены в корзину</h2>
            Ваши товары добавлены в корзину
            <div class="good-added__elems">
                <a  class="js-button js-button-yellow left" href="/personal/cart">Перейти в корзину</a>
                <a class="left" href="javascript:void(0)">или продолжить покупки</a>
                <p>Всего <span class="quantityTotalBasket"></span> на сумму <span class="priceTotalBasket"></span></p>
                <div class="clear"></div>
            </div>
            <?if (count($arResult['SIMILAR_PRODUCTS']) > 0) {?>
                <div class="delimiter-yellow"></div>
                <div class="b-goods__more-like">
                    <h3>Похожие товары</h3>
                    <div class="clear"></div>
                    <div class="b-accessories__slider">
                        <div class="b-accessories__slider-wrapper">
                            <ul>
                                <?foreach ($arResult['SIMILAR_PRODUCTS'] as $similarProduct) {
                                    $arFileTmp = CFile::ResizeImageGet(
                                        CFile::GetFileArray($similarProduct['DETAIL_PICTURE']),
                                        array("width" => 183, "height" => 147),
                                        BX_RESIZE_IMAGE_EXACT,
                                        true,
                                        false
                                    );
                                    ?>
                                    <li>
                                        <a href="<?=$similarProduct['DETAIL_PAGE_URL']?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                                        <a href="<?=$similarProduct['DETAIL_PAGE_URL']?>"><p><?=$similarProduct['NAME']?></p></a>
                                        <span><?=FormatCurrency($similarProduct['CATALOG_PRICE_1'], 'RUB')?></span>
                                    </li>
                                <?}?>
                            </ul>
                        </div>
                        <a class="b-accessories__slider-button b-accessories__slider-button-prev" href="javascript:void(0)"></a>
                        <a class="b-accessories__slider-button b-accessories__slider-button-next" href="javascript:void(0)"></a>
                    </div>
                </div>
                <div class="clear"></div>
            <?}?>
        </div>
    </div>
<script type="text/x-template" id="good-gallery">
    <div class="good-gallery">
        <div class="b-goods__good-photo">
            <div class="b-goods__good-photo-main">
<!--                <a href="--><?//=$arResult['DETAIL_PICTURE']['SRC']?><!--">-->
                    <img class="catalog-img-new" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" style="display: block;">
<!--                </a>-->
            </div>
            <div class="b-goods__good-photo-thumbs">
                <ul>
                    <?
                    $arFilter = array(array("name" => "sharpen", "precision" => 30));
                    $arFileTmp = CFile::ResizeImageGet(
                        $arResult['DETAIL_PICTURE'],
                        array("width" => 70, "height" => 70),
                        BX_RESIZE_IMAGE_EXACT,
                        true,
                        $arFilter
                    );
                    ?>
                    <li class="b-goods__good-photo-thumbs-current">
                        <a href="javascript:void(0)" data-img="<?=$arResult['DETAIL_PICTURE']['SRC']?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                    </li>
                    <?foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $file) {
                        $arFileTmp = CFile::ResizeImageGet(
                            CFile::GetFileArray($file),
                            array("width" => 70, "height" => 70),
                            BX_RESIZE_IMAGE_EXACT,
                            true,
                            $arFilter
                        );
                        ?>
                        <li>
                            <a href="javascript:void(0)" data-img="<?=CFile::GetPath($file)?>"><img src="<?=$arFileTmp['src']?>" alt=""/></a>
                        </li>
                    <?}?>
                </ul>
                <a class="b-accessories__slider-button b-accessories__slider-button-prev" href="javascript:void(0)"></a>
                <a class="b-accessories__slider-button b-accessories__slider-button-next" href="javascript:void(0)"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</script>

<?
if ($arResult['FEEDBACK_ADDED']) {
?>
    <input type="hidden" id="commentAdded" />
<?
}
?>