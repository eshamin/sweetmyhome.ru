<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ORDER"]))
{
	?>
<div class="b-basket__inner-wrapper b-order-create">
    <h1>Заказ успешно оформлен</h1>
    <p>В ближайшее время наш менеджер обязательно свяжется с вами для уточнения деталей доставки заказа.<br />
       После уточнения заказа менеджером вам поступит уведомление на телефон и электронную почту о номере заказа, дате и времени доставки
    </p>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
		?>
    <div class="b-order__pay">
        <?
        if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
        {
            if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
            {
                ?>
                <script language="JavaScript">
                    window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
                </script>
                <?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
                <div class="clear"></div>
                <p>Если Вы совершали оплату товара при помощи банковской карты, то возврат денежных средств осуществляется на ту же банковскую карту,
                    с которой производился платеж. Возврат производится на основании утвержденного обращения в отдел рекламаций Компании</p>
                <?
                if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
                {
                    ?><br />
                    <?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
                    <?
                }
            }
            else
            {
                if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
                {
                    try
                    {
                        include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                    }
                    catch(\Bitrix\Main\SystemException $e)
                    {
                        if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
                            $message = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
                        else
                            $message = $e->getMessage();

                        echo '<span style="color:red;">'.$message.'</span>';
                    }
                }
            }
        }
	}
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
