<?
if (!$_SESSION['CLIENT_NUM']) {
    $_SESSION['CLIENT_NUM'] = rand(111111, 999999);
}

if (isset($_GET['location'])) {
    $GLOBALS['locationId'] = $_GET['location'];
} elseif (isset($_SESSION['LOCATION_ID'])) {
    $GLOBALS['locationId'] = $_SESSION['LOCATION_ID'];
} elseif (isset($_COOKIE['LOCATION_ID'])) {
    $GLOBALS['locationId'] = $_COOKIE['LOCATION_ID'];
} else {
    $GLOBALS['locationId'] = 19;
}
$GLOBALS['locationName'] = CSaleLocation::GetCityByID($GLOBALS['locationId']);
$GLOBALS['locationName'] = $GLOBALS['locationName']['NAME'];

setcookie('LOCATION_ID', $GLOBALS['locationId'], time() + 365 * 24 * 3600);
$_SESSION['LOCATION_ID'] = $GLOBALS['locationId'];

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title><?=$APPLICATION->ShowTitle()?></title>
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="<?=SITE_TEMPLATE_PATH?>/css/style.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/flexslider.css" media="screen" rel="stylesheet" type="text/css" />
<!--    <link rel="stylesheet" href="--><?//=SITE_TEMPLATE_PATH?><!--/css/masonry-docs.css" media="screen">-->
    <link href="<?=SITE_TEMPLATE_PATH?>/css/new.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.ui.custom.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/styles.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/styles.css.map" media="screen" rel="stylesheet" type="text/css" />

<!--    <script type="text/javascript" src="--><?//=SITE_TEMPLATE_PATH?><!--/js/libs/jquery-1.11.3.min.js"></script>-->
<!--    <script type="text/javascript" src="--><?//=SITE_TEMPLATE_PATH?><!--/js/libs/jquery-migrate-1.2.1.min.js"></script>-->
<!--    <script type="text/javascript" src="--><?//=SITE_TEMPLATE_PATH?><!--/js/libs/bootstrap.min.js"></script>-->
<!--    <script type="text/javascript" src="--><?//=SITE_TEMPLATE_PATH?><!--/js/libs/jquery.flexslider-min.js"></script>-->
<!--    <script type="text/javascript" src="--><?//=SITE_TEMPLATE_PATH?><!--/js/libs/masonry-docs.min.js"></script>-->

    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendors.js"></script>
<!--    <script type="text/javascript" src="--><?//=SITE_TEMPLATE_PATH?><!--/js/vendors.js.map"></script>-->
<!--    <script type="text/javascript" src="--><?//=SITE_TEMPLATE_PATH?><!--/js/popup.js"></script>-->
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/scripts.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/app.js"></script>
<!--    <script type="text/javascript" src="--><?//=SITE_TEMPLATE_PATH?><!--/js/templates.js"></script>-->

<!--<script src="http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU" type="text/javascript"></script>-->

    <?$APPLICATION->ShowHead();?>
</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div class="wrapper">
    <div class="container">
        <div class="header">
            <div class="header-wrapper">
                <div class="info-panel">
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "mainMenu", Array(
                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MAX_LEVEL" => "2",	// Уровень вложенности меню
                        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    ),
                        false
                    );?>
                    <ul class="info-list b-infolist g-right">
                        <li class="dropdown-menu-toggle" data-menu="location">
                            <span class="info-label">Мой город: </span><span class="info-location"><?=$GLOBALS['locationName']?></span><span class="ico ico-angle-down"></span>
                            <div class="b-dropright-catalog">
                                <h2>Ваш город — <span class="location"><?=$GLOBALS['locationName']?></span>?</h2>
                                <a class="btn btn-y b-infolist__button js-infolist__confirm" href="javascript:void(0)">Да</a>
                                <a class="btn btn-y b-infolist__button js-infolist__choice" href="javascript:void(0)">Выбрать другой город</a>
                                <p class="b-infolist__text g-muted">От выбранного города зависят цены, наличие товара и способы доставки</p>
                                <input type="hidden" value="<?=$GLOBALS['locationName']?>" name="selected_city"/>
                            </div>
                        </li>
                        <li class="blue">
                            <a href="/personal/"></span><span class="info-cabinet">Личный кабинет</span><span class="ico ico-angle-down"></a>
                            <ul class="b-dropright-catalog">
                                <li><a href="#">Войти</a></li>
                                <li><a href="#">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="contacts-panel" itemscope itemtype="http://schema.org/Store">
                    <ul class="contacts-list">
                        <li>
                        <?if (!$isMain) {?>
                            <a href="/">
                        <?}?>
                                <img class="logo" src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" itemprop="logo" alt=""/>
                                <input type="hidden" itemprop="name" value="SweetMyHome">
                                <input type="hidden" itemprop="address" value="г.Москва">
                        <?if (!$isMain) {?>
                            </a>
                        <?}?>
                        </li>
                    </ul>
                    <ul class="right-element">
                        <?$APPLICATION->IncludeFile('include_areas/freePhone.php')?>
                        <?$APPLICATION->IncludeFile('include_areas/contactsPhone.php')?>
                    </ul>
                </div>

                <div class="b-catalog-panel">
                    <div class="b-catalog-panel__main-menu b-main-menu">
                        <a href='/catalog/' class="btn btn-y dropdown-catalog-toggle b-main-menu__btn">КАТАЛОГ ТОВАРОВ<span class="b-main-menu__btn--ico"></span></a>

                        <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "topMenu", array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => CATALOG_IB,
                            "SECTION_ID" => "",
                            "SECTION_CODE" => "",
                            "COUNT_ELEMENTS" => "N",
                            "TOP_DEPTH" => "3",
                            "SECTION_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SECTION_USER_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SECTION_URL" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000",
                            "CACHE_GROUPS" => "N",
                            "ADD_SECTIONS_CHAIN" => "N"
                        ),
                            false
                        );?>
                    </div>
                    <div class="catalog-search">
                        <form id="search-form" action="/search" method="get">
                            <input name="q" class="form-control query" type="text" autocomplete="off"  value="<?=urldecode($_GET['q']);?>">
                            <input type="submit" class="btn btn-w text-center" value="НАЙТИ">
                        </form>
                    </div>
                    <div class="delivery-status">
                        <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "smallTopCart", Array(
                            "PATH_TO_BASKET" => "/personal/cart/",	// Страница корзины
                            "PATH_TO_ORDER" => "/personal/order/",	// Страница оформления заказа
                            "SHOW_DELAY" => "N",	// Показывать отложенные товары
                            "SHOW_NOTAVAIL" => "N",	// Показывать товары, недоступные для покупки
                            "SHOW_SUBSCRIBE" => "N",	// Показывать товары, на которые подписан покупатель
                        ),
                            false
                        );?>
                    </div>

                </div>
            </div>
        </div>
        <div class="content">
            <div class="content-wrapper">