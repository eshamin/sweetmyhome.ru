function heightTinyFix(){
	if( $( '.b-goods__good-more-about').length ){
		var good_hei = $( '.b-goods__good-more-about' ).outerHeight();
		good_hei = ( good_hei < 400 ) ? 400:good_hei;
		$( '.b-goods__good').css( { "min-height":good_hei } );
	};
}

function photoZoom(){
	var $zoom = $( '.b-goods__good-photo-zoom' );
	if( !$zoom ){
		return false;
	}
	$zoom.on( 'click', function(){
		MODAL.showOverlay();
		$( '.b-overlay' ).css( { "background":"#353535" } );
		var id = 'good-gallery';
		renderPopupHTML( '#' + id, null, 'b-popup__good' );
	} );
}

function priceDelWarTabsStart(){
	var $del_container = $( '.b-goods__good-more-delivery' );
	if( !$del_container ){
		return false;
	}
	var $delivery_tablinks = $del_container.find( '.b-makeorder__tablinks_2').find( 'a' );
	var $delivery_tabs = $del_container.find( '.b-makeorder__tabs' );

	$delivery_tablinks.on( 'click', function(){
		var $that = $( this );
		var indice = $that.index();

		if( $that.hasClass( 'b-makeorder__currenttab' ) ){
			return false;
		}
		$del_container.find( '.b-makeorder__currenttab' ).removeClass( 'b-makeorder__currenttab' );
		$del_container.find( '.b-makeorder__tabs-current').removeClass( 'b-makeorder__tabs-current' );
		$that.addClass( 'b-makeorder__currenttab' );
		$delivery_tabs.eq( indice).addClass( 'b-makeorder__tabs-current' );
	} );
}

function productThumbStart(){
	var $thumbs = $( '.b-goods__good-photo-thumbs li' );
	if( !$thumbs ){
		return false;
	}
	$thumbs.on( 'click', function(){
		var $that = $( this );
		$( '.b-goods__good-photo-thumbs-current').removeClass( 'b-goods__good-photo-thumbs-current' );
		$that.addClass( 'b-goods__good-photo-thumbs-current' );
		var src = $that.find( 'a' ).data( 'img' );
		$( '.b-goods__good-photo-main a' ).prop( 'href', src );
		$( '.b-goods__good-photo-main a img' ).prop( 'src', src );
		$( '.cloud-zoom, .cloud-zoom-gallery' ).CloudZoom();
	} );
}

function cityChoiceStart(){
	var $city_panel_toggler = $( '.b-infolist' ).find( '.dropdown-menu-toggle' );
	var $city_panel = $city_panel_toggler.find( '.b-dropright-catalog' );
	$city_panel_toggler.on( 'mouseover', function(){
		$city_panel.fadeIn();
	} );
	$city_panel_toggler.on( 'mouseleave', function(){
		$city_panel.fadeOut();
	} );
	$( '.js-infolist__confirm' ).on( 'click', function(){
		$city_panel.fadeOut();
		var city = $city_panel.find( 'input[name=selected_city]' ).val();
		$city_panel_toggler.find( '.info-location' ).html( city );
	} );
	$( '.js-infolist__choice').on( 'click', function(){
		$city_panel.fadeOut();
		var src = MyApp[ 'templates' ][ 'popup' ][ 'yourcitylist' ];
		var HTML = src();
		MODAL.showOverlay();
		MODAL.showPopup( HTML );
	} );
	$( document ).on( 'click', '.abc-city-list a, .preselected-city a', function(){
		var $that = $( this );
		var val = $that.html();
		$( 'input[name="selected_city"]' ).val( val );
		$( '.b-infolist .location').html( val );
		MODAL.closePopup();
		MODAL.closeOverlay();
		$( '.js-infolist__confirm' ).trigger( 'click' );
	} );
	$(document).on('click', '.showCities', function() {
		var letter = $(this).html();
		var postData = {
			'action': 'getCities',
			'letter': letter
		};

		BX.ajax({
			url: '/ajax/index.php',
			method: 'POST',
			data: postData,
			dataType: 'json',
			onsuccess: function(result)
			{
				BX.closeWait();
				if (result['CODE'] == 'SUCCESS') {
					$('.abc-city-list').html(result['CITIES']);
				}
			}
		});
	});
}

function callBackStart(){
	$( '.contacts-callback' ).on( 'click', function(){
		var src = MyApp[ 'templates' ][ 'popup' ][ 'oneclickcall' ];
		var HTML = src();
		MODAL.showOverlay();
		MODAL.showPopup( HTML );
		makeDateTimePicker();
		$("#user-phone").mask("7(999)999-9999");
	} );
}

function tabLinksStart(){
	var $tablink = $( '.b-params-tabs-link' );
	if( !$tablink ){
		return false;
	}
	$tablink.on( 'click', function(){
		var $tab = $( '.b-params__tabs-tab-current' );
		var $that = $( this );
		if( $that.hasClass( 'b-params-tabs-link-current' ) ){
			return false;
		};
		var indice = $that.index();
		$( '.b-params-tabs-link-current' ).removeClass( 'b-params-tabs-link-current' );
		$tab.removeClass( 'b-params__tabs-tab-current' );
		$tablink.eq( indice).addClass( 'b-params-tabs-link-current' );
		$( '.b-params__tabs-tab' ).eq( indice).addClass( 'b-params__tabs-tab-current' );

		/* поправим высоту параметров */
		tab_hei = $tab.outerHeight();
		if( tab_hei > 727 ) {
			$('.b-params').css({"height": tab_hei});
		}
		/* поправим высоту параметров */
	} );
}

function masonryInit(){
	// Инициализация Масонри, после загрузки изображений
	var $container = $('.grid');
	if(!$container){
		return false;
	}
	$container.imagesLoaded( function() {
		$container.masonry({
			columnWidth: '.grid-sizer',
			itemSelector: '.grid-item',
			percentPosition: true
		});
	});
}

function spinnerStart(){
	$( '.b-goods__good-more-buy .js-digital-spinner-up').on( 'click', function(){
		var $that = $( this );
		var $input = $that.closest( '.js-digital-spinner').find( 'input' );
		var val = parseInt( $input.val() );
		if( !val || val == 0 ){
			val = 1;
			$input.val( val );
		};
		val++;
		$input.val( val );
	} );
	$( '.b-goods__good-more-buy .js-digital-spinner-down').on( 'click', function(){
		var $that = $( this );
		var $input = $that.closest( '.js-digital-spinner').find( 'input' );
		var val = parseInt( $input.val() );
		if( !val || val == 0 ){
			val = 1;
			$input.val( val );
		};
		if( val == 1 ){
			return false;
		};
		val--;
		$input.val( val );
	} );
}

function addOneClickOrder(){
	$('#order-oneclick').on('click', function(e) {
		var phone_maska=/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
		$phone = $("#oneclick-phone");
		phone = $phone.val();
		if (!(phone_maska.test(phone))){
			$phone.addClass('input-error');
		}else{
			$phone.removeClass('input-error');
			postData = {
				action: 'addOneClickOrder',
				mobile: $phone.val(),
				productId: $(this).attr('item-id')
			};
			BX.ajax({
				url: '/ajax/',
				method: 'POST',
				data: postData,
				dataType: 'json',
				onsuccess: function (result) {
					BX.closeWait();
					if (result['CODE'] == 'SUCCESS') {
						var src = MyApp[ 'templates' ][ 'popup' ][ 'oneclickordercreated' ];
						var HTML = src( { "order":result['ORDER_ID'] } );
						MODAL.showOverlay();
						MODAL.showPopup( HTML );
					} else {
						alert(result['MESSAGE']);
					}
				}
			});
		}
		return false;
	});
}

function validateOneClickForm(){
	var oneClickForm = $( '#one-click' );
	if( !oneClickForm ){
		return false;
	}
	var requiredInputs = oneClickForm.find( '.required' ).find( 'input' );
	var is_validate = true;
	requiredInputs.each( function(){
		var that = $( this );
		if( that.val() == '' ){
			that.addClass( 'b-input__error' );
			is_validate = false;
		}
		else{
			that.removeClass( 'b-input__error' );
		}
	} );
	return is_validate;
}

function makeDateTimePicker() {
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '<Пред',
		nextText: 'След>',
		currentText: 'Сегодня',
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
		dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
		dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		weekHeader: 'Не',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};

	$.datepicker.setDefaults($.datepicker.regional['ru']);

	$("#call-date").datepicker({minDate: 0, maxDate: "+3D"});

	$('#call-time').timepicker({
		showPeriodLabels: false, // показывать обозначения am и pm
		hourText: 'Часы', // название заголовка часов
		minuteText: 'Минуты', // название заголовка минут
		showMinutes: true, // показывать блок с минутами
		rows: 4, // сколько рядов часов и минут выводить
		timeSeparator: ':', // разделитель часов и минут
		defaultTime: '', // не подсвечивать время
		hours: {
			starts: 10, // настройка часов
			ends: 20 // от - до
		},
		minutes: {
			starts: 0, // настройка минут
			ends: 40, // от - до
			interval: 20 // интервал между минутами
		},
		onHourShow: function (hour) {
			var check_dat = $('#call-date').datepicker('getDate');
			if (check_dat) {
				var dat = new Date();
				var day_now = dat.getDate();
				var day_selected = check_dat.getDate();
				if (day_now < day_selected) {
					return true;
				}
				else {
					var hour_now = dat.getHours();
					if (hour < hour_now) {
						return false;
					}
					else {
						if (hour == hour_now) {
							return dat.getMinutes() <= 30;
						}
						else {
							return true;
						}
					}
				}
			}
			else {
				return false;
			}
		},
		onMinuteShow: function (hour, minute) {
			var check_dat = $('#call-date').datepicker('getDate');
			if (check_dat) {
				var dat = new Date();
				var day_now = dat.getDate();
				var day_selected = check_dat.getDate();
				if (day_now < day_selected) {
					return true;
				}
				var hour_now = dat.getHours();
				if (hour > hour_now) {
					return true;
				}
				else {
					var min_now = dat.getMinutes();
					if (minute < min_now) {
						return false;
					}
					else {
						return (minute - min_now) > 10;
					}
				}
			}
			else {
				return false;
			}
		}
	});
	/* Call back modal section */

	$(document).on('keydown', '#call-time', function () {
		return false;
	});
	$(document).on('keydown', '#call-date', function () {
		return false;
	});

	$(document).on('change', '#call-date', function () {
		$('#call-time').val('');
	});
}

