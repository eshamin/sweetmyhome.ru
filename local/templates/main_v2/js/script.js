jQuery(document).ready(function(){
	jQuery('.grid').masonry({
		columnWidth: '.grid-sizer',
		itemSelector: '.grid-item',
		percentPosition: true

	});	
})
jQuery(window).load(function() {
	jQuery('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
	fixHeight()
});
jQuery(window).resize(function() {
	fixHeight()
});
// jQuery(window).scroll(function(){
//     jQuery('.fixed-panel').css('left', originalLeft - jQuery(this).scrollLeft());
// });





jQuery(window).scroll(function(event) {   
    jQuery('#fixed-panel').css('left',- $(this).scrollLeft()+'px')
});

function fixHeight() {
	var maxHeight = jQuery('.grid .max-height').height();
	var smallHeight = (maxHeight -  (jQuery('.grid .min-height > a:first-child img').height() + 23 + 83*2)) /4;
	jQuery('.grid .sub').each(function(){
		jQuery(this).css('padding-top',smallHeight+'px').css('padding-bottom',smallHeight+'px')
	})	
}