(function($){
    $.fn.iceslider = function(options){
        var options = $.extend({
            autoRotate: true, // автопрокрутка
            interval: 7000, // интервал автопрокрутки
            slideSpeed: 700, // скорость анимации смены слайда
            navBtn: false, // кнопки навигации - лево-право
            pager: true // slider pager
        }, options);

        var make = function(){
            var that = this;
            var slide = $(this).find('li');
            var count = $(slide).length;
            var pager, num;
            var slideTimer = 0;
            var slideNum = 0;
            $(slide).css({position: 'absolute', display: 'none'});
            $(slide).eq(0).css('display', 'block');
            if(!options.navBtn){
                $('#linkprev').css('display', 'none');
                $('#linknext').css('display', 'none');
            }
            //if (options.pager){
            //    pager = $('#my_pager');
            //    $(pager).append('<div class="bx-pager bx-default-pager"></div>');
            //    for (var i = 0; i < count; i++){
            //        $(pager).find('.bx-pager').append('<div class="bx-pager-item"><a href="" data-slide-index="' + i + '" class="bx-pager-link">' + i + '</a></div>');
            //    };
            //}
            pager = $( '.b-banners__link' );
            pager.eq(slideNum).addClass('active');
            var rotate = function(direct){
                if(options.autoRotate) {clearTimeout(slideTimer);}
                $(slide).eq(slideNum).fadeOut(options.slideSpeed);
                $(pager).eq(slideNum).removeClass('b-banners__link-current');
                switch (direct){
                    case 'prev':
                        slideNum--;
                        if (slideNum == -1) slideNum = count-1;
                        break;
                    case 'next':
                        slideNum++;
                        if (slideNum == count) slideNum = 0;
                        break;
                    case 'num':
                        slideNum = num;
                        if (slideNum == count) slideNum = 0;
                        break;
                }
                $(slide).eq(slideNum).fadeIn(options.slideSpeed, rotator());
                $(pager).eq(slideNum).addClass('b-banners__link-current');
            }
            var rotator = function(){
                if (options.autoRotate) {slideTimer = setTimeout(function(){rotate('next')}, options.interval);}
            }
            if (options.autoRotate) {rotator();}
            $('#linkprev').click(function(){
                rotate('prev');
            });
            $('#linknext').click(function(){
                rotate('next');
            });
            $(pager).on('click', function(){
                num = $(this).index();
                rotate('num');
                return false;
            });

            $(that).on('mouseenter', function(){
                clearTimeout(slideTimer);
            });

            $(that).on('mouseleave', function(){
                rotator();
            });
        };

        return this.each(make);
    };
})(jQuery);

