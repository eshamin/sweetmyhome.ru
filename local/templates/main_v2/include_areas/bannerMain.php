<div class="content-block-slider">
    <div class="flexslider">
        <?$resBanner = CIBlockElement::GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => 16, 'SECTION_ID' => 917));?>
        <ul class="slides">
            <?while ($banner = $resBanner->Fetch()) {?>
                <li>
                    <img border="0" src="<?=CFile::GetPath($banner['PREVIEW_PICTURE'])?>" alt="<?=$banner['NAME']?>" title="<?=$banner['NAME']?>"/>
                </li>
            <?}?>
        </ul>
    </div>
</div>