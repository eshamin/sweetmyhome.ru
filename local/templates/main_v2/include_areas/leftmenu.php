        <div class="b-content__leftmenu">
            <ul class="b-content__leftmenu-uplevel">
                <li class="b-content__leftmenu-uplevel--item">
                    <h4>Оформление заказа</h4>
                    <ul class="b-content__leftmenu-downlevel">
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Как оформить заказ?</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Как заказать кухню?</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Как удобно искать товары?</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Что с моим заказом?</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Телепортатор?</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Заказ в 1 клик</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Популярные вопросы</a></li>
                    </ul>
                </li>
                <li class="b-content__leftmenu-uplevel--item">
                    <h4>Оплата</h4>
                    <ul class="b-content__leftmenu-downlevel">
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Что с моим заказом?</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Телепортатор?</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Заказ в 1 клик</a></li>
                        <li class="b-content__leftmenu-downlevel--item"><a href="#">Популярные вопросы</a></li>
                    </ul>
                </li>
                <li class="b-content__leftmenu-uplevel--item">
                    <h4>Доставка и услуги</h4>
                    <ul class="b-content__leftmenu-downlevel">

                    </ul>
                </li>
                <li class="b-content__leftmenu-uplevel--item">
                    <h4>Гарантии и Возвраты</h4>
                    <ul class="b-content__leftmenu-downlevel">

                    </ul>
                </li>
            </ul>
        </div>