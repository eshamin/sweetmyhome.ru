$(document).ready(function() {

    var wi = $( '.cloud-zoom-wrap').width();
    var he = wi / 1.33;
    $( '.cloud-zoom-wrap').css( { "height":he } );
    var wimg = $( '.cloud-zoom img').width();
    $( '.cloud-zoom').css( { "width":wimg } );

    $( window ).resize( function(){
        var $div = $( '.cloud-zoom-wrap' );
        var $zoomDiv = $( '#cloud-zoom-big' );
        var wi = $div.width();
        var he = wi / 1.33;
        var wimg = $( '.cloud-zoom img').width();
        $div.css( { "height":he } );
        $( '.cloud-zoom').css( { "width":wimg } );
        var leftPos = $div.outerWidth();
        $zoomDiv.css( { "left":leftPos } );
    } );
    BX.addCustomEvent('onAjaxSuccess', fuckedUpBxAjax);
    ymaps.ready(function() {
        var geolocation = ymaps.geolocation;

        postData = {
            'action': 'getCity',
            'cityName': geolocation.city
        };

        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function (result) {
                BX.closeWait();
                if (result['CODE'] == 'SUCCESS') {
                    $('.location').html(result['cityName']);
                }

                if (result['SHOW_LOCATION']) {
                    $('#your-city-link').click();
                }
            }
        });
    });

    $( '#buy-accessories').on( 'click', function( e ){
        e.preventDefault();
        var data_to_send = buyAccessories( this );
        postData = {
            'action': 'add2basket',
            'itemId': data_to_send,
            'quantity': 1
        };

        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();
                if (result['CODE'] == 'SUCCESS') {
                    $('#quantitySmallBasket').html(parseInt(result['QUANTITY']));
                    $('#priceSmallBasket').html(result['PRICE']);
                    $('#quantityAccessories').html(parseInt(result['QUANTITY']));
                    $('#priceAccessories').html(result['PRICE']);
                    $('#quantityFavorites').parents('li').addClass('active');

                    $('.quantityTotalBasket').html(getNumEnding(parseInt(result['QUANTITY']), Array('товар', 'товара', 'товаров')));
                    $('.priceTotalBasket').html(result['PRICE']);
                    $('#item_name').html(result['ITEM_NAME']);
                    $('#item_discount_price').html(result['ITEM_DISCOUNT_PRICE']);
                    $('#item_price').html(result['ITEM_PRICE']);
                    $('#item_count').html(result['ITEM_QUANTITY']);
                    $('#item_img').html(result['ITEM_IMG']);
                    MODAL.showOverlay();
                    renderPopupHTML('#acc-added');
                }
            }
        });
        return false;
    } );

    function fuckedUpBxAjax() {
        $("#ORDER_PROP_7").on('keyup', function () {
            var search_query = $('.bx-ui-sls-fake').attr('title') + ' + ' + $(this).val();
            search_result = [];
            $.getJSON('http://geocode-maps.yandex.ru/1.x/?format=json&results=50&callback=?&geocode=' + search_query, function (data) {
                for (var i = 0; i < data.response.GeoObjectCollection.featureMember.length; i++) {
                    if (data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.kind == "locality" ||
                        data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.kind == "street" ||
                        data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.kind == "house") {
                        search_result.push({
                            label: data.response.GeoObjectCollection.featureMember[i].GeoObject.description + ' - ' + data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                            value: data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                            longlat: data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos
                        });
                    }
                }
                $("#ORDER_PROP_7").autocomplete({
                    source: search_result
                });
            });
        });

        $.ui.autocomplete.filter = function (array, term) {
            return $.grep(array, function (value) {
                return value.label || value.value || value;
            });
        };

        $( '.b-makeorder__services-link' ).on( 'click', function(){
            var $li = $( this ).closest( 'li' );
            if( $li.hasClass( 'b-makeorder__services-open' ) ){
                $li.removeClass( 'b-makeorder__services-open' );
            }
            else {
                $li.addClass( 'b-makeorder__services-open' );
            }
        } );

        /* поправим высоту параметров */
        var $tab = $( '.b-params__tabs-tab-current' );
        var tab_hei = $tab.outerHeight();
        if( tab_hei > 727 ) {
            $('.b-params').css({"height": tab_hei});
        }
        /* поправим высоту параметров */

        var $tablink = $( '.b-params-tabs-link' );
        $tablink.on( 'click', function(){
            var $that = $( this );
            if( $that.hasClass( 'b-params-tabs-link-current' ) ){
                return false;
            };
            var indice = $that.index();
            $( '.b-params-tabs-link-current').removeClass( 'b-params-tabs-link-current' );
            $( '.b-params__tabs-tab-current').removeClass( 'b-params__tabs-tab-current' );
            $tablink.eq( indice).addClass( 'b-params-tabs-link-current' );
            $( '.b-params__tabs-tab').eq( indice).addClass( 'b-params__tabs-tab-current' );

            /* поправим высоту параметров */
            $tab = $( '.b-params__tabs-tab-current' );
            tab_hei = $tab.outerHeight();
            if( tab_hei > 727 ) {
                $('.b-params').css({"height": tab_hei});
            }
            /* поправим высоту параметров */
        } );

        $( '.b-goods__good-more-buy .js-digital-spinner-up').on( 'click', function(){
            var $that = $( this );
            var $input = $that.closest( '.js-digital-spinner').find( 'input' );
            var val = parseInt( $input.val() );
            if( !val || val == 0 ){
                val = 1;
                $input.val( val );
            };
            val++;
            $input.val( val );
        } );
        $( '.b-goods__good-more-buy .js-digital-spinner-down').on( 'click', function(){
            var $that = $( this );
            var $input = $that.closest( '.js-digital-spinner').find( 'input' );
            var val = parseInt( $input.val() );
            if( !val || val == 0 ){
                val = 1;
                $input.val( val );
            };
            if( val == 1 ){
                return false;
            };
            val--;
            $input.val( val );
        } );

        $( '#your-city-link').on( 'click', function(){
            $( '.your-city-dropdown' ).fadeIn( 200 );
        } );
        $( '.your-city-yes').on( 'click', function(){
            var $input = $( 'input[name="selected_city"]' );
            var $span = $( '#your-city-link' ).find( 'span' );
            if( $input.val() == '' ){
                var city = $( '.your-city-dropdown').find( '.location').html();
                $span.html( city );
            }
            else{
                $span.html( $input.val() );
            };
            $( '.your-city-dropdown').fadeOut( 200 );
        } );
        $( '.your-city-choice').on( 'click', function(){
            var id = 'your-city-list';
            MODAL.showOverlay();
            renderPopupHTML( '#' + id );
        } );
        $( document).on( 'click', '.abc-city-list a, .preselected-city a', function(){
            var $that = $( this );
            var val = $that.html();
            $( 'input[name="selected_city"]').val( val );
            MODAL.closePopup();
            MODAL.closeOverlay();
            $( '.your-city-yes').trigger( 'click' );
        } );

        $( '.b-makeorder__fastorder-cross-span').on( 'click', function(){
            $( this).prev( 'input').val( '' );
        } );
        $( '.b-makeorder__userphone' ).mask( '+7(999)999-99-99' );

        $( '.b-makeorder__order-remove').on( 'click', function(){
            MODAL.showOverlay();
            var id = 'remove-from-basket';
            renderPopupHTML( '#' + id );
        } );

        $( '#ORDER_CONFIRM_BUTTON').on( 'click', function( e ){
            if ($('#fast-order').val() == 'N') {
                var $input = $('.b-makeorder__adress').find('input:required');
                var errors = 0;
                $input.each(function () {
                    var $that = $(this);
                    if (!$that.val()) {
                        errors++;
                        $that.addClass('b-makeorder__adress-error');
                        $that.closest('li').find('.error-tooltip').css({"display": "block"});
                    }
                });
                if (!$("input[name='ORDER_PROP_6']").val()) {
                    errors++;
                    $('.bx-ui-sls-input-block').addClass('b-makeorder__adress-error');
                    $('#error-city').css({"display": "block"});
                }
            } else {
                var $input = $('.b-makeorder__fastorder').find('input:required');
                var errors = 0;
                $input.each(function () {
                    var $that = $(this);
                    if (!$that.val()) {
                        errors++;
                        $that.addClass('b-makeorder__adress-error');
                        $that.closest('div').find('.error-tooltip').css({"display": "block"});
                    }
                });
            }

            if( errors ){
                e.preventDefault();
            } else {
                if ($('#fast-order').val() == 'N') {
                    submitForm('Y');
                    return false;
                } else {
                    postData = {
                        action: 'addFastOrder',
                        mobile: $("input[name='mobile']").val(),
                        name: $("input[name='name']").val()
                    };

                    BX.ajax({
                        url: '/ajax/index.php',
                        method: 'POST',
                        data: postData,
                        dataType: 'json',
                        onsuccess: function (result) {
                            BX.closeWait();
                            if (result['CODE'] == 'SUCCESS') {
                                window.location.href = '/personal/order/make/?ORDER_ID=' + result['MESSAGE'];
                            } else {
                                alert(result['MESSAGE']);
                            }
                        }
                    });
                }
            }
        });

        var currentPage = 1;
        $('.btn-product-more-list').on('click', function() {
            el = $(this);
            postData = {
                action: 'getProducts',
                filter: $('#arFilter').val(),
                sectionId: $('#sectionId').val(),
                PAGEN_1: ++currentPage,
                PAGEN_32: currentPage
            };


            BX.ajax({
                url: '/ajax/index.php',
                method: 'POST',
                data: postData,
                dataType: 'json',
                onsuccess: function(result)
                {
                    BX.closeWait();

                    el.before(result['PRODUCTS_LIST']);
                    $('.btn-product-more-card').before(result['PRODUCTS_CARD']);
                    if (currentPage * 30 >= $('#total-count-items').html()) {
                        $('.btn-product-more').remove();
                    } else if ((currentPage + 1) * 30 > $('#total-count-items').html()) {
                        var countItems = $('#total-count-items').html() - (currentPage * 30);
                        $('.btn-product-more__text').html('<span>Показать еще ' + countItems + '</span>');
                        $('.total-show-count').html(currentPage * 30);
                    } else {
                        $('.total-show-count').html(currentPage * 30);
                    }
                }
            });
            return false;
        });

        $('.btn-product-more-card').on('click', function() {
            el = $(this);
            postData = {
                action: 'getProducts',
                filter: $('#arFilter').val(),
                sectionId: $('#sectionId').val(),
                PAGEN_1: ++currentPage,
                PAGEN_32: currentPage
            };

            BX.ajax({
                url: '/ajax/index.php',
                method: 'POST',
                data: postData,
                dataType: 'json',
                onsuccess: function(result)
                {
                    BX.closeWait();

                    el.before(result['PRODUCTS_CARD']);
                    $('.btn-product-more-list').before(result['PRODUCTS_LIST']);
                    if (currentPage * 30 >= $('#total-count-items').html()) {
                        $('.btn-product-more').remove();
                    } else if ((currentPage + 1) * 30 > $('#total-count-items').html()) {
                        var countItems = $('#total-count-items').html() - (currentPage * 30);
                        $('.btn-product-more__text').html('<span>Показать еще ' + countItems + '</span>');
                        $('.total-show-count').html(currentPage * 30);
                    } else {
                        $('.total-show-count').html(currentPage * 30);
                    }
                }
            });
            return false;
        });

        $('.search-cat').click(function() {
            $('#where').val($(this).data('value'));
            $('#whereText').html($(this).html());
        });

        $('.switch_view.lines').on('click',function(){
            $('.lines').addClass('active');
            $('.blocks').removeClass('active');
        });
        $('.switch_view.blocks').on('click',function(){
            $('.lines').removeClass('active');
            $('.blocks').addClass('active');
        });

        var isFixedSupported = (function(){
            var isSupported = null;
            if (document.createElement) {
                var el = document.createElement("div");
                if (el && el.style) {
                    el.style.position = "fixed";
                    el.style.top = "10px";
                    var root = document.body;
                    if (root && root.appendChild && root.removeChild) {
                        root.appendChild(el);
                        isSupported = (el.offsetTop === 10);
                        root.removeChild(el);
                    }
                }
            }
            return isSupported;
        })();
        window.onload = function(){
            if (!isFixedSupported){
                // добавляем контекст для "старичков"
                document.body.className += ' no-fixed-supported';
                // имитируем position: fixed;
                var bottombar = document.getElementById('fixed-panel');
                var bottomBarHeight = bottombar.offsetHeight;
                var windowHeight = window.innerHeight;
                // обрабатываем события touch и scroll
                window.ontouchmove = function() {
                    if (event.target !== bottombar){
                        bottombar.style = "";
                    }
                };
                window.onscroll = function(){
                    var scrollTop = window.scrollY;
                    bottombar.style.bottom = (scrollTop + windowHeight - bottomBarHeight) + 'px';
                };
            }
            // первичный scroll
            window.scrollBy(0, 1);
        };

        /* mask for date field */
        $('.b-makeorder__adress-data').find('input').mask('99.99.9999');
        /* remove eroor on keypress */
        var $input = $('.b-makeorder__adress').find('input:required');
        $input.on('keypress', function () {
            var $that = $(this);
            $that.removeClass('b-makeorder__adress-error');
            $that.closest('li').find('.error-tooltip').css({"display": "none"});
        });
        var $input = $('.b-makeorder__fastorder').find('input:required');
        $input.on('keypress', function () {
            var $that = $(this);
            $that.removeClass('b-makeorder__adress-error');
            $that.closest('div').find('.error-tooltip').css({"display": "none"});
        });

        $('.bx-ui-sls-input-block').find('input').on('keypress', function () {
            $('.bx-ui-sls-input-block').removeClass('b-makeorder__adress-error');
            $('#error-city').css({"display": "none"});
        });
        var $datepicker = $( '#datepicker');
        $datepicker.datepicker({
            showOtherMonths: true,
            selectOtherMonths: true
        });
        $datepicker.on( 'change', function(){
            var $that = $(this);
            $that.removeClass('b-makeorder__adress-error');
            $that.closest('li').find('.error-tooltip').css({"display": "none"});
        } );
    }

    $("#ORDER_PROP_7").on('keyup', function () {
        var search_query = $('.bx-ui-sls-fake').attr('title') + ' + ' + $(this).val();
        search_result = [];
        $.getJSON('http://geocode-maps.yandex.ru/1.x/?format=json&results=50&callback=?&geocode=' + search_query, function (data) {
            for (var i = 0; i < data.response.GeoObjectCollection.featureMember.length; i++) {
                if (data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.kind == "locality" ||
                    data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.kind == "street" ||
                    data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.kind == "house") {
                    search_result.push({
                        label: data.response.GeoObjectCollection.featureMember[i].GeoObject.description + ' - ' + data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                        value: data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                        longlat: data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos
                    });
                }
            }
            $("#ORDER_PROP_7").autocomplete({
                source: search_result
            });
        });
    });

    $.ui.autocomplete.filter = function (array, term) {
        return $.grep(array, function (value) {
            return value.label || value.value || value;
        });
    };

    $( '.b-makeorder__services-link' ).on( 'click', function(){
        var $li = $( this ).closest( 'li' );
        if( $li.hasClass( 'b-makeorder__services-open' ) ){
            $li.removeClass( 'b-makeorder__services-open' );
        }
        else{
            $li.addClass( 'b-makeorder__services-open' );
        }
    } );

    var $tablink1 = $( '.b-makeorder__tablinks').find( 'a' );
    var $tabs1 = $( '.b-makeorder__tabs1');
    $tablink1.on( 'click', function(){
        var $that = $( this );
        if( $that.hasClass( 'b-makeorder__currenttab' ) ){
            return false;
        }
        $tablink1.toggleClass( 'b-makeorder__currenttab' );
        $tabs1.toggleClass( 'b-makeorder__tabs-current' );
    } );

    /* поправим высоту параметров */
    var $tab = $( '.b-params__tabs-tab-current' );
    var tab_hei = $tab.outerHeight();
    if( tab_hei > 727 ) {
        $('.b-params').css({"height": tab_hei});
    }
    /* поправим высоту параметров */

    var $tablink = $( '.b-accessories__slider' );
    $tablink.on( 'click', function(){
        var $that = $( this );
        if( $that.hasClass( 'b-params-tabs-link-current' ) ){
            return false;
        };
        var indice = $that.index();
        $( '.b-params-tabs-link-current').removeClass( 'b-params-tabs-link-current' );
        $( '.b-params__tabs-tab-current').removeClass( 'b-params__tabs-tab-current' );
        $tablink.eq( indice).addClass( 'b-params-tabs-link-current' );
        $( '.b-params__tabs-tab').eq( indice).addClass( 'b-params__tabs-tab-current' );

        /* поправим высоту параметров */
        $tab = $( '.b-params__tabs-tab-current' );
        tab_hei = $tab.outerHeight();
        if( tab_hei > 727 ) {
            $('.b-params').css({"height": tab_hei});
        }
        /* поправим высоту параметров */
    } );

    $( '.b-goods__good-more-buy .js-digital-spinner-up').on( 'click', function(){
        var $that = $( this );
        var $input = $that.closest( '.js-digital-spinner').find( 'input' );
        var val = parseInt( $input.val() );
        if( !val || val == 0 ){
            val = 1;
            $input.val( val );
        };
        val++;
        $input.val( val );
    } );
    $( '.b-goods__good-more-buy .js-digital-spinner-down').on( 'click', function(){
        var $that = $( this );
        var $input = $that.closest( '.js-digital-spinner').find( 'input' );
        var val = parseInt( $input.val() );
        if( !val || val == 0 ){
            val = 1;
            $input.val( val );
        };
        if( val == 1 ){
            return false;
        };
        val--;
        $input.val( val );
    } );

    var putSrars = function( elem ){
        var $that = $( elem );
        var $stars = $that.find( '.b-top-panel__feedback-star-likelink' );
        $stars.css( { "cursor":"pointer" } );
        if( $stars.length ){
            $stars.on( 'mouseenter', function(){
                var $that = $( this );
                var indice = $that.index() + 1;
                $stars.removeClass( 'b-top-panel__feedback-star-gold' ).addClass( 'b-top-panel__feedback-star-gray' );
                for( var i = 0; i < indice; i++ ){
                    $stars.eq( i ).removeClass( 'b-top-panel__feedback-star-gray' ).addClass( 'b-top-panel__feedback-star-gold' );
                }
            } );
            $stars.on( 'click', function(){
                var indice = $( this).index() + 1;$stars.removeClass( 'b-top-panel__feedback-star-gold' ).addClass( 'b-top-panel__feedback-star-gray' );
                for( var i = 0; i < indice; i++ ){
                    $stars.eq( i ).removeClass( 'b-top-panel__feedback-star-gray' ).addClass( 'b-top-panel__feedback-star-gold' );
                }
                $( '#rating' ).val( indice );
            } );
        }
    };

    $( '.js-popup' ).on( 'click', function()
    {
        var $that = $( this );
        MODAL.showOverlay();
        var id = $that.data( 'id' );
        var callback = $that.data( 'callback' );
        if( callback = 'putStars' ){
            callback = putSrars;
        }
        renderPopupHTML( '#' + id, callback );
    } );

    function renderPopupHTML( id, callback, add_class )
    {
        if( add_class == 'b-popup__good' ){
            var he = $( window ).height() - 260;
            var hei = $( window ).height() - 100;
            console.log( hei );
        }

        MODAL.showPopup( $( id ).html(), hei );

        if( add_class ){
            $( '.b-popup' ).addClass( add_class );
        }

        var $o = $( '.b-accessories__slider-wrapper ul' );

        if ($o.length) {
            var i = $o.length - 1;
            $elem = $o.eq(i).closest('.b-accessories__slider');
            btnN = $elem.find('.b-accessories__slider-button-next');
            btnP = $elem.find('.b-accessories__slider-button-prev');
            carouselInit($o.eq(i), 5, 1, btnP, btnN);
        }
        if( typeof callback === 'function' ){
            callback( $( '.b-popup' ).children( 'div').first() );
        }
    };

    function carouselInit(slider, count, rotate, btnPrev, btnNext) {
        var o = $(slider);
        var elem = $(o).closest('div');
        if (elem.length) {
            if ($(o).children('li').length > count) {
                $(elem).Carousel({
                    visible: count,
                    rotateBy: rotate,
                    speed: 500,
                    btnNext: btnNext,
                    btnPrev: btnPrev,
                    auto: false,
                    margin: 16,
                    position: "h",
                    dirAutoSlide: false
                });
            } else {
                $(elem).css({overflow: 'visible'});
            }
        }
    };

    var $o = $('.b-goods__more-like-slider ul');

    var btnN, btnP;

    if ($o.length) {
        $o.each( function(){
            var $that = $( this );
            $elem = $that.closest('.b-goods__more-like-slider');
            btnN = $elem.find('.b-goods__more-like-slider-button-next');
            btnP = $elem.find('.b-goods__more-like-slider-button-prev');
            carouselInit($o.eq(0), 1, 1, btnP, btnN);
        } );
    }

    $o = $( '.b-accessories__slider-wrapper ul' );

    if( $o.length ){
        $o.each( function(){
            var $that = $( this );
            $elem = $that.closest('.b-accessories__slider');
            btnN = $elem.find('.b-accessories__slider-button-next');
            btnP = $elem.find('.b-accessories__slider-button-prev');
            carouselInit($that, 6, 1, btnP, btnN);
        } );
    }

    $o = $( '.b-banners__banners' );
    if( $o.length ){
        $o.iceslider();
    };

    var $thumbs = $( '.b-goods__good-photo-thumbs li' );
    $thumbs.on( 'click', function(){
        var $that = $( this );
        $( '.b-goods__good-photo-thumbs-current').removeClass( 'b-goods__good-photo-thumbs-current' );
        $that.addClass( 'b-goods__good-photo-thumbs-current' );
        var src = $that.find( 'a' ).data( 'img' );
        $( '.b-goods__good-photo-main a').prop( 'href', src );
        $( '.b-goods__good-photo-main a img').prop( 'src', src );
        $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
    } );
    function onlyDigits() {
        if( !this.value ) return false;
        this.value = this.value.replace(/[^\d]/g, "");
        if( this.value == "" || this.value == 0 ){
            this.value = 1;
        }
    }

    $( document ).on( 'keyup', '.tab-pane', onlyDigits );
    $( document ).on( 'keyup', '.onlyDigits', onlyDigits );
    $( document ).on( 'blur', '.onlyDigits', function(){
        if( this.value == "" || this.value == 0 ){
            this.value = 1;
        }
    } );
    $( '#oneclick-phone').mask( '(999)999-99-99' );

    $( document).on( 'click', '.b-accessories__header-bottom a', function( e ){
        e.preventDefault();
        var $that = $( this).closest( 'li' );
        if( $that.hasClass( 'b-accessories__header-bottom-current' ) ){
            return false;
        }
        var indice = $that.index();
        $( '.b-accessories__header-bottom-current').removeClass( 'b-accessories__header-bottom-current' );
        $that.addClass( 'b-accessories__header-bottom-current' );
        var $wrapper = $that.closest( '.b-accessories' );
        if( !$wrapper.length ){
            return false
        }
        var $slider = $wrapper.find( '.b-accessories__slider' );
        $( '.b-accessories__slider-current').removeClass( 'b-accessories__slider-current').css( { "display" : "none" } );
        $slider.eq( indice).css( { "display" : "block" } ).addClass( 'b-accessories__slider-current' );
    } );

    /* маленький фик для высоты блока товара */
    if( $( '.b-goods__good-more-about').length ){
        var good_hei = $( '.b-goods__good-more-about' ).outerHeight();
        good_hei = ( good_hei < 400 ) ? 400:good_hei;
        $( '.b-goods__good').css( { "min-height":good_hei } );
    };
    /* маленький фик для высоты блока товара */
    /* увеличенная картинка */

    $( '.b-goods__good-photo-zoom').on( 'click', function(){
        MODAL.showOverlay();
        $( '.b-overlay').css( { "background":"#353535" } );
        var id = 'good-gallery';
        renderPopupHTML( '#' + id, null, 'b-popup__good' );
    } );

    /* увеличенная картинка */

    /* price-delivery-warranty */
    var $del_container = $( '.b-goods__good-more-delivery' );
    var $delivery_tablinks = $del_container.find( '.b-makeorder__tablinks_2').find( 'a' );
    var $delivery_tabs = $del_container.find( '.b-makeorder__tabs' );

    $delivery_tablinks.on( 'click', function(){
        console.log( 'we are here' );
        var $that = $( this );
        var indice = $that.index();

        if( $that.hasClass( 'b-makeorder__currenttab' ) ){
            return false;
        }
        $del_container.find( '.b-makeorder__currenttab' ).removeClass( 'b-makeorder__currenttab' );
        $del_container.find( '.b-makeorder__tabs-current').removeClass( 'b-makeorder__tabs-current' );
        $that.addClass( 'b-makeorder__currenttab' );
        $delivery_tabs.eq( indice).addClass( 'b-makeorder__tabs-current' );
    } );

    /* price-delivery-warranty */

    $( '#your-city-link').on( 'click', function(){
        $( '.your-city-dropdown' ).fadeIn( 200 );
    } );
    $( '.your-city-yes').on( 'click', function(){
        var $input = $( 'input[name="selected_city"]' );
        var $span = $( '#your-city-link' ).find( 'span' );
        if( $input.val() == '' ){
            var city = $( '.your-city-dropdown').find( '.location').html();
            $span.html( city );
        }
        else{
            $span.html( $input.val() );
        };
        $( '.your-city-dropdown').fadeOut( 200 );
    } );
    $( '.your-city-choice').on( 'click', function(){
        var id = 'your-city-list';
        MODAL.showOverlay();
        renderPopupHTML( '#' + id );
    } );
    $( document).on( 'click', '.abc-city-list a, .preselected-city a', function(){
        var $that = $( this );
        var val = $that.html();
        $( 'input[name="selected_city"]').val( val );
        MODAL.closePopup();
        MODAL.closeOverlay();
        $( '.your-city-yes').trigger( 'click' );
    } );

    $(document).on('click', '.showCities', function() {
        var letter = $(this).html();
        postData = {
            'action': 'getCities',
            'letter': letter
        };

        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();
                if (result['CODE'] == 'SUCCESS') {
                    $('.abc-city-list').html(result['CITIES']);
                }
            }
        });
    });

    $(document).on('keyup', '#city_name', function() {
        var letter = $(this).val();
        postData = {
            'action': 'getCities',
            'letter': letter
        };

        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();
                if (result['CODE'] == 'SUCCESS') {
                    $('.abc-city-list').html(result['CITIES']);
                }
            }
        });
    });

    $(document).on('click', '.add2basket', function() {
        itemId = $(this).attr('item-id');
        if ($('input').is('#qnt2basket' + itemId)) {
            quantity = $('#qnt2basket' + itemId).val();
        } else {
            quantity = 1;
        }

        postData = {
            'action': 'add2basket',
            'itemId': itemId,
            'quantity': quantity
        };

        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();
                if (result['CODE'] == 'SUCCESS') {
                    $('#quantitySmallBasket').html(parseInt(result['QUANTITY']));
                    $('#priceSmallBasket').html(result['PRICE']);
                    $('#quantityAccessories').html(parseInt(result['QUANTITY']));
                    $('#priceAccessories').html(result['PRICE']);
                    $('#quantityFavorites').parents('li').addClass('active');

                    $('.quantityTotalBasket').html(getNumEnding(parseInt(result['QUANTITY']), Array('товар', 'товара', 'товаров')));
                    $('.priceTotalBasket').html(result['PRICE']);
                    $('#item_name').html(result['ITEM_NAME']);
                    $('#item_discount_price').html(result['ITEM_DISCOUNT_PRICE']);
                    $('#item_price').html(result['ITEM_PRICE']);
                    $('#item_count').html(result['ITEM_QUANTITY']);
                    $('#item_img').html(result['ITEM_IMG']);
                    MODAL.showOverlay();
                    renderPopupHTML('#good-added');
                }
            }
        });
        return false;
    });

    $(document).on('click', '.deleteFromBasket', function(e) {
        e.preventDefault();
        itemId = $(this).attr('item-id');

        MODAL.showOverlay();
        renderPopupHTML( '#remove-from-basket', basketRemoveCallback );

        //postData = {
        //    action: 'deleteFromBasket',
        //    itemId: itemId
        //};
        //
        //BX.ajax({
        //    url: '/ajax/index.php',
        //    method: 'POST',
        //    data: postData,
        //    dataType: 'json',
        //    onsuccess: function(result)
        //    {
        //        BX.closeWait();
        //        $('#quantitySmallBasket').html(parseInt(result['QUANTITY']));
        //        $('#priceSmallBasket').html(result['PRICE']);
        //        $('#totalCount').html(getNumEnding(parseInt(result['QUANTITY']), Array('товар', 'товара', 'товаров')));
        //        $('#allSum_FORMATED').html(result['PRICE']);
        //
        //        if (result['CODE'] == 'SUCCESS') {
        //            BX(itemId).remove();
        //        }
        //
        //        if (result['QUANTITY'] == 0) {
        //            document.location = '/catalog/';
        //        }
        //    }
        //});
        return false;
    });

    var basketRemoveCallback = function(){
        $( document).on( 'click', '.js-basket-back', function(){
            MODAL.closePopup();
            MODAL.closeOverlay();
            $( document).off( 'click', '.js-basket-remove' );
        } );
        $( document).on( 'click', '.js-basket-remove', function(e){
            e.preventDefault();
            $( document).off( 'click', '.js-basket-remove' );
            postData = {
                action: 'deleteFromBasket',
                itemId: itemId
            };

            BX.ajax({
                url: '/ajax/index.php',
                method: 'POST',
                data: postData,
                dataType: 'json',
                onsuccess: function(result)
                {
                    BX.closeWait();
                    $('#quantitySmallBasket').html(parseInt(result['QUANTITY']));
                    $('#priceSmallBasket').html(result['PRICE']);
                    $('#totalCount').html(getNumEnding(parseInt(result['QUANTITY']), Array('товар', 'товара', 'товаров')));
                    $('#allSum_FORMATED').html(result['PRICE']);

                    if (result['CODE'] == 'SUCCESS') {
                        BX(itemId).remove();
                    }

                    if (result['QUANTITY'] == 0) {
                        document.location = '/catalog/';
                    }
                    MODAL.closePopup();
                    MODAL.closeOverlay();
                    $( document).off( 'click', '.js-basket-remove' );
                }
            });
            return false;
        } );
    };

    $(document).on('click', '.add-to-favorites', function() {
        itemId = $(this).attr('item-id');

        postData = {
            'action': 'add2favorites',
            'itemId': itemId
        };

        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();
                if (result['CODE'] == 'SUCCESS') {
                    $('#quantityFavorites').html(parseInt(result['QUANTITY']));
                    $('#quantityFavorites').parents('li').addClass('active');
                    /*
                     MODAL.showOverlay();
                     renderPopupHTML('#good-added');*/
                }
            }
        });
        return false;
    });

    $( '.b-makeorder__fastorder-cross-span').on( 'click', function(){
        $( this).prev( 'input').val( '' );
    } );
    $( '.b-makeorder__userphone' ).mask( '+7(999)999-99-99' );

    $( '.b-makeorder__order-remove').on( 'click', function(){
        MODAL.showOverlay();
        var id = 'remove-from-basket';
        renderPopupHTML( '#' + id );
    } );

    //function renderPopupHTML( id ){
    //    MODAL.showPopup( $( id ).html() );
    //    var $o = $( '.b-accessories__slider-wrapper ul' );
    //
    //    if ($o.length) {
    //        $elem = $o.eq(0).closest('.b-accessories__slider');
    //        btnN = $elem.find('.b-accessories__slider-button-next');
    //        btnP = $elem.find('.b-accessories__slider-button-prev');
    //        carouselInit($o.eq(0), 5, 1, btnP, btnN);
    //    }
    //};
    $( '#ORDER_CONFIRM_BUTTON').on( 'click', function( e ){
        if ($('#fast-order').val() == 'N') {
            var $input = $('.b-makeorder__adress').find('input:required');
            var errors = 0;
            $input.each(function () {
                var $that = $(this);
                if (!$that.val()) {
                    errors++;
                    $that.addClass('b-makeorder__adress-error');
                    $that.closest('li').find('.error-tooltip').css({"display": "block"});
                }
            });
            if (!$("input[name='ORDER_PROP_6']").val()) {
                errors++;
                $('.bx-ui-sls-input-block').addClass('b-makeorder__adress-error');
                $('#error-city').css({"display": "block"});
            }
        } else {
            var $input = $('.b-makeorder__fastorder').find('input:required');
            var errors = 0;
            $input.each(function () {
                var $that = $(this);
                if (!$that.val()) {
                    errors++;
                    $that.addClass('b-makeorder__adress-error');
                    $that.closest('div').find('.error-tooltip').css({"display": "block"});
                }
            });
        }

        if( errors ){
            e.preventDefault();
        } else {
            if ($('#fast-order').val() == 'N') {
                submitForm('Y');
                return false;
            } else {
                postData = {
                    action: 'addFastOrder',
                    mobile: $("input[name='mobile']").val(),
                    name: $("input[name='name']").val()
                };

                BX.ajax({
                    url: '/ajax/index.php',
                    method: 'POST',
                    data: postData,
                    dataType: 'json',
                    onsuccess: function (result) {
                        BX.closeWait();
                        if (result['CODE'] == 'SUCCESS') {
                            window.location.href = '/personal/order/make/?ORDER_ID=' + result['MESSAGE'];
                        } else {
                            alert(result['MESSAGE']);
                        }
                    }
                });
            }
        }
    });

    $('#order-oneclick').on('click', function(e) {
        postData = {
            action: 'addOneClickOrder',
            mobile: $("#oneclick-phone").val(),
            productId: $(this).attr('item-id')
        };

        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function (result) {
                BX.closeWait();
                if (result['CODE'] == 'SUCCESS') {
                    MODAL.showOverlay();
                    $('#orderID').html(result['ORDER_ID']);
                    renderPopupHTML('#order-created');
                } else {
                    alert(result['MESSAGE']);
                }
            }
        });
    });

    var currentPage = 1;
    $('.btn-product-more-list').on('click', function() {
        el = $(this);
        postData = {
            action: 'getProducts',
            filter: $('#arFilter').val(),
            sectionId: $('#sectionId').val(),
            PAGEN_1: ++currentPage,
            PAGEN_32: currentPage
        };


        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();

                el.before(result['PRODUCTS_LIST']);
                $('.btn-product-more-card').before(result['PRODUCTS_CARD']);
                if (currentPage * 30 >= $('#total-count-items').html()) {
                    $('.btn-product-more').remove();
                } else if ((currentPage + 1) * 30 > $('#total-count-items').html()) {
                    var countItems = $('#total-count-items').html() - (currentPage * 30);
                    $('.btn-product-more__text').html('<span>Показать еще ' + countItems + '</span>');
                    $('.total-show-count').html(currentPage * 30);
                } else {
                    $('.total-show-count').html(currentPage * 30);
                }
            }
        });
        return false;
    });

    $('.btn-product-more-card').on('click', function() {
        el = $(this);
        postData = {
            action: 'getProducts',
            filter: $('#arFilter').val(),
            sectionId: $('#sectionId').val(),
            PAGEN_1: ++currentPage,
            PAGEN_32: currentPage
        };

        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();

                el.before(result['PRODUCTS_CARD']);
                $('.btn-product-more-list').before(result['PRODUCTS_LIST']);
                if (currentPage * 30 >= $('#total-count-items').html()) {
                    $('.btn-product-more').remove();
                } else if ((currentPage + 1) * 30 > $('#total-count-items').html()) {
                    var countItems = $('#total-count-items').html() - (currentPage * 30);
                    $('.btn-product-more__text').html('<span>Показать еще ' + countItems + '</span>');
                    $('.total-show-count').html(currentPage * 30);
                } else {
                    $('.total-show-count').html(currentPage * 30);
                }
            }
        });
        return false;
    });

    if ($('input').is('#commentAdded')) {
        MODAL.showOverlay();
        renderPopupHTML('#feedback-thank');
    }

    $('.search-cat').click(function() {
        $('#where').val($(this).data('value'));
        $('#whereText').html($(this).html());
    });

    $('.switch_view.lines').on('click',function(){
        $('.lines').addClass('active');
        $('.blocks').removeClass('active');
    });
    $('.switch_view.blocks').on('click',function(){
        $('.lines').removeClass('active');
        $('.blocks').addClass('active');
    });

    var isFixedSupported = (function(){
        var isSupported = null;
        if (document.createElement) {
            var el = document.createElement("div");
            if (el && el.style) {
                el.style.position = "fixed";
                el.style.top = "10px";
                var root = document.body;
                if (root && root.appendChild && root.removeChild) {
                    root.appendChild(el);
                    isSupported = (el.offsetTop === 10);
                    root.removeChild(el);
                }
            }
        }
        return isSupported;
    })();
    window.onload = function(){
        if (!isFixedSupported){
            // добавляем контекст для "старичков"
            document.body.className += ' no-fixed-supported';
            // имитируем position: fixed;
            var bottombar = document.getElementById('fixed-panel');
            var bottomBarHeight = bottombar.offsetHeight;
            var windowHeight = window.innerHeight;
            // обрабатываем события touch и scroll
            window.ontouchmove = function() {
                if (event.target !== bottombar){
                    bottombar.style = "";
                }
            };
            window.onscroll = function(){
                var scrollTop = window.scrollY;
                bottombar.style.bottom = (scrollTop + windowHeight - bottomBarHeight) + 'px';
            };
        }
        // первичный scroll
        window.scrollBy(0, 1);
    };




});

function setQuantity(basketId, ratio, sign, bUseFloatQuantity)
{
    var curVal = parseFloat(BX("QUANTITY_INPUT_" + basketId).value),
        newVal;

    ratio = parseInt(ratio);
    newVal = (sign == 'up') ? curVal + ratio : curVal - ratio;

    if (newVal < 0)
        newVal = 0;

    if (ratio > 0 && newVal < ratio)
    {
        newVal = ratio;
    }

    BX("QUANTITY_INPUT_" + basketId).value = newVal;
    BX("QUANTITY_INPUT_" + basketId).defaultValue = newVal;

    updateQuantity('QUANTITY_INPUT_' + basketId, basketId, ratio, bUseFloatQuantity);
}

function updateQuantity(controlId, basketId, ratio, bUseFloatQuantity)
{
    var oldVal = BX(controlId).defaultValue,
        newVal = parseFloat(BX(controlId).value) || 0,
        ratio = parseInt(ratio);


    BX(controlId).defaultValue = newVal;

    BX("QUANTITY_INPUT_" + basketId).value = newVal;

    BX("QUANTITY_" + basketId).value = newVal;

    updateBasket({'itemId': basketId, 'quantity': newVal, 'action': 'updateBasket'});
}

function updateBasket(params)
{
    BX.ajax({
        url: '/ajax/index.php',
        method: 'POST',
        data: params,
        dataType: 'json',
        onsuccess: function(result)
        {
            BX.closeWait();
            $('#quantitySmallBasket').html(parseInt(result['QUANTITY']));
            $('#priceSmallBasket').html(result['PRICE']);
            $('#current_price_' + params['itemId']).html(result['ITEM_PRICE']);
            $('#totalCount').html(getNumEnding(parseInt(result['QUANTITY']), Array('товар', 'товара', 'товаров')));
            $('#allSum_FORMATED').html(result['PRICE']);

            if (result['QUANTITY'] == 0) {
                document.location = '/catalog/';
            }
        }
    });
    return false;
}

function getNumEnding(number, endingArray)
{
    num = number;
    number = number % 100;
    if (number >= 11 && number <= 19) {
        ending = endingArray[2];
    } else {
        i = number % 10;
        switch (i) {
            case (1):
                ending = endingArray[0];
                break;
            case (2):
            case (3):
            case (4):
                ending = endingArray[1];
                break;
            default:
                ending = endingArray[2];
        }
    }

    return num + ' ' + ending;
}

function buyAccessories( elem ){
    var $that = $( elem );
    var $container = $that.closest( '.b-accessories__slider-current' );
    var $checkboxes = $container.find( '.custom_chb:checked' );
    var data = [];
    $checkboxes.each( function(){
        var id = $( this).val();
        data.push( { "id":id } );
    } );
    return data;
};

$(document).ready( function () {
        /* mask for date field */
        $('.b-makeorder__adress-data').find('input').mask('99.99.9999');
        /* remove eroor on keypress */
        var $input = $('.b-makeorder__adress').find('input:required');
        $input.on('keypress', function () {
            var $that = $(this);
            $that.removeClass('b-makeorder__adress-error');
            $that.closest('li').find('.error-tooltip').css({"display": "none"});
        });
        var $input = $('.b-makeorder__fastorder').find('input:required');
        $input.on('keypress', function () {
            var $that = $(this);
            $that.removeClass('b-makeorder__adress-error');
            $that.closest('div').find('.error-tooltip').css({"display": "none"});
        });

        $('.bx-ui-sls-input-block').find('input').on('keypress', function () {
            $('.bx-ui-sls-input-block').removeClass('b-makeorder__adress-error');
            $('#error-city').css({"display": "none"});
        });
        var $datepicker = $( '#datepicker');
        $datepicker.datepicker({
            showOtherMonths: true,
            selectOtherMonths: true
        });
        $datepicker.on( 'change', function(){
            var $that = $(this);
            $that.removeClass('b-makeorder__adress-error');
            $that.closest('li').find('.error-tooltip').css({"display": "none"});
        } );
    }
);