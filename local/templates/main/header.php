<?
if (!$_SESSION['CLIENT_NUM']) {
    $_SESSION['CLIENT_NUM'] = rand(111111, 999999);
}

if (isset($_GET['location'])) {
    $GLOBALS['locationId'] = $_GET['location'];
} elseif (isset($_SESSION['LOCATION_ID'])) {
    $GLOBALS['locationId'] = $_SESSION['LOCATION_ID'];
} elseif (isset($_COOKIE['LOCATION_ID'])) {
    $GLOBALS['locationId'] = $_COOKIE['LOCATION_ID'];
} else {
    $GLOBALS['locationId'] = 19;
}
$GLOBALS['locationName'] = CSaleLocation::GetCityLangByID($GLOBALS['locationId']);
$GLOBALS['locationName'] = $GLOBALS['locationName']['NAME'];

setcookie('LOCATION_ID', $GLOBALS['locationId'], time() + 365 * 24 * 3600);
$_SESSION['LOCATION_ID'] = $GLOBALS['locationId'];

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title><?=$APPLICATION->ShowTitle()?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?=SITE_TEMPLATE_PATH?>/css/style.css?<?=rand(1000, 9999)?>" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/stl.css?<?=rand(1000, 9999)?>" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/dev.css?<?=rand(1000, 9999)?>" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/cloud-zoom.css" type="text/css" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/css/ui-lightness/jquery-ui.min.css" type="text/css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/min/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/min/scripts.min.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/iceslider.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/carousel.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/cloud-zoom.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedit.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/modal.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js"></script>
    <script src="http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/datepicker-ru.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/modal.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/min/slider.min.js" type="text/javascript"></script>

    <?$APPLICATION->ShowHead();?>
</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div class="container">
    <div class="header">
        <div class="header-wrapper">
            <div class="info-panel">
                <ul class="info-list">
                    <li>
                        <span class="info-label">Мой номер: </span><span class="info-number"><?=$_SESSION['CLIENT_NUM']?></span>
                    </li>
                    <li class="dropdown-menu-toggle" data-menu="location">
                        <ul>
                            <li class="your-city-link-holder">
                                <a class="" id="your-city-link" href="javascript:void(0)">Ваш город <span class="location"><?=$GLOBALS['locationName']?></span></a>
                                <div class="your-city-dropdown">
                                    <h2>Ваш город — <span class="location"><?=$GLOBALS['locationName']?></span>?</h2>
                                    <a class="js-button js-button-white fl_l your-city-yes" href="javascript:void(0)">Да</a>
                                    <a class="js-button js-button-white fl_l your-city-choice" href="javascript:void(0)">Выбрать другой город</a>
                                    <div class="clear"></div>
                                    <span style="white-space:normal;">От выбранного города зависят цены, наличие товара и способы доставки</span>
                                    <input type="hidden" value="<?=$GLOBALS['locationName']?>" name="selected_city"/>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li> </li>
                    <!--<li><a href=""><span class="info-order">Что с моим заказом?</span></a></li>-->
                    <li>
                        <a href="/faq"><span class="info-help">Помощь </span></a>
                    </li>
                    <li>
                        <a href=""><span class="ico ico-enter"></span><span class="info-cabinet">Вход</span></a>
                    </li>
                </ul>
            </div>
            <div class="contacts-panel" itemscope itemtype="http://schema.org/Store">
                <ul class="contacts-list">
                    <li>
                        <a href="/"><img class="logo" src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" itemprop="logo" alt=""/></a>
                        <input type="hidden" itemprop="name" value="Merlion">
                        <input type="hidden" itemprop="address" value="г.Москва">
                    </li>
                    <li>
                        <?$APPLICATION->IncludeFile('include_areas/contactsPhone.php')?>
                    </li>
                    <li>
                        <?$APPLICATION->IncludeFile('include_areas/freePhone.php')?>
                    </li>
                    <li>
                        <?$APPLICATION->IncludeComponent("bitrix:menu", "mainMenu", Array(
                            "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                            "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                            "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                            "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                            "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                            "MAX_LEVEL" => "2",	// Уровень вложенности меню
                            "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                            "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            "DELAY" => "N",	// Откладывать выполнение шаблона меню
                            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        ),
                            false
                        );?>
                    </li>
                </ul>
            </div>
            <div class="catalog-panel">
                <div class="catalog-menu">
                    <a class="btn btn-y dropdown-catalog-toggle" href="/#catalog">Каталог товаров<span class="ico ico-menu"></span></a>

                    <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "topMenu", array(
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => "19",
                        "SECTION_ID" => "",
                        "SECTION_CODE" => "",
                        "COUNT_ELEMENTS" => "N",
                        "TOP_DEPTH" => "3",
                        "SECTION_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "SECTION_USER_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "SECTION_URL" => "",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "36000",
                        "CACHE_GROUPS" => "N",
                        "ADD_SECTIONS_CHAIN" => "N"
                    ),
                        false
                    );?>
                </div>
                <div class="catalog-search">
                    <form id="search-form" action="/search">
                        <div class="btn btn-primary dropdown-search-toggle">
                            <span class="ico ico-search"></span>
                            <div id="whereText">Везде</div>
                            <span class="ico ico-angle-down"></span>
                            <?
                            $arSort = array('SORT');
                            $arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => 19, 'SECTION_ID' => false);
                            $resSections = CIBlockSection::GetList($arSort, $arFilter);
                            ?>
                            <ul class="dropdown-search">
                                <li class="selected"><a data-value="0" class="search-cat">Везде</a></li>
                                <?while ($arSection = $resSections->Fetch()) {?>
                                    <li><a class="search-cat" data-value="<?=$arSection['ID']?>"><?=$arSection['NAME']?></a></li>
                                <?}?>
                            </ul>
                            <input type="hidden" name="where" value="" id="where" />
                        </div>
                        <input name="q" class="form-control query" type="text" value="<?=urldecode($_GET['q']);?>">
                        <input type="submit" class="btn btn-w text-center" value="Найти">
                    </form>
                </div>
                <div class="delivery-status">
                    <span class="ico ico-delivery"></span>
                    <span class="item item-dotted">Бесплатная доставка по России</span>
                </div>
            </div>
        </div>
    </div>
    <div class="content <?=($isCatalog) ? 'catalog': ''?>">