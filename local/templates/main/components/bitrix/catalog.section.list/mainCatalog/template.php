<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bOpen = false;
$sectionUrl = '';
$sectionName = '';
foreach($arResult["SECTIONS"] as $arSection) {
    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));

    if ($arSection['DEPTH_LEVEL'] == 1 && $bOpen) {
        $bOpen = false;
        ?>
            </div>
            <div class="catalog-tab__footer">
                <a class="catalog-tab__footer-item" href="<?=$sectionUrl?>"><?=$sectionName?><span class="ico ico-angle-right"></span></a>
            </div>
        </div>
    <?
    }
    if ($arSection['DEPTH_LEVEL'] == 1) {
        $sectionName = $arSection['UF_ALL'];
        $sectionUrl = $arSection['SECTION_PAGE_URL'];
        $bOpen = true;
        $totalCount = 0; ?>
        <div class="catalog-tab__item">
            <a id="<?=$this->GetEditAreaId($arSection['ID']); ?>" class="catalog-tab__title" href="<?=$arSection['SECTION_PAGE_URL'] ?>">
                <h2 class="title"><img src="<?=CFile::GetPath($arSection['UF_MENU_ICO'])?>" class="image" /><?=$arSection['NAME'] ?></h2>
            </a>
            <div class="catalog-tab__list">
    <?
    } elseif ($totalCount < 9) {
        $totalCount++; ?>
                <a id="<?=$this->GetEditAreaId($arSection['ID']); ?>" class="catalog-tab__list-item" href="<?=$arSection['SECTION_PAGE_URL'] ?>"><?=$arSection['NAME'] ?></a>
    <?
    }
}
?>
            </div>
            <div class="catalog-tab__footer">
                <a class="catalog-tab__footer-item" href="<?=$sectionUrl?>"><?=$sectionName?><span class="ico ico-angle-right"></span></a>
            </div>
        </div>