<?php

$arRes = [];
$resSections = CIBlockPropertyEnum::GetList(
    ['SORT' => 'ASC', 'ID' => 'ASC'],
    ['IBLOCK_ID' => CATALOG_IB, 'CODE' => 'AREATYPE']
);

while ($arSection = $resSections->Fetch()) {
    if (!isset($arRes[$arSection['ID']])) {
        $arRes[$arSection['ID']] = [
            'NAME' => $arSection['VALUE'],
            'SECTIONS' => []
        ];
    }
}

foreach ($arResult["SECTIONS"] as $arSection) {
    $resElements = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => CATALOG_IB,
            'SECTION_ID' => $arSection['ID'],
            'ACTIVE' => 'Y',
            'INCLUDE_SUBSECTIONS' => 'Y'
        ],
        false,
        false,
        ['PROPERTY_AREATYPE']);

    while ($arElement = $resElements->Fetch()) {
        if (!isset($arRes[$arElement['PROPERTY_AREATYPE_ENUM_ID']]['SECTIONS'][$arSection['ID']])) {
            $arRes[$arElement['PROPERTY_AREATYPE_ENUM_ID']]['SECTIONS'][$arSection['ID']] = $arSection;
        }
    }
}

$arResult = $arRes;