<?php
$arRes = [];
foreach ($arResult["SECTIONS"] as $arSection) {
    if ($arSection['UF_POPULAR']) {
        $arRes[] = [
            'ID' => $arSection['ID'],
            'IBLOCK_ID' => $arSection['IBLOCK_ID'],
            'EDIT_LINK' => $arSection['EDIT_LINK'],
            'DELETE_LINK' => $arSection['DELETE_LINK'],
            'SECTION_PAGE_URL' => $arSection['SECTION_PAGE_URL'],
            'PICTURE' => CFile::GetFileArray($arSection['UF_PICTURE'])
        ];
    }

    if (count($arRes) >= 8) {
        break;
    }
}

$arResult['SECTIONS'] = $arRes;