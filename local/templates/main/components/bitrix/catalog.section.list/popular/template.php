<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="popular__row popular__row_pos_1">
<?
foreach($arResult["SECTIONS"] as $arSection) {
    $res = CIBlockSection::GetByID($arSection["ID"]);
    if($ar_res = $res->GetNext())
    $nameSect = $ar_res['NAME'];
    $totalCount++;
    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <a id="<?=$this->GetEditAreaId($arSection['ID']);?>" class="popular__item" href="<?=$arSection['SECTION_PAGE_URL']?>"><img class="image" src="<?=$arSection['PICTURE']['SRC']?>" alt="<?=$nameSect?>" title="<?=$nameSect?>"></a>
    <?
    if ($totalCount == 4) {?>
</div>
<div class="popular__row popular__row_pos_2">
    <?}
} ?>
</div>
<!--
<div class="popular__row popular__row_pos_1">
    <a class="popular__item" href="/catalog/Spalnja/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/1.jpg" alt="" title=""></a>
    <div class="popular__item" href="">
        <a class="popular__item w100" href="/catalog/kompyuternye-stoli/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/2.jpg" alt="" title=""></a>
        <a class="popular__item w100" href="/catalog/shkafy-v-spalnu/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/3.jpg" alt="" title=""></a>
    </div>
    <a class="popular__item" href="/catalog/detskaja-mebel/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/4.jpg" alt="" title=""></a>
    <a class="popular__item" href="/catalog/Divany-i-kresla/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/5.jpg" alt="" title=""></a>
</div>
<div class="popular__row popular__row_pos_2">
    <a class="popular__item" href="/catalog/Gostinaja/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/6.jpg" alt="" title=""></a>
    <div class="popular__item">
        <a class="popular__item w100 left0 h49" href="/catalog/Prikhozhaja/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/7.jpg" alt="" title=""></a>
        <a class="popular__item w100 left0 top51 h50" href=""><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/8.jpg" alt="" title=""></a>
    </div>
    <a class="popular__item" href="/catalog/Ofis-i-kabinet/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/9.jpg" alt="" title=""></a>
    <a class="popular__item" href="/catalog/Kukhnja-i-stoly/"><img class="image" src="<?=SITE_TEMPLATE_PATH?>/img/popular/10.jpg" alt="" title=""></a>
</div>-->