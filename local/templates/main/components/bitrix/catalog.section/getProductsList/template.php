<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!empty($arResult['ITEMS'])) {
    foreach ($arResult['ITEMS'] as $key => $arItem) { ?>
        <!--        каталог списком                -->
        <section class="b-product b-box  b-product_list-item-w-foto" id="<? echo $strMainID; ?>">
            <div class="b-box__i">
                <div class="b-product__right">
                    <div class="b-product__right__wrap">
                        <span class="js_gtm_helper" style="display: none"></span>

                        <div class="b-product__price">
                                <span class="b-price b-price_size4">
                                    <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?></span>
                                    <span class="b-price__sign"></span></span>
                            <?if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                                <del class="b-price b-price_old">
                                    <span class="num"><?= $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] ?></span>
                                    <span class="sign"></span>
                                </del>
                            <?
                            } ?>
                        </div>

                        <div
                            class="b-product-status b-product-status_theme_normal b-product-status_instock b-product__status ">
                                <span class="b-pseudolink js-qtip-click-handle js-avail-click">
                                    <?if ($arItem['CAN_BUY'] == 1) { ?>В наличии<?
                                    } ?>
                                </span>
                        </div>

                    </div>

                    <div
                        class="btn-group btn-group_theme_normal btn-group-vertical b-product__actions js-rutarget-actions">
                        <a href="" class="btn add-to-favorites" item-id="<?=$arItem['ID']?>" title="Добавить в избранное">
                            <span class="ico ico-favorite"></span>
                            <span class="btn__i">В&nbsp;избранное</span>
                        </a>
                        <a href="<?= $arItem['COMPARE_URL'] ?>" class="btn add-to-compare" title="К сравнению">
                            <span class="ico ico-compare"></span>
                            <span class="btn__i">К&nbsp;сравнению</span>
                        </a>
                        <a class="btn btn btn-primary add2basket" item-id="<?= $arItem['ID'] ?>"
                           href="<?= $arItem['ADD_URL'] ?>">Купить</a>
                    </div>

                </div>
                <?
                $arFileTmp = CFile::ResizeImageGet(
                    $arItem['DETAIL_PICTURE'],
                    array("width" => 235, "height" => 148),
                    BX_RESIZE_IMAGE_EXACT,
                    true,
                    false
                );
                ?>
                <a target="_blank" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                   class="must_be_href double-hover js-gtm-product-click">
                        <span class="b-img2 b-img2_size b-img2_size_sm b-product__img">
                            <img src="<?= $arFileTmp['src'] ?>" class="b-img2__img">
                        </span>
                </a>

                <div class="b-product__center">
                    <div class="b-product__title">
                            <span class="b-truncate helper-block">
                                <a target="_blank" class="must_be_href js-gtm-product-click"
                                   href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
                            </span>
                    </div>
                    <div class="b-product__descr">
                        <?if ($arItem['PROPERTIES']['SIZE1']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE']) { ?>
                            Длина: <?= $arItem['PROPERTIES']['SIZE1']['VALUE'] ?>, Глубина: <?= $arItem['PROPERTIES']['SIZE3']['VALUE'] ?>, Высота: <?= $arItem['PROPERTIES']['SIZE2']['VALUE'] ?>
                            <img src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                        <?
                        } ?>
                        <? if ($arItem['PROPERTIES']['COLORTEXT']['VALUE']) { ?>
                            Цвет: <?= $arItem['PROPERTIES']['COLORTEXT']['VALUE'] ?><img
                            src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                        <?
                        } ?>
                        <? if ($arItem['PROPERTIES']['SER']['VALUE']) { ?>
                            Серия: <?= $arItem['PROPERTIES']['SER']['VALUE'] ?><img
                            src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                        <?
                        } ?>
                        <? if ($arItem['PROPERTIES']['SLEEPSIZE']['VALUE']) { ?>
                            Спальное место: <?= $arItem['PROPERTIES']['SLEEPSIZE']['VALUE'] ?> см<img
                                src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                        <?
                        } ?>
                        <? if ($arItem['PROPERTIES']['MATERIAL']['VALUE']) { ?>
                            Материал: <?= $arItem['PROPERTIES']['MATERIAL']['VALUE'] ?><img
                            src="<?= SITE_TEMPLATE_PATH ?>/img/basket-delimiter.png" alt=""/>
                        <?
                        } ?>
                        <? if ($arItem['PROPERTIES']['WEIGHT']['VALUE']) { ?>
                            Вес: <?= $arItem['PROPERTIES']['WEIGHT']['VALUE'] ?> кг
                        <?
                        } ?>
                    </div>
                    <?
                    $resComments = CIBlockSection::GetList(
                        array('DATE_CREATED' => 'DESC'),
                        array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID_VALUE' => $arItem['ID']),
                        false,
                        false,
                        array('PROPERTY_RATING'));
                    $totalRating = 0;
                    $totalCount = 0;
                    while ($comment = $resComments->Fetch()) {
                        $totalRating += $comment['PROPERTY_RATING_VALUE'];
                        $totalCount++;
                    }

                    if ($totalCount > 0) {
                        $totalRating = floor($totalRating / $totalCount);
                    }
                    ?>
                    <div class="b-product__group">
                        <div class="b-product__art">Артикул <span class="num"><?= $arItem['ID'] ?></span></div>
                        <div class="b-stars-wrap g-clickable b-product__stars-wrap _small">
                            <?if ($totalRating > 0) { ?>
                                <div class="b-small-stars _s4 g-clickable js-qtip-click-handle">
                                    <i class="s1"></i>
                                    <i class="s2"></i>
                                    <i class="s3"></i>
                                    <i class="s4"></i>
                                    <i class="s5"></i>
                                </div>
                                <span class="pseudolink count"><?= $totalCount; ?></span>
                            <?
                            } ?>
                        </div>
                        <div class="b-label-group b-label-group_size_xs b-product__label2-group">
                            <!--<span class="b-label b-label_info">Выгодная покупка</span>
                            <span class="b-label b-label_info">Акция</span>-->
                            <?if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                                <span class="b-label b-label_info">Суперцена</span>
                            <?
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?
    }
}
?>
