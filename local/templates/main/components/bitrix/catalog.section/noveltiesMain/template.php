<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ITEMS'])) {?>
    <ul class="b-products__list">
        <?foreach ($arResult['ITEMS'] as $arItem) {
            $strMainID = $this->GetEditAreaId($arItem['ID'] . $key);
            ?>
            <li class="b-products__item" id="<?=$strMainID?>">
                <div class="b-products__item-container">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=$arItem['NAME']?>">
                        <img class="b-product__image" src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="">
                    </a>
                    <p class="b-product__title">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=$arItem['NAME']?>"><?=$arItem['NAME']?></a>
                        <a href="<?=$arItem['ADD_URL']?>"> | Добавить в карзину</a>
                        <a href="javascript: void(0)" class="btn add-to-favorites" item-id="<?=$arItem['ID']?>" title="Добавить в избранное">
                            <span class="ico ico-favorite"></span>
                            <span class="btn__i">В&nbsp;избранное</span>
                        </a>
                    </p>
                    <div class="b-product__price">
                        <span><?=$arItem['PRICES']['BASE']['PRINT_VALUE']?></span>
                    </div>
                </div>
            </li>
        <?}?>
    </ul>
<?
}
?>