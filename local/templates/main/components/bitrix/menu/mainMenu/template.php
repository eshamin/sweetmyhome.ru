<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)) {?>
    <ul class="contacts-menu">

    <?
    foreach($arResult as $arItem) {
        if ($arItem['PARAMS']['IS_PARENT']) {
        ?>
            <li class="item-info dropdown-menu-toggle" data-menu="info">
                <span class="item item-dotted"><?=$arItem['TEXT']?></span><span class="ico ico-angle-down"></span>
                <ul class="dropdown-menu menu-info">
        <?
        } else {
        ?>
        <li class='<?=$arItem['PARAMS']['CLASS']?>'><a href="<?=$arItem["LINK"]?>"><span class="item"><?=$arItem["TEXT"]?></span></a></li>
        <?
        }

        if ($arItem['PARAMS']['LAST']) {?>
                </ul>
            </li>
        <?}
    }?>

    </ul>
<?}?>