<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)) {?>
    <ul class="features border-top">

    <?
    foreach($arResult as $arItem) {
    ?>
        <li><a href="<?=$arItem['LINK']?>"><span><span class="ico <?=$arItem['PARAMS']['CLASS']?>"></span><?=$arItem['TEXT']?></span></a></li>
    <?}?>

    </ul>
<?}?>