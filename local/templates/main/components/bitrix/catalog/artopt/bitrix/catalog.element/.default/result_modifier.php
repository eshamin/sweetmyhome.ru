<?
global $USER;
if ($_POST['feedback'] == 'Сохранить') {
    $arProps['PLUS'] = htmlspecialchars($_POST['positive']);
    $arProps['MINUS'] = htmlspecialchars($_POST['negative']);
    $arProps['COMMENT'] = htmlspecialchars($_POST['comments']);
    $arProps['FIO'] = htmlspecialchars($_POST['username']);
    $arProps['EMAIL'] = htmlspecialchars($_POST['usermail']);
    $arProps['PRODUCT_ID'] = $arResult['ID'];
    $arProps['RATING'] = $_POST['rating'];

    if ($USER->IsAuthorized()) {
        $arProps['USER_ID'] = $USER->GetID();
    }

    $arProps['COMMENT'] = htmlspecialchars($_POST['comments']);
    $arFields['NAME'] = $arResult['NAME'];
    $arFields['ACTIVE'] = 'N';
    $arFields['PROPERTY_VALUES'] = $arProps;
    $arFields['IBLOCK_ID'] = 8;

    $el = new CIBlockElement;
    if ($el->Add($arFields)) {
        $arResult['FEEDBACK_ADDED'] = 'show';
    }

    $product = CIBlockElement::GetList(array(), array('ID' => $arResult['ID'], 'IBLOCK_ID' => 19), false, false, array('ID', 'PROPERTY_vote_sum', 'PROPERTY_vote_count', 'PROPERTY_exist_comment'))->Fetch();
    if ($_POST['rating'] > 0) {
        $vote_sum = $product['PROPERTY_VOTE_SUM_VALUE'] + $_POST['rating'];
        $vote_count = $product['PROPERTY_VOTE_COUNT_VALUE'] + 1;
        $rating = round($vote_sum / $vote_count, 2);

        CIBlockElement::SetPropertyValues($arResult['ID'], 19, $vote_sum, 'vote_sum');
        CIBlockElement::SetPropertyValues($arResult['ID'], 19, $vote_count, 'vote_count');
        CIBlockElement::SetPropertyValues($arResult['ID'], 19, $rating, 'rating');
    }

    $exist_comment = ((strlen($_POST['positive']) > 0 || strlen($_POST['negative']) > 0 || strlen($_POST['comments']) > 0) && !$product['PROPERTY_EXIST_COMMENT_VALUE']) ? 754 : '';
    CIBlockElement::SetPropertyValues($arResult['ID'], 19, $exist_comment, 'exist_comment');
}

global $arrFilter;
/*-- Цвета и модификации --*/
if ($arResult['PROPERTIES']['SERIATWO']['VALUE']) {
    $arSort = array('SORT');
    $arFilter = array(
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => 19,
        '!ID' => $arResult['ID'],
        'PROPERTY_SERIATWO' => $arResult['PROPERTIES']['SERIATWO']['VALUE']
    );
    $arSelect = array(
        'ID',
        'NAME',
        'CODE',
        'DETAIL_PAGE_URL',
        'PROPERTY_COLOR_IMAGE',
        'PROPERTY_COLORTEXT',
        'DETAIL_PICTURE',
        'CATALOG_GROUP_1',
        'PROPERTY_SIZE1',
        'PROPERTY_SIZE2',
        'PROPERTY_SIZE3',
        'PROPERTY_SER',
        'COMPARE_URL'
    );

    $resSameProducts = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, false, $arSelect);

    while ($sameProduct = $resSameProducts->GetNext()) {
        $colorImage = CIBlockPropertyEnum::GetByID($sameProduct['PROPERTY_COLORTEXT_ENUM_ID']);
        $sameProduct['PICTURE'] = $colorImage['XML_ID'];
        $arResult['SAME_PRODUCTS'][] = $sameProduct;
    }
}

/*-- Похожие товары --*/
$arSort = array('SORT', 'CATALOG_GROUP_1');
$arFilter = array(
    'ACTIVE' => 'Y',
    'IBLOCK_ID' => 19,
    'SECTION_ID' => $arResult['IBLOCK_SECTION_ID'],
    '!ID' => $arResult['ID'],
    '>=CATALOG_PRICE_1' => $arResult['PRICES']['BASE']['VALUE'] - $arResult['PRICES']['BASE']['VALUE'] * 0.1,
    '<=CATALOG_PRICE_1' => $arResult['PRICES']['BASE']['VALUE'] + $arResult['PRICES']['BASE']['VALUE'] * 0.1,
);

if ($arResult['PROPERTIES']['COLORTEXT']['VALUE']) {
    $arFilter['PROPERTY_COLORTEXT_VALUE'] = $arResult['PROPERTIES']['COLORTEXT']['VALUE'];
}

$arNavStartParams = array(
    'nTopCount' => 30
);
$arSelect = array(
    'ID', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME', 'DETAIL_PAGE_URL'
);
$resSimilarProducts = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavStartParams, $arSelect);
while ($similarProduct = $resSimilarProducts->GetNext()) {
    $arResult['SIMILAR_PRODUCTS'][] = $similarProduct;
}

/*-- Лучшие аксессуары --*/
$arFilter = array('ID' =>$arResult['IBLOCK_SECTION_ID'], 'IBLOCK_ID' => 19);
$arSelect = array('UF_SECTIONS_RELATED');
$arSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect)->Fetch();

if (count($arSection['UF_SECTIONS_RELATED']) > 0) {
    $arResult['ACCESSORIES_SECTIONS'][0] = 'Рекомендуем';
    foreach ($arSection['UF_SECTIONS_RELATED'] as $section) {
        $arSort = array('CATALOG_GROUP_1');
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => 5,
            'SECTION_ID' => $section,
        );
        $arNavStartParams = array(
            'nTopCount' => 50
        );
        $arSelect = array(
            'ID', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME', 'CODE', 'IBLOCK_SECTION_ID'
        );

        $exist = false;
        $resBestAccessories = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavStartParams, $arSelect);
        while ($bestAccessory = $resBestAccessories->Fetch()) {
            if (!$exist) {
                $arResult['ACCESSORIES'][0][] = $bestAccessory;
                $exist = true;
            }
            $arResult['ACCESSORIES'][$bestAccessory['IBLOCK_SECTION_ID']][] = $bestAccessory;
            $arSec = CIBlockSection::GetByID($bestAccessory['IBLOCK_SECTION_ID'])->Fetch();
            $arResult['ACCESSORIES_SECTIONS'][$bestAccessory['IBLOCK_SECTION_ID']] = $arSec['NAME'];
        }
    }
}

/*-- Баннеры --*/
$arSort = array('SORT');
$arFilter = array('IBLOCK_ID' => 17, 'ACTIVE' => 'Y');
$resBanners = CIBlockElement::GetList($arSort, $arFilter);
while ($banner = $resBanners->Fetch()) {
    $arResult['BANNERS'][] = $banner;
}

/*-- Вы уже смотрели --*/
if (count($_SESSION['VIEWED_PRODUCTS']) > 0) {
    $arSort = array();
    $arFilter = array(
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => 5,
        'ID' => $_SESSION['VIEWED_PRODUCTS'],
        '!ID' => $arResult['ID']
    );
    $arNavStartParams = array(
        'nTopCount' => 30
    );
    $arSelect = array(
        'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME'
    );
    $resViewedProducts = CIBlockElement::GetList($arSelect, array_merge($arFilter, $arrFilter), false, $arNavStartParams, $arSelect);
    while ($viewedProduct = $resViewedProducts->GetNext()) {
        $arResult['VIEWED_PRODUCTS'][] = $viewedProduct;
    }
}

/*-- Люди также покупают --*/
$arSelect = array(
    'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME'
);

CModule::IncludeModule('sale');
$resOrders = CSaleOrder::GetList(array(), array('BASKET_PRODUCT_ID' => $arResult['ID']));
while ($arOrder = $resOrders->Fetch()) {
    $resBaskets = CSaleBasket::GetList(array(), array('ORDER_ID' => $arOrder['ID'], '!PRODUCT_ID' => $arResult['ID']));
    while ($arBasket = $resBaskets->Fetch()) {
        $alsoProduct = CIBlockElement::GetList(
            array(),
            array_merge(array('ID' => $arBasket['PRODUCT_ID'], $arrFilter)),
            false,
            false,
            $arSelect
        )->GetNext();
        if ($alsoProduct) {
            $arResult['ALSO_PRODUCTS'][$arBasket['PRODUCT_ID']] = $alsoProduct;
        }
    }
}

$arOrder = array(
    'SHOW_COUNTER' => 'DESC'
);
$arFilter = array(
    'ACTIVE' => 'Y',
    'IBLOCK_ID' => 5,
    '!ID' => $arResult['ALSO_PRODUCTS']
);
$arNavStartParams = array(
    'nTopCount' => 30
);
$arSelect = array(
    'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME'
);
$resAlsoProducts = CIBlockElement::GetList(
    $arSelect,
    array_merge($arFilter, $arrFilter),
    false,
    $arNavStartParams,
    $arSelect
);
while ($alsoProduct = $resAlsoProducts->GetNext()) {
    $arResult['ALSO_PRODUCTS'][$alsoProduct['ID']] = $alsoProduct;
}

/*-- Отзывы --*/
$arOrder = array('PROPERTY_RATING' => 'DESC');
$arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID' => $arResult['ID']);
$arSelect = array(
    'PROPERTY_RATING',
    'PROPERTY_FIO',
    'PROPERTY_COMMENT',
    'PROPERTY_MINUS',
    'PROPERTY_PLUS',
    'PROPERTY_MAIN'
);
$arResult['STAT_COMMENTS'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
    4 => array(),
    5 => array(),
);
$resComments = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
while ($comment = $resComments->Fetch()) {
    $arResult['ALL_COMMENTS'][] = $comment;
    $arResult['STAT_COMMENTS'][$comment['PROPERTY_RATING_VALUE']][] = $comment;
    if ($comment['PROPERTY_RATING_VALUE'] > 0) {
        $arResult['RATING_COMMENTS']++;
        $arResult['STAT_COMMENTS'][$comment['PROPERTY_RATING_VALUE']]['COUNT']++;
    }

    if (strlen(trim($comment['PROPERTY_COMMENT_VALUE']['TEXT'])) > 0) {
        $arResult['SHOW_COMMENTS'][] = $comment;
    }

    if ($comment['PROPERTY_MAIN_COMMENT_VALUE'] == 'Y') {
        $arResult['MAIN_COMMENT'] = $comment;
    }

    if (count($arResult['MAIN_COMMENT']) < 1 && strlen(trim($comment['PROPERTY_COMMENT_VALUE']['TEXT'])) > 0) {
        $arResult['MAIN_COMMENT'] = $comment;
    }
}
?>