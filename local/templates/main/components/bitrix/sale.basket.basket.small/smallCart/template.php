<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arOrder = array(
    "NAME" => "ASC",
    "ID" => "ASC"
);
$arFilter = array(
    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
    "LID" => SITE_ID,
    "ORDER_ID" => "NULL"
);
$resBasket = CSaleBasket::GetList($arOrder, $arFilter);
while ($arBasket = $resBasket->Fetch()) {
    if ($arBasket['CAN_BUY'] == 'N' || $arBasket['DELAY'] == 'Y') {
        CSaleBasket::Delete($arBasket['ID']);
    }
}

$totalCount = 0;
if ($arResult["READY"]=="Y")
{
    $totalPrice = 0;
    foreach ($arResult["ITEMS"] as &$v) {
        $totalCount += $v['QUANTITY'];
        $totalPrice += $v['QUANTITY'] * $v['PRICE'];
    }
}
?>

<li <?if ($totalCount > 0) {?> class="active" <?}?>>
    <a href="/personal/cart">
        <span class="ico ico-cart"></span>
        <span class="underline">Корзина</span>
        <span class="counter" id="quantitySmallBasket"><?=$totalCount?></span>
    </a>
    <span class="cart-total" id="priceSmallBasket"><?=CurrencyFormat($totalPrice, 'RUB')?></span>
</li>