    </div>
    <div class="clear"></div>
    <div class="footer">
        <div class="seo-text">
            <div class="seo-text-wrapper">
                <div class="lamp"><span class="ico ico-lamp"></span>Заголовок сеотекста</div>
                <div class="readmore">Узнать подробнее<span class="ico ico-angle-down"></span></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="footer-wrapper">
            <div class="footer-block">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottomMenu", Array(
                    "ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MAX_LEVEL" => "2",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    ),
                    false
                );?>
                <div class="footer-social">
                    <a href="http://vkontakte.ru/" target="_blank" title="Vkontakte">
                        <span class="ico ico-vk"></span>
                    </a>
                    <a href="http://www.facebook.com/" target="_blank" title="Facebook">
                        <span class="ico ico-fb"></span>
                    </a>
                    <!--
                                            <a href="http://www.odnoklassniki.ru/" target="_blank" title="Odnoklassniki">
                                                <span class="ico ico_ok"></span>
                                            </a>
                    -->
                    <a href="http://www.twitter.com/" target="_blank" title="Twitter">
                        <span class="ico ico_tw"></span>
                    </a>
                    <a href="https://plus.google.com/" target="_blank" title="Google+">
                        <span class="ico ico_gp"></span>
                    </a>
                </div>
            </div>
            <div class="clear"></div>
            <div class="footer-block">
                <div class="rating">
                    <a href="http://market.yandex.ru"><img src="<?=SITE_TEMPLATE_PATH?>/img/yandex.jpg" alt=""></a>
                </div>
                <div class="copyright">
                    <?=date('Y')?> © Artopt. Все права защищены.
                </div>
            </div>
        </div>
    </div>
    <div class="fixed-panel">
        <div class="panel-wrapper">
            <div class="controls-left">
                <div class="controls-list">
                    <li>
<!--                        <a class="btn btn-primary"><span class="ico ico-letter"></span>Подписаться на новости</a>-->
                    </li>
                </div>
            </div>
            <div class="controls-right">
                <ul class="controls-list">

                    <li <?if (is_array(getFavorites()) && count(getFavorites()) > 0) {?>class="active" <?}?>>
                        <a href="/favorites">
                            <span class="ico ico-favorite"></span>
                            <span class="underline">Избранное</span>
                            <span class="counter" id="quantityFavorites"><?=(is_array(getFavorites())) ? count(getFavorites()) : 0?></span>
                        </a>
                    </li>

                    <?$APPLICATION->IncludeComponent("bitrix:catalog.compare.list", "footerMenu", Array(
                        "POSITION_FIXED" => "N",	// Отображать список сравнения поверх страницы
                        "AJAX_MODE" => "N",	// Включить режим AJAX
                        "IBLOCK_TYPE" => "catalog",	// Тип инфоблока
                        "IBLOCK_ID" => "5",	// Инфоблок
                        "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
                        "COMPARE_URL" => "/compare/",	// URL страницы с таблицей сравнения
                        "NAME" => "CATALOG_COMPARE_LIST",	// Уникальное имя для списка сравнения
                        "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
                        "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        ),
                        false
                    );?>

                    <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "smallCart", Array(
                        "PATH_TO_BASKET" => "/personal/cart/",	// Страница корзины
                        "PATH_TO_ORDER" => "/personal/order/",	// Страница оформления заказа
                        "SHOW_DELAY" => "N",	// Показывать отложенные товары
                        "SHOW_NOTAVAIL" => "N",	// Показывать товары, недоступные для покупки
                        "SHOW_SUBSCRIBE" => "N",	// Показывать товары, на которые подписан покупатель
                        ),
                        false
                    );?>
                </ul>

                <a href="/personal/cart" class="btn btn-y">Оформить заказ</a>
            </div>
        </div>
    </div>
</div>
    <script type="text/x-template" id="your-city-list">
        <div class="city-list">
            <h2>Выберите город</h2>
            <div class="input-holder">
                <input type="text" name="city_name" id="city_name" placeholder="Введите название города"/>
                <a class="js-button js-button-yellow" href="javascript:void(0)">Найти</a>
                <div class="abc-holder">
                    <a href="javascript:void(0)" class="showCities">А</a>
                    <a href="javascript:void(0)" class="showCities">Б</a>
                    <a href="javascript:void(0)" class="showCities">В</a>
                    <a href="javascript:void(0)" class="showCities">Г</a>
                    <a href="javascript:void(0)" class="showCities">Д</a>
                    <a href="javascript:void(0)" class="showCities">Е</a>
                    <a href="javascript:void(0)" class="showCities">Ж</a>
                    <a href="javascript:void(0)" class="showCities">З</a>
                    <a href="javascript:void(0)" class="showCities">И</a>
                    <a href="javascript:void(0)" class="showCities">Й</a>
                    <a href="javascript:void(0)" class="showCities">К</a>
                    <a href="javascript:void(0)" class="showCities">Л</a>
                    <a href="javascript:void(0)" class="showCities">М</a>
                    <a href="javascript:void(0)" class="showCities">Н</a>
                    <a href="javascript:void(0)" class="showCities">О</a>
                    <a href="javascript:void(0)" class="showCities">П</a>
                    <a href="javascript:void(0)" class="showCities">Р</a>
                    <a href="javascript:void(0)" class="showCities">С</a>
                    <a href="javascript:void(0)" class="showCities">Т</a>
                    <a href="javascript:void(0)" class="showCities">У</a>
                    <a href="javascript:void(0)" class="showCities">Ф</a>
                    <a href="javascript:void(0)" class="showCities">Х</a>
                    <a href="javascript:void(0)" class="showCities">Ц</a>
                    <a href="javascript:void(0)" class="showCities">Ч</a>
                    <a href="javascript:void(0)" class="showCities">Ш</a>
                    <a href="javascript:void(0)" class="showCities">Щ</a>
                    <a href="javascript:void(0)" class="showCities">Э</a>
                    <a href="javascript:void(0)" class="showCities">Я</a>
                </div>
                <div class="divider"></div>
                <ul class="preselected-city">
                    <li class="fl_l"><a href="?location=19">Москва</a></li>
                    <li class="fl_l"><a href="?location=30">Санкт-Петербург</a></li>
                    <li class="fl_l"><a href="?location=1699">Нижний Новгород</a></li>
                    <li class="fl_l"><a href="?location=1832">Самара</a></li>
                    <li class="fl_l"><a href="?location=2208">Екатеринбург</a></li>
                    <li class="fl_l"><a href="?location=1559">Казань</a></li>
                    <li class="fl_l"><a href="?location=2664">Омск</a></li>
                    <li class="clear">&nbsp;</li>
                </ul>
                <ul class="abc-city-list">

                    <li class="clear">&nbsp;</li>
                </ul>
            </div>
        </div>
    </script>

    <script src="<?=SITE_TEMPLATE_PATH?>/js/scripts.js?<?=rand(1000, 9999)?>" type="text/javascript"></script>
    <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter33954349 = new Ya.Metrika({ id:33954349, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/33954349" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    </body>

</html>