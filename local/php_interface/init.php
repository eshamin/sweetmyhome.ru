<?
CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');
CModule::IncludeModule('search');

require_once('include/debug.php');

define('CATALOG_IB', 19);
$GLOBALS['arrFilter'] = [];

function getNumEnding($number, $endingArray)
{
    $num = $number;
    $number = $number % 100;
    if ($number >= 11 && $number <= 19) {
        $ending = $endingArray[2];
    } else {
        $i = $number % 10;
        switch ($i) {
            case (1):
                $ending = $endingArray[0];
                break;
            case (2):
            case (3):
            case (4):
                $ending = $endingArray[1];
                break;
            default:
                $ending = $endingArray[2];
        }
    }

    return "{$num} {$ending}";
}

function getFavorites()
{
    global $USER, $APPLICATION;
    if (!$USER->IsAuthorized()) {
        $arElements = unserialize($APPLICATION->get_cookie('favorites'));
    } else {
        $idUser = $USER->GetID();
        $resUser = CUser::GetByID($idUser);
        $arUser = $resUser->Fetch();
        $arElements = unserialize($arUser['UF_FAVORITES']);
    }

    if (empty($arElements)) {
        return false;
    } else {
        return $arElements;
    }
}

AddEventHandler("search", "BeforeIndex", Array("SweetMyHomeSearchIndex", "BeforeIndexHandler"));
AddEventHandler("iblock", "OnStartIBlockElementAdd", Array("SweetMyHome", "OnBeforeIBlockElementAddHandler"));
AddEventHandler("iblock", "OnStartIBlockElementUpdate", Array("SweetMyHome", "OnBeforeIBlockElementAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockSectionAdd", Array("SweetMyHome", "OnBeforeIBlockElementAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", Array("SweetMyHome", "OnBeforeIBlockElementAddHandler"));

class SweetMyHomeSearchIndex
{
    // создаем обработчик события "BeforeIndex"
    function BeforeIndexHandler($arFields)
    {
        if($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == CATALOG_IB && substr($arFields["ITEM_ID"], 0, 1) != "S")
        {
            $arFields["PARAMS"]["iblock_section"] = array();
            //Получаем разделы привязки элемента (их может быть несколько)
            $rsSections = CIBlockElement::GetElementGroups($arFields["ITEM_ID"], true);
            while($arSection = $rsSections->Fetch())
            {
                //Сохраняем в поисковый индекс
                $arFields["PARAMS"]["iblock_section"][] = $arSection["ID"];
            }
        }

        //Всегда возвращаем arFields
        return $arFields;
    }
}

class SweetMyHome
{
	function OnBeforeIBlockElementAddHandler(&$arFields)
	{
		$arParams = array("replace_space" => "-", "replace_other" => "-");
		if (strlen($arFields["CODE"]) <= 0) {
			$arFields["CODE"] = Cutil::translit($arFields["NAME"], "ru", $arParams);
		}
	}
}
?>