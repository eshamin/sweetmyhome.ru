<?
$isMain = true;
require($_SERVER['DOCUMENT_ROOT']."/bitrix/header.php");
$APPLICATION->SetTitle("Милый Мой Дом - интернет-магазин товаров для дома на заказ");?>

<!--            слайдер\каталог-->
<div class="content-block b-tabs tabs">
    <ul>
        <li class="b-tabs__tab b-tabs__tab--active active"><a href="#slider" data-toggle="tab">Витрина</a></li>
        <li class="b-tabs__tab"><a href="#catalog" data-toggle="tab">Каталог</a></li>
    </ul>
</div>
<div class="content-block tabs-content border-bottom">
    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/slider.php')?>
    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/catalogMain.php')?>
</div>
<!--            /слайдер\каталог-->

<!--            плитка популярные категрии-->
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/popular.php')?>
<!--            /плитка популярные категрии-->

<!--            хиты продаж-->
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/bestsellers.php')?>
<!--            /хиты продаж-->

<!--            суперцена\новинки-->
<div class="content-block tabs b-tabs">
    <ul>
        <li class="b-tabs__tab active"><a href="#discount" data-toggle="tab">Супер цена</a></li>
        <li class="b-tabs__tab"><a href="#new" data-toggle="tab">Новинки</a></li>
        <li class="last"><a href="#">Все товары по супер цене</a></li>
        <li class="last"><a href="#">Показать ещё</a></li>
    </ul>
</div>
<div class="content-block tabs-content">
    <div class="tab-pane active" id="discount">
        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/superPrice.php')?>
    </div>
    <div class="tab-pane" id="new">
        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/novelties.php')?>
    </div>
</div>
<!--            /суперцена\новинки-->

<!--            баннер -->
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/bannerMain.php')?>
<!--            /баннер-->

<!--            что другие смотрят сейчас -->
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/lookAlso.php')?>
<!--            /что другие смотрят сейчас -->

<!--            seo -->
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/seo.php')?>
<!--            /seo -->

    <!--            seo -->
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/news.php')?>
    <!--            /seo -->

<?require($_SERVER['DOCUMENT_ROOT']."/bitrix/footer.php");?>