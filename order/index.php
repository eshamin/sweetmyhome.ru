<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Информация о заказе");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("Информация о заказе");
?>
    <div class="b-content__infopage b-content__delivery" style="margin-top: 50px;">
        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/leftmenu.php')?>
        <div class="b-delivery-wrapper">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/tablinks.php')?>

            <div class="b-order">
                <h1 class="b-delivery__header">Как оформить заказ?</h1>
                <p>
                    Мы осуществляем доставку в более 8 000 населенных пунктов по всей России.<br>
                    Выберите подходящий способ оформления заказа из приведенных ниже
                </p>
                <div>
                    <ul style="border: 1px solid #e6e6e6;border-radius: 3px;margin-top: 30px;">
                        <li style="border-bottom: 1px solid #e6e6e6;">
                            <div
                                style="float: left;padding-top: 20px;padding-bottom: 80px;padding-left: 3%;padding-right: 30px;width:7%;">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-easy-4.png" alt="">
                            </div>
                            <div style="line-height: 24px;margin-right: 5%;margin-bottom: 45px;width:80%;float:right;">
                                <h2 style="padding-top: 30px;">По телефону</h2>

                                <p style="line-height: 24px;">Оплата заказа производится курьеру в момент доставки после
                                    проверки вами комплектности товара. И когда вы убедитесь
                                    в соответствующем качестве, вам будет предложено расписаться в накладной за прием
                                    товара и расплатиться наличными.
                                    После оформления заказа с вами свяжется наш менеджер и уточнит состав заказа и
                                    удобное время доставки.</p>
                            </div>
                            <div class="clear"></div>
                        </li>

                        <li style="border-bottom: 1px solid #e6e6e6;">
                            <div
                                style="float: left;padding-top: 20px;padding-bottom: 80px;padding-left: 15px;padding-right: 4%;width:7%;">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/comp.png" alt=""
                                     style="margin-left: 8px;">
                            </div>
                            <div style="line-height: 24px;margin-right: 5%;margin-bottom: 45px;width:80%;float:right;">
                                <h2 style="padding-top: 30px;">На сайте</h2>

                                <p>Вы можете оформить заказ через корзину на нашем сайте в любое время суток из любого
                                    региона. Выберите понравившиеся товары в нашем каталоге (выделить синим с
                                    подчеркиванием). Добавьте в корзину понравившиеся товары, нажав кнопку "Купить" в
                                    описании товаров. После добавления в корзину всех понравившихся товаров нажмите
                                    кнопку "Корзина" или "Оформить заказ". Удалите ненужные товары в корзине, введите
                                    промокод, если он у вас есть. Затем нажмите "Продолжить". После чего вы можете
                                    выбрать либо:</p>

                                <p>• <b>Быстрый заказ</b>, где вы можете ввести имя и телефон, и завершить оформление
                                    заказа.<br>• <b>Оформить заказ</b>, где вам необходимо ввести данные о вашем адресе,
                                    дате доставки, необходимых услугах, способе доставки и оплаты и подтвердить заказ.
                                </p>

                                <p>После оформления заказа с вами свяжется менеджер в ближайшее время в рабочее время
                                    ежедневно с 9:00 до 20:00
                                    и согласует содержание заказа и удобное время доставки. Статус и номер заказа вам
                                    будут отправлены СМС сообщением
                                    на указанный вами номер телефона. </p>

                                <p>Преимущества самостоятельного оформления заказа с указанием электронной почты - это
                                    создание вашего Личного кабинета, а это:</p>

                                <p>• информирование по почте о статусе вашего заказа,<br>
                                    • возможность создавать вечные списки сравнения товаров и избранных товаров,<br>
                                    • участие в акциях,<br>
                                    • накопление бонусов для последующих скидок,<br>
                                    • информирование об акциях, скидках и рекомендациях,<br>
                                    • прочие приятные сюрпризы, которыми мы будем вас радовать и дружить с вами.</p>
                            </div>
                            <div class="clear"></div>
                        </li>

                        <li style="border-bottom: 1px solid #e6e6e6;">
                            <div
                                style="float: left;padding-top: 20px;padding-bottom: 80px;padding-left: 30px;padding-right: 3%;width:7%;">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/local.png" alt=""
                                     style="margin-left: 8px;">
                            </div>
                            <div
                                style="line-height: 24px;margin-right: 5%;margin-bottom: 45px;width: 80%;float: right;">
                                <h2 style="padding-top: 30px;">В магазине</h2>

                                <p style="line-height: 24px;">Вы можете оформить заказ, посетив наш магазин площадью 700
                                    квадратных метров по адресу г. Москва, ул. Иркутская 11 корпус 1, Бизнес центр
                                    БЭЛРАЙС 2-й этаж.
                                    Подробнее о магазине можно почитать <a href="/contacts/" target="_blank"
                                                                           style="color: #7374a4;">здесь</a></p>
                            </div>
                            <div class="clear"></div>
                        </li>

                        <li style="border-bottom: 1px solid #e6e6e6;">
                            <div
                                style="float: left;padding-top: 20px;padding-bottom: 80px;padding-left: 30px;padding-right: 3%;width:7%;">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/convert-img.png" alt=""
                                     style="margin-left: 8px;">
                            </div>
                            <div style="line-height: 24px;margin-right: 5%;margin-bottom: 45px;width:80%;float:right;">
                                <h2 style="padding-top: 30px;">Обратная связь</h2>

                                <p style="line-height: 24px;">Вы можете оформить заказ на любые товары
                                    из нашего каталога отправив нам письмо через форму обратной связи. В форме обратной
                                    связи вы можете написать текстовое сообщение и по желанию прикрепить эскиз кухни или
                                    прочие документы. На указанный вами адрес электронной почты наши специалисты в
                                    ближайшее время ответят письмо, посчитают по эскизу стоимость вашего заказа, ответят
                                    на любые вопросы, примут заказ.</p>
                            </div>
                            <div class="clear"></div>
                        </li>

                        <li>
                            <div
                                style="float: left;padding-top: 20px;padding-bottom: 80px;padding-left: 30px;padding-right: 3%;width:7%;">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/operator-online.png" alt=""
                                     style="margin-left: 8px;">
                            </div>
                            <div style="line-height: 24px;margin-right: 5%;margin-bottom: 45px;width:80%;float:right;">
                                <h2 style="padding-top: 30px;">Онлайн-консультант</h2>

                                <div>
                                    <p style="line-height: 24px;">Вы можете оформить заказ через онлайн консультанта на
                                        нашем сайте.</p>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </li>
                    </ul>
                </div>

                <div class="b-delivery" style="min-width: 910px;">
                    <h2 class="b-delivery__header">Как оформить доставку?</h2>

                    <div class="b-content__delivery-easy b-delivery-easy">
                        <ul>
                            <li class="b-delivery-easy__item">
                                <div class="b-delivery-easy__item-div">
                                    <p>Выберите любой понравившийся товар и оформите его покупку</p>
                                    <span>
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-easy-1.png" alt="">
                                    </span>
                                </div>
                            </li>
                            <li class="b-delivery-easy__item">
                                <div class="b-delivery-easy__item-div">
                                    <p>Мы автоматически предложим вам подходящий вид доставки</p>
                                    <span>
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-easy-2.png" alt="">
                                    </span>
                                </div>
                            </li>
                            <li class="b-delivery-easy__item">
                                <div class="b-delivery-easy__item-div">
                                    <p>Введите ваш адрес и выберите удобные дату и время</p>
                                    <span>
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-easy-3.png" alt="">
                                    </span>
                                </div>
                            </li>
                            <li class="b-delivery-easy__item">
                                <div class="b-delivery-easy__item-div">
                                    <p>С вами свяжется наш специалист и подтвердит время доставки</p>
                                    <span>
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-easy-4.png" alt="">
                                    </span>
                                </div>
                            </li>
                            <li class="b-delivery-easy__item">
                                <div class="b-delivery-easy__item-div">
                                    <p>Получите товар у нашего курьера в назначенное время</p>
                                    <span>
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-easy-5.png" alt="">
                                    </span>
                                </div>
                            </li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="b-delivery">
                    <h2 class="b-delivery__header">Доставка</h2>

                    <div class="b-delivery-payway">
                        <ul>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-adv-1.png" alt="">

                                <p>Доставка по Москве<br> в день заказа</p>
                            </li>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-adv-2.png" alt="">

                                <p>Доставка в 8 000<br> пунктов по России</p>
                            </li>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-adv-3.png" alt="">

                                <p>Привезем в удобное<br> время</p>
                            </li>
                            <li class="b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-adv-4.png" alt="">

                                <p>Доставка 365 дней<br> в году</p>
                            </li>
                            <li class="b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-adv-5.png" alt="">

                                <p>Более 40 служб<br> доставки</p>
                            </li>
                            <li class="b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/del-adv-6.png" alt="">

                                <p>Собственный<br> транспорт</p>
                            </li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Оплата -->

                <div class="b-delivery">
                    <h2 class="b-delivery__header">Оплата</h2>

                    <div class="b-delivery-payway">
                        <ul>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway1.png" alt="">
                                <p>Наличными курьеру</p>
                            </li>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway5.png" alt="">
                                <p>Баланс телефона</p>
                            </li>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway7.png" alt="">
                                <p>Безналичный расчет</p>
                            </li>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway2.png" alt="">
                                <p>Банковские карты</p>
                            </li>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway4.png" alt="">
                                <p>Терминалы</p>
                            </li>
                            <li class="pb30 b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway6.png" alt="">
                                <p>Банковский перевод <br>на реквизиты</p>
                            </li>
                            <li class="b-delivery-payway__item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/static/payway3.png" alt="">
                                <p>Электронные деньги</p>
                            </li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Оплата -->
                <!--  -->

                <div class="b-delivery">
                    <h2 class="b-delivery__header">Гарантии и возврат</h2>
                    <div
                        style="border: 1px solid #ddd;border-radius: 2px;background: #fff;padding: 24px 15px; padding-bottom: 60px;">
                        <ul>
                            <li>
                                <div style="float: left;width: 20%;">
								<span style="display: block;margin-left:43px;margin-top:12px;">
									<img src="<?= SITE_TEMPLATE_PATH ?>/img/static/pct.gif" alt="">
								</span>
                                    <p style="margin-top: 15px;margin-left: 8px;font-weight:bold;">Весь товар официально сертифицирован</p>
                                </div>
                            </li>
                            <li>
                                <div style="float: left;width: 20%;">
								<span style="display: block;margin-left:12px;margin-top:10px;">
									<img src="<?= SITE_TEMPLATE_PATH ?>/img/static/yandex.gif" alt="">
								</span>
                                    <p style="margin-top: 20px;margin-left: 2px;font-weight:bold;">Весь товар официально сертифицирован</p>
                                </div>
                            </li>
                            <li>
                                <div style="float: left;width: 20%;">
								<span style="display: block;margin-left:28px;margin-top:7px;">
									<img src="<?= SITE_TEMPLATE_PATH ?>/img/static/garanty.gif" alt="">
								</span>
                                    <p style="margin-top: 19px;margin-left: -9px;font-weight:bold;">Мы не работаем с некачественным товаром</p>
                                </div>
                            </li>
                            <li>
                                <div style="float: left;width: 20%;">
								<span style="display: block;margin-left:35px;margin-top:10px;">
									<img src="<?= SITE_TEMPLATE_PATH ?>/img/static/ofdiler.gif" alt="">
								</span>
                                    <p style="margin-top: 13px;margin-left: -2px;font-weight:bold;">Качество нашего сервиса подтверждено статусами </p>
                                </div>
                            </li>
                            <li>
                                <div style="float: left;width: 20%;">
								<span style="display: block;margin-left:51px;margin-top:2px;">
									<img src="<?= SITE_TEMPLATE_PATH ?>/img/static/100.png" alt="">
								</span>
                                    <p style="margin-top: 10px;margin-left: 36px;font-weight:bold;">100% гарантия<br> возврата</p>
                                </div>
                            </li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>