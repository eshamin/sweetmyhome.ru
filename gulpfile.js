var argv = require('yargs').argv;
var gulp = require('gulp');

var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var watch = require('gulp-watch');
var babel = require('gulp-babel');
var spritesmith = require('gulp.spritesmith');
var buffer = require('vinyl-buffer');
var csso = require('gulp-csso');
var imagemin = require('gulp-imagemin');
var merge = require('merge-stream');

var paths = {
    src: {
        less: './local/templates/main_v2/less/**/*.*',
        less_main: './local/templates/main_v2/less/main.less',
        vendors: './local/templates/main_v2/js/libs/*.js',
        hbs:'./local/templates/main_v2/js/tpls/*.hbs',
        app:'./local/templates/main_v2/js/app/*.js',
        sprite:'./local/templates/main_v2/img/imgforsprite/**/*.png'
    },
    dest: {
        less: './local/templates/main_v2/css/',
        vendors: './local/templates/main_v2/js/',
        app: './local/templates/main_v2/js/',
        hbs:'./local/templates/main_v2/js/app/',
        sprite:'./local/templates/main_v2/img/sprite/'
    }
};

gulp.task('sprite', function () {
    var spriteData = gulp.src(paths.src.sprite).pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '/local/templates/main_v2/img/sprite/sprite.png',
        cssName: 'sprites.css'
    }));
    // Pipe image stream through image optimizer and onto disk
    var imgStream = spriteData.img
        // DEV: We must buffer our stream into a Buffer for `imagemin`
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dest.sprite));

    // Pipe CSS stream through CSS optimizer and onto disk
    var cssStream = spriteData.css
        //.pipe(csso())
        .pipe(gulp.dest('./local/templates/main_v2/less/'));

    // Return a merged stream to handle both `end` events
    return merge(imgStream, cssStream);
});

gulp.task('less', function () {
    gulp.src(paths.src.less_main)
        .pipe(sourcemaps.init())
            .pipe(less())
            .pipe(concat('styles.css'))
            .pipe(autoprefixer({
                // см. https://github.com/ai/browserslist#queries
                browsers: ['last 5 versions', 'IE >= 8', 'Opera 12.1']
            }))
            .pipe(minifyCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.dest.less));
});


gulp.task('watch', function() {
    gulp.watch(paths.src.less, ['less']);
    gulp.watch(paths.src.hbs, ['templates']);
    gulp.watch(paths.src.vendors, ['vendors']);
    gulp.watch(paths.src.app, ['app']);
});

gulp.task('vendors', function() {
    return gulp.src(paths.src.vendors)
        .pipe(sourcemaps.init())
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(paths.dest.vendors));
});

gulp.task('app', function() {
    return gulp.src(paths.src.app)
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(paths.dest.app));
});

gulp.task('templates', function(){
    gulp.src(paths.src.hbs)
        .pipe(handlebars())
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
            namespace: 'MyApp.templates',
            noRedeclare: true, // Avoid duplicate declarations
        }))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest(paths.dest.hbs));
});

gulp.task('es6', function(){
    return gulp.src(paths.src.app)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(paths.dest.app));
});

gulp.task('all:watch', ['watch']);
gulp.task('less:build', ['less']);
gulp.task('js:vendors', ['vendors']);
gulp.task('js:app', ['app']);
gulp.task('js:templates', ['templates']);