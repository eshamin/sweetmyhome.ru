<?
$isCatalog = true;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?>
<div class="content-wrapper">
    <div class="content-block sliderBanners-wrapper">
<?
if (intval($_GET['where']) > 0) {
    $parentSection = CIBlockSection::GetList(
        array(),
        array('ID' => intval($_GET['where']), 'IBLOCK_ID' => CATALOG_IB)
    )->Fetch();

    $arOrder = array('SORT' => 'ASC');
    $arFilter = array(
        'IBLOCK_ID' => CATALOG_IB,
        'GLOBAL_ACTIVE' => 'Y',
        '>LEFT_MARGIN' => $parentSection['LEFT_MARGIN'],
        '<RIGHT_MARGIN' => $parentSection['RIGHT_MARGIN'],
    );
    $searchSections = array();
    $resSections = CIBlockSection::GetList($arOrder, $arFilter);
    while ($arSection = $resSections->Fetch()) {
        $searchSections[] = $arSection['ID'];
    }
    $GLOBALS['arrFilter']['PARAMS'] = array("iblock_section" => $searchSections);
}
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.search",
        "artopt",
        Array(
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "IBLOCK_TYPE" => "catalog",	// Тип инфоблока
            "IBLOCK_ID" => CATALOG_IB,	// Инфоблок
            "ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
            "ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
            "ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
            "ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
            "SECTION_URL" => "/catalog/#SECTION_CODE#/",	// URL, ведущий на страницу с содержимым раздела
            "DETAIL_URL" => "/catalog/#SECTION_CODE#/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
            "BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
            "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
            "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
            "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
            "SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
            "DISPLAY_COMPARE" => "Y",	// Выводить кнопку сравнения
            "PAGE_ELEMENT_COUNT" => "30",	// Количество элементов на странице
            "LINE_ELEMENT_COUNT" => "4",	// Количество элементов выводимых в одной строке таблицы
            "PROPERTY_CODE" => array(	// Свойства
                0 => "SER",
                1 => "WEIGHT",
                2 => "SIZE1",
                3 => "SIZE2",
                4 => "SIZE3",
                5 => "COLORTEXT",
                6 => "MATERIAL",
                7 => "",
            ),
            "OFFERS_FIELD_CODE" => "",
            "OFFERS_PROPERTY_CODE" => "",
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_ORDER" => "asc",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER2" => "desc",
            "OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
            "PRICE_CODE" => array(	// Тип цены
                0 => "BASE",
            ),
            "USE_PRICE_COUNT" => "Y",	// Использовать вывод цен с диапазонами
            "SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
            "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
            "USE_PRODUCT_QUANTITY" => "Y",	// Разрешить указание количества товара
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "RESTART" => "Y",	// Искать без учета морфологии (при отсутствии результата поиска)
            "NO_WORD_LOGIC" => "N",	// Отключить обработку слов как логических операторов
            "USE_LANGUAGE_GUESS" => "Y",	// Включить автоопределение раскладки клавиатуры
            "CHECK_DATES" => "Y",	// Искать только в активных по дате документах
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
            "PAGER_TITLE" => "Товары",	// Название категорий
            "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
            "PAGER_DESC_NUMBERING" => "Y",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
            "HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
            "CONVERT_CURRENCY" => "Y",	// Показывать цены в одной валюте
            "CURRENCY_ID" => "RUB",	// Валюта, в которую будут сконвертированы цены
            "OFFERS_CART_PROPERTIES" => "",
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "Y",	// Включить эмуляцию навигации браузера
            "COMPONENT_TEMPLATE" => ".default",
            "AJAX_OPTION_ADDITIONAL" => "undefined",	// Дополнительный идентификатор
            "PRODUCT_PROPERTIES" => "",	// Характеристики товара
        ),
        false
    );
?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>