<?
CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');
CModule::IncludeModule('search');

function debugMe($val)
{
    $file = fopen("{$_SERVER['DOCUMENT_ROOT']}/debug.txt", 'a+');
    ob_start();
    print_r($val);
    $content = ob_get_contents();
    ob_end_clean();
    fputs($file, date('Y-m-d H:i:s') . " " . $content . "\n");
    fclose($file);
}

if(!function_exists(d)) {
    function d($var)
    {
        global $USER;
        if($USER->IsAdmin()) {
            echo "<div style='clear:both'></div>";
            echo "<pre style='background: #FFF'>";
            print_r($var);
            echo "</pre>";
        }
        return;
    }
}

function getNumEnding($number, $endingArray)
{
    $num = $number;
    $number = $number % 100;
    if ($number >= 11 && $number <= 19) {
        $ending = $endingArray[2];
    } else {
        $i = $number % 10;
        switch ($i) {
            case (1):
                $ending = $endingArray[0];
                break;
            case (2):
            case (3):
            case (4):
                $ending = $endingArray[1];
                break;
            default:
                $ending = $endingArray[2];
        }
    }

    return "{$num} {$ending}";
}

function getFavorites()
{
    global $USER, $APPLICATION;
    if (!$USER->IsAuthorized()) {
        $arElements = unserialize($APPLICATION->get_cookie('favorites'));
    } else {
        $idUser = $USER->GetID();
        $resUser = CUser::GetByID($idUser);
        $arUser = $resUser->Fetch();
        $arElements = unserialize($arUser['UF_FAVORITES']);
    }

    if (empty($arElements)) {
        return false;
    } else {
        return $arElements;
    }
}

function getProductOfDay($categoryId)
{
    global $arrFilter;
    $arSort = array(
        'SHOW_COUNTER' => 'DESC'
    );
    $arFilter = array(
        'SECTION_ID' => $categoryId,
        'ACTIVE' => 'Y'
    );

    $arProperties = array(
        'ID',
        'NAME',
        'CATALOG_GROUP_1',
    );

    $arProduct = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, false, array($arProperties));
    ?>
    <img src="<?=SITE_TEMPLATE_PATH?>/img/good123.jpg" alt=""/>
    <span class="b-top-panel__feedback-stars">
            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
            <a href="javascript:void(0)">3</a>
        </span>
    <p>Спальня АКМ | Кровать 160х200 см Сонет с Подъемным Механизмом...</p>
    <span class="price">24 590. -</span>
    <a class="js-button js-button-white" href="javascript:void(0)">Купить</a>
    <a href="#" class="sevenday"><img src="<?=SITE_TEMPLATE_PATH?>/img/bullet.png" alt=""/>&nbsp;&nbsp;Под заказ 7 дней</a>
<?}

AddEventHandler("search", "BeforeIndex", Array("AkmmosSearchIndex", "BeforeIndexHandler"));

class AkmmosSearchIndex
{
    // создаем обработчик события "BeforeIndex"
    function BeforeIndexHandler($arFields)
    {
        if($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 5 && substr($arFields["ITEM_ID"], 0, 1) != "S")
        {
            $arFields["PARAMS"]["iblock_section"] = array();
            //Получаем разделы привязки элемента (их может быть несколько)
            $rsSections = CIBlockElement::GetElementGroups($arFields["ITEM_ID"], true);
            while($arSection = $rsSections->Fetch())
            {
                //Сохраняем в поисковый индекс
                $arFields["PARAMS"]["iblock_section"][] = $arSection["ID"];
            }
        }

        //Всегда возвращаем arFields
        return $arFields;
    }
}
?>