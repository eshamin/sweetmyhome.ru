<?
$module_id = "twofingers.checkout";
IncludeModuleLangFile(__FILE__);
include_once($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/include.php");
CModule::Includemodule('iblock');
CModule::Includemodule('sale');
CModule::Includemodule('catalog');
if(isset($_POST)) {
	if (isset($_POST['TF_CHECKOUT_KEY'])) {
		TF_CHECKOUT_Settings::SetSettings($_POST);
	}
}
$arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC");
$db_ptype = CSalePaySystem::GetList($arOrder);
while ($ptype = $db_ptype->Fetch()) {
	$paySystems[$ptype['ID']] = $ptype['NAME'];
}

$OrderStatuses = array();
$db_StatusOrder = CSaleStatus::GetList($arOrder);
while($arStatus = $db_StatusOrder->GetNext()){
    $OrderStatuses[$arStatus['ID']] = $arStatus['NAME'];
}

$db_ptype = CSalePersonType::GetList($arOrder, Array("ACTIVE"=>"Y"));
while ($ptype = $db_ptype->Fetch())
{
	$arPersonTypeIDs[] = $ptype['ID'];
	$arPersonTypeProps[$ptype['ID']] =  array('NAME'=>$ptype['NAME'], 'PROPS'=>array(), 'LIDS'=>$ptype['LIDS']);
}

$db_props = CSaleOrderProps::GetList(
		array("SORT" => "ASC"),
		array(
				"PERSON_TYPE_ID" => $arPersonTypeIDs,
				"TYPE"           => array('TEXT', 'TEXTAREA')
		),
		false,
		false,
		array('ID', 'NAME', 'PERSON_TYPE_ID')
);
while($arProps = $db_props->GetNext()){
	$arPersonTypeProps[$arProps['PERSON_TYPE_ID']]['PROPS'][] = $arProps;
}

$arPropsForCheckout = array("NAME", "ADDRESS", "EMAIL", "PHONE");

$settings = TF_CHECKOUT_Settings::GetSettings();
$MOD_RIGHT = $APPLICATION->GetGroupRight($module_id);
if($MOD_RIGHT>="W" && true):
	$aTabs = array(
		0 => array(
			'TAB'=>GetMessage('TF_CHECKOUT_SETTINGS_TAB'), 
			'DIV'=>'edit1', 
			'TITLE' => GetMessage('TF_CHECKOUT_SETTINGS_TAB_TITLE'),
		)
	);
	$tab = new CAdminTabControl('TwoFingers_Settings_tab', $aTabs);
	$tab->Begin();
	
?>
	<form method="post">
<?	$tab->BeginNextTab();?>
        <tr class="heading">
            <td colspan="2"><?=GetMessage('TF_MAIN_SETTING_HEADING') ?></td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap"><?=GetMessage('TF_CHECKOUT_KEY') ?>:</td>
			<td width="60%">
				<input type="text" name="TF_CHECKOUT_KEY"  value="<?=$settings['TF_CHECKOUT_KEY']?>" />
			</td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="adm-info-message-wrap" align="center">
                    <div class="adm-info-message" style="margin: 0px;"><?=GetMessage('TF_INFORMATION_ABOUT_CHECKOUT');?></div>
                </div>
            </td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap"><?=GetMessage('TF_CHECKOUT_DEFAULT_WEIGHT') ?>:</td>
			<td width="60%">
				<input type="text" name="TF_CHECKOUT_DEFAULT_WEIGHT"  value="<?=$settings['TF_CHECKOUT_DEFAULT_WEIGHT']?>" />
			</td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap"><?=GetMessage('TF_CHECKOUT_PAY_SYSTEMS') ?>:</td>
			<td width="60%">
				<select size="5" multiple="multiple" name="TF_CHECKOUT_PAY_SYSTEMS[]">
<?		foreach ($paySystems as $id=>$system):?>
					<option value="<?=$id?>" <?if (in_array($id, $settings['TF_CHECKOUT_PAY_SYSTEMS'])):?> selected<?endif?>><?=$system?></option>
<?		endforeach?>
				</select>
			</td>
        </tr>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap"><?=GetMessage('TF_ORDER_STATUS_SEND') ?>:</td>
            <td width="60%">
                <select name="TF_order_status_send">
                    <option value="empty"><?=GetMessage('EMPTY_SELECT_VALUE') ?></option>
                    <?		foreach ($OrderStatuses as $idStatus=>$StatusName):?>
                        <option value="<?=$idStatus?>" <?if ($idStatus == $settings['TF_order_status_send']):?> selected<?endif?>><?=$StatusName?></option>
                    <?		endforeach?>
                </select>
            </td>
        </tr>
        <? foreach($arPersonTypeProps as $idPerson => $arPerson):?>
        <tr class="heading">
            <td colspan="2"><?=GetMessage('TF_PERSON_TYPE_BLOCK_HEADING') ?><?=$arPerson['NAME']?> [<?foreach($arPerson['LIDS'] as $k => $lid) $separator = ($k+1!=count($arPerson['LIDS']))?',':''; echo $lid.$separator?>]</td>
        </tr>
	        <? foreach ($arPropsForCheckout as $codeProp):?>
	       	<tr>
	       		<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap"><?=GetMessage('TF_CHOISE_PROP_FOR_'.$codeProp) ?>:</td>
	       		<td width="60%">
	       			<select name='TF_CHECKOUT_PERSON_PROPS[<?=$idPerson?>][<?=$codeProp?>]'>
	       				<option value="empty"><?=GetMessage('EMPTY_SELECT_VALUE') ?></option>
	       				<? foreach($arPerson['PROPS'] as $arProp):?>
		       				<option value="<?=$arProp['ID']?>" <?if ($arProp['ID'] == $settings['TF_CHECKOUT_PERSON_PROPS'][$idPerson][$codeProp]):?> selected<?endif?>><?=$arProp['NAME']?></option>
		       			<? endforeach;?>
	       			</select>
	       		</td>
	        </tr>
	        <? endforeach;?>
        <? endforeach;?>
        <!--<tr>
            <td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap"><?/*=GetMessage('TF_ORDER_STATUS_DELIVERY_DONE') */?>:</td>
            <td width="60%">
                <select name="order_status_delivery_done">
                    <option value="empty"><?/*=GetMessage('EMPTY_SELECT_VALUE') */?></option>
                    <?/*		foreach ($OrderStatuses as $idStatus=>$StatusName):*/?>
                        <option value="<?/*=$idStatus*/?>" <?/*if ($idStatus == $settings['order_status_delivery_done']):*/?> selected<?/*endif*/?>><?/*=$StatusName*/?></option>
                    <?/*		endforeach*/?>
                </select>
            </td>
        </tr>-->
        <tr class="heading">
            <td colspan="2"><?=GetMessage('TF_SCRTIPS_BLOCK_HEADING') ?></td>
        </tr>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap"><?=GetMessage('TF_INCLUDE_JQUERY') ?>:</td>
            <td width="60%">
                <input type="checkbox" name="TF_jquery_include" value="Y"<?if($settings['TF_jquery_include']=="Y")echo" checked";?>>
            </td>
        </tr>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap"><?=GetMessage('TF_INCLUDE_YMAPS') ?>:</td>
            <td width="60%">
                <input type="checkbox" name="TF_ymaps_include" value="Y"<?if($settings['TF_ymaps_include']=="Y")echo" checked";?>>
            </td>
        </tr>


<?	$tab->EndTab();?>
<?	$tab->Buttons();?>
	<input type="submit" name="TF_CHECKOUT_SAVE_SETTINGS" class="adm-btn-save"  value="<?=GetMessage('TF_CHECKOUT_SAVE') ?>" title="<?=GetMessage('TF_CHECKOUT_SAVE_TITLE') ?>" />
	<input type="button" onclick="window.document.location = '?lang=<?=LANGUAGE_ID ?>'" value="<?=GetMessage('TF_CHECKOUT_CANCEL') ?>" title="<?=GetMessage('TF_CHECKOUT_CANCEL_TITLE') ?>" />
</form>
<?$tab->End();?>
<?endif;?>