<?
if (!class_exists('TF_CHECKOUT_admin')) {
    CModule::IncludeModule("sale");
    if($_REQUEST['ID'] && $_SERVER['SCRIPT_NAME']=='/bitrix/admin/sale_order_new.php' && CModule::IncludeModule('twofingers.checkout')){
        include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/twofingers.checkout/classes/TF_EditOrder.php');
    }
    class TF_CHECKOUT_admin{
        function generatePlaceSelect($type, $delivery, $name, $city, $person_type) {
            $deliveries = CDeliveryCheckoutPlatform::GetDelivery();

            if(LANG_CHARSET != 'UTF-8'){
                $checkedPoint = iconv("utf-8", LANG_CHARSET, $delivery[$type]['addresses'][$_REQUEST['checkout_place_'.$type.'_address_id']]);
            }else{
                $checkedPoint = $delivery[$type]['addresses'][$_REQUEST['checkout_place_'.$type.'_address_id']];
            }

            if($_REQUEST['checkout_place_'.$type.'_address'] != $checkedPoint){
                unset($_REQUEST['checkout_place_'.$type.'_address_id']);
                unset($_REQUEST['checkout_place_'.$type.'_address']);
            }

            $select = '';
            $choice_word = GetMessage('EMPTY_SELECT_VALUE');
            $select = '<a href="javascript:;" ';
            $select .= 'class="tf-choise-points" ';
            $select .= 'data-type="'.$type.'" ';
            $select .= 'data-location="'.$city['LOCATION'].'" ';
            $select .= 'data-text="'.$choice_word.' '.$name.'">'.$choice_word.' '.$name.'</a>';

            $select .= "<ul class=\"regions_list ".$type."\" style=\"display:none;\">";
            foreach ($delivery[$type]['addresses'] as $key=>$address) {
                if(LANG_CHARSET != 'UTF-8'){
                    $address = iconv("utf-8", LANG_CHARSET, $address);
                    $delivery[$type]['additionalInfo'][$key] = iconv("utf-8", LANG_CHARSET, $delivery[$type]['additionalInfo'][$key]);
                }
                $select .= '<li class="'.$type.'_select_'.$key.'">';
                $select .= '<a class="tf-choise-point" data-location="'.$city['LOCATION'].'" ';
                $select .= 'data-latitudes="'.$delivery[$type]['latitudes'][$key].'" ';
                $select .= 'data-longitudes="'.$delivery[$type]['longitudes'][$key].'" ';
                $select .= 'data-id-point="'.$key.'" ';
                $select .= 'data-type="'.$type.'" ';
                $select .= 'data-image-point="'.$deliveries[$delivery[$type]['deliveries'][$key]]['IMAGE'].'" ';
                $select .= 'data-name-point="'.htmlspecialcharsEx($address).'" ';
                $select .= 'data-desc-point=\''.CDeliveryCheckoutPlatform::strip(str_replace(Array('\r\n', '\r', '\n', PHP_EOL), PHP_EOL, str_replace('\'', '', $delivery[$type]['additionalInfo'][$key]))).'\' ';
                $select .= 'data-delivery-price-point="'.$delivery[$type]['costs'][$key].'" ';
                $select .= 'href="javascript:;">'.$address.'</a></li>';
            }
            $select .= "</ul>";

            return $select;
        }

        function __GetLocationPrice($LOCATION_ID, $arConfig, $arOrder, $OrderID = false) {
            $arDetailOrderInfo = CSaleOrder::GetByID($OrderID);
            $settings = TF_CHECKOUT_Settings::GetSettings();
            $ticket = CDeliveryCheckoutPlatform::getTicket();
            $city = CDeliveryCheckoutPlatform::GetCityID($LOCATION_ID);
            $itemsCount = 0;
            $PropAddressID = $settings['TF_CHECKOUT_PERSON_PROPS'][$arDetailOrderInfo['PERSON_TYPE_ID']]['ADDRESS'];

            $resPropPointVal = CSaleOrderPropsValue::GetList(array(), array("ORDER_ID"=>$OrderID, "ORDER_PROPS_ID"=>$PropAddressID));
            if($arPropPointVal=$resPropPointVal->GetNext()){
                preg_match('/\[([0-9]*)\]/', $arPropPointVal['VALUE'], $matches);
                $IdPoint = $matches[1];
            }

            $dbBasketItems = CSaleBasket::GetList(
                array("NAME" => "ASC","ID" => "ASC"),
                array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => $OrderID),
                false,
                false,
                array()
            );
            while($items = $dbBasketItems->GetNext()){
                $orderWeight += $items['QUANTITY'];
                $itemsCount++;
            }
            
            $TotalWeight = ($arOrder['WEIGHT']>0)?floatval($arOrder['WEIGHT'])/1000:$settings['TF_CHECKOUT_DEFAULT_WEIGHT']/1000*$orderWeight;

            $delivery = CDeliveryCheckoutPlatform::RequestPricePoints($ticket, $city['ID'], intval($arOrder['PRICE']), intval($arOrder['PRICE']), $TotalWeight, $itemsCount);

            $deliveries = CDeliveryCheckoutPlatform::GetDelivery();
            if (!in_array($arDetailOrderInfo['PAY_SYSTEM_ID'], $settings['TF_CHECKOUT_PAY_SYSTEMS'])
                || (isset($_REQUEST['payment_id']) && !in_array($_REQUEST['payment_id'], $settings['TF_CHECKOUT_PAY_SYSTEMS']))) {
                $delivery = CDeliveryCheckoutPlatform::RequestPricePoints($ticket, $city['ID'], 0, intval($arOrder['PRICE']), $TotalWeight, $itemsCount);
            }

            $return = Array();

            if (isset($delivery['express']['cost'])){
                $return['express'] = Array(
                    'cost' => $delivery['express']['cost'],
                    'from' => $delivery['express']['minDeliveryTerm'],
                    'to' => $delivery['express']['maxDeliveryTerm'],
                );
            }

            if (isset($delivery['postamat']['cost'])) {
                $select = self::generatePlaceSelect('postamat', $delivery, GetMessage('TF_POSTAMAT_NAME'), $city);
                $return['postamat'] = Array(
                    'cost' => $delivery['postamat']['cost'],
                    'from' => $delivery['postamat']['minDeliveryTerm'],
                    'to' => $delivery['postamat']['maxDeliveryTerm'],
                    'select' => $select
                );
                if (isset($IdPoint) && $IdPoint != ""){
                    $return['postamat']['cost'] = $delivery['postamat']['costs'][$IdPoint];
                }
            }

            if (isset($delivery['pvz']['cost'])) {
                $select = self::generatePlaceSelect('pvz', $delivery, GetMessage('TF_PVZ_NAME'), $city);

                $return['pvz'] = Array(
                    'cost' => $delivery['pvz']['cost'],
                    'from' => $delivery['pvz']['minDeliveryTerm'],
                    'to' => $delivery['pvz']['maxDeliveryTerm'],
                    'select' => $select
                );
                if (isset($IdPoint) && $IdPoint != ""){
                    $return['pvz']['cost'] = $delivery['pvz']['costs'][$IdPoint];
                }
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'http://twofingers.ru/co.php?site='.$_SERVER['SERVER_NAME'].'&adminPage=Y');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_exec($curl);
            curl_close($curl);

            return $return;
        }

        function Calculate($profile, $arConfig, $arOrder, $OrderID = false, $STEP, $TEMP = false) {
            $arResult = self::__GetLocationPrice($arOrder["LOCATION_TO"], $arConfig, $arOrder, $OrderID);
            $return = array(
                "RESULT" => "OK",
                "VALUE" => $arResult[$profile]['cost'],
                'TRANSIT' => $arResult[$profile]['from'].' - '.$arResult[$profile]['to'].'<span class="checkout_block checkout_place_'.$profile.'_address">'.$_REQUEST['checkout_place_'.$profile.'_address'].'<input type="hidden" class="checkout_place_'.$profile.'_address checkout_input_address" value="'.$_REQUEST['checkout_place_'.$profile.'_address'].'" name="checkout_place_'.$profile.'_address"><input type="hidden" class="checkout_place_'.$profile.'_address_id checkout_input_address_id" value="'.$_REQUEST['checkout_place_'.$profile.'_address_id'].'" name="checkout_place_'.$profile.'_address_id"><span class="checkout_place">'.$arResult[$profile]['select'].'</span></span>',
                'TRANSIT_CHECKOUT' => GetMessage('TRANSIT_CHECKOUT_FROM').$arResult[$profile]['from'].GetMessage('TRANSIT_CHECKOUT_TO').$arResult[$profile]['to'],
            );
            return $return;
        }

        function GetLinkPointsToShowModal($profile, $arConfig, $arOrder, $OrderID = false, $STEP, $TEMP = false){
            $arResult = self::__GetLocationPrice($arOrder["LOCATION_TO"], $arConfig, $arOrder, $OrderID);
            return ($arResult[$profile]['select'])?$arResult[$profile]['select']:'';
        }

        function CalculateDeliveryOnEditPageAdmin($SID, $profile, $arOrder, $arReturn){
            $ID = ($_REQUEST['ID'])?$_REQUEST['ID']:$_REQUEST['id'];
            if($ID && $_SERVER['SCRIPT_NAME']=='/bitrix/admin/sale_order_new.php'){
                $result_return = self::Calculate($profile, $arConfig, $arOrder, $ID);
                return $result_return;
            }
        }
    }
}
?>