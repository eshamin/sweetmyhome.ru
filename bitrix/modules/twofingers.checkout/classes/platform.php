<?
if (!class_exists('CDeliveryCheckoutPlatform')) {
    IncludeModuleLangFile(__FILE__);
    CModule::IncludeModule('twofingers.checkout');
    CModule::IncludeModule("sale");
    class CDeliveryCheckoutPlatform {
        public static $path_to_images = '/bitrix/images/twofingers.checkout/deliveryes/';

        function strip($b1) {
            $b1 = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $b1);
            $b1 = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $b1);
            return $b1;
        }

        function Init() {
            return array(
                "SID" => "CheckoutPlatform",
                "NAME" => "Checkout.ru",
                "DESCRIPTION" => "",
                "DESCRIPTION_INNER" => "",
                "BASE_CURRENCY" => COption::GetOptionString("sale", "default_currency", "RUB"),
                "HANDLER" => __FILE__,
                /* Методы обработчика */
                "DBGETSETTINGS" => array("CDeliveryCheckoutPlatform", "GetSettings"),
                "DBSETSETTINGS" => array("CDeliveryCheckoutPlatform", "SetSettings"),
                //"GETCONFIG" => array("CDeliveryCheckoutPlatform", "GetConfig"),
                "COMPABILITY" => array("CDeliveryCheckoutPlatform", "Compability"),
                "CALCULATOR" => array("CDeliveryCheckoutPlatform", "Calculate"),
                /* Список профилей доставки */
                "PROFILES" => array(
                    "express" => array(
                        "TITLE" => GetMessage('COURIER_DELIVERY'),
                        "DESCRIPTION" => GetMessage('COURIER_DELIVERY_DESC'),
                        "RESTRICTIONS_WEIGHT" => array(0), // без ограничений
                        "RESTRICTIONS_SUM" => array(0), // без ограничений
                    ),
                    "mail" => array(
                        "TITLE" => GetMessage('MAIL_DELIVERY'),
                        "DESCRIPTION" => GetMessage('MAIL_DELIVERY_DESC'),
                        "RESTRICTIONS_WEIGHT" => array(0), // без ограничений
                        "RESTRICTIONS_SUM" => array(0), // без ограничений
                    ),
                    "postamat" => array(
                        "TITLE" => GetMessage('POSTAMAT_DELIVERY'),
                        "DESCRIPTION" => GetMessage('POSTAMAT_DELIVERY_DESC'),
                        "RESTRICTIONS_WEIGHT" => array(0), // без ограничений
                        "RESTRICTIONS_SUM" => array(0), // без ограничений
                    ),
                    "pvz" => array(
                        "TITLE" => GetMessage('PVZ_DELIVERY'),
                        "DESCRIPTION" => GetMessage('PVZ_DELIVERY_DESC'),
                        "RESTRICTIONS_WEIGHT" => array(0), // без ограничений
                        "RESTRICTIONS_SUM" => array(0), // без ограничений
                    ),
                    "express_own" => array(
                        "TITLE" => GetMessage('OWN_DELIVERY'),
                        "DESCRIPTION" => GetMessage('OWN_DELIVERY_DESC'),
                        "RESTRICTIONS_WEIGHT" => array(0), // без ограничений
                        "RESTRICTIONS_SUM" => array(0), // без ограничений
                    ),
                ),
                'ISNEEDEXTRAINFO' => 'Y'
            );
        }

        function GetDelivery() {
            return Array(
                2 => Array(
                    'NAME' => 'PickPoint',
                    'MARGIN' => 2,
                    'MIN_MARGIN' => 0,
                    'IMAGE' => self::$path_to_images.'pickpoint.jpg'
                ),
                4 => Array(
                    'NAME' => 'Hermes',
                    'MARGIN' => 3,
                    'MIN_MARGIN' => 0,
                    'IMAGE' => self::$path_to_images.'hermes.jpg'
                ),
                5 => Array(
                    'NAME' => GetMessage('SPSR_NAME'),
                    'MARGIN' => 2.4,
                    'MIN_MARGIN' => 0,
                    'IMAGE' => self::$path_to_images.'spsr.jpg'
                ),
                6 => Array(
                    'NAME' => 'DPD Parcel',
                    'MARGIN' => 2,
                    'MIN_MARGIN' => 0,
                    'IMAGE' => self::$path_to_images.'dpd.jpg'
                ),
                7 => Array(
                    'NAME' => 'DPD Consumer',
                    'MARGIN' => 2,
                    'MIN_MARGIN' => 40,
                    'IMAGE' => self::$path_to_images.'dpd.jpg'
                ),
                8 => Array(
                    'NAME' => 'Shoplogistic',
                    'MARGIN' => 1.5,
                    'MIN_MARGIN' => 40,
                    'IMAGE' => self::$path_to_images.'shoplogistic.jpg'
                ),
                9 => Array(
                    'NAME' => 'Boxberry',
                    'MARGIN' => 1.8,
                    'MIN_MARGIN' => 0,
                    'IMAGE' => self::$path_to_images.'boxberry.jpg'
                ),
                10 => Array(
                    'NAME' => 'DPD Ecoomy',
                    'MARGIN' => 2,
                    'MIN_MARGIN' => 40,
                    'IMAGE' => self::$path_to_images.'spsr.jpg'
                ),
            );
        }

        // настройки обработчика
        function GetConfig() {
        }

        // подготовка настроек для занесения в базу данных
        function SetSettings($arSettings) {
            return serialize($arSettings);
        }

        // подготовка настроек, полученных из базы данных
        function GetSettings($strSettings) {
            // вернем десериализованный массив настроек
            return unserialize($strSettings);
        }

        function CheckedDeliveryCalculate(&$arResult, &$arUserResult, $arParams){

            $settings = TF_CHECKOUT_Settings::GetSettings();
            if(!$arResult['ORDER_WEIGHT']){
                $ORDER_QUANTITY = 0;
                foreach($arResult['BASKET_ITEMS'] as $arItem){
                    $ORDER_QUANTITY += $arItem['QUANTITY'];
                }
            }
            $arDelivery = explode(':',$arUserResult['DELIVERY_ID']);
            if($arDelivery[1] == 'pvz' || $arDelivery[1]=='postamat'){
                $currency = CSaleLang::GetLangCurrency(SITE_ID);
                $arOrder = array(
                    "WEIGHT" => ($arResult['ORDER_WEIGHT'])?floatval($arResult['ORDER_WEIGHT'])/1000:$settings['TF_CHECKOUT_DEFAULT_WEIGHT']/1000*$ORDER_QUANTITY, // вес заказа в граммах
                    "PRICE" => $arResult['ORDER_PRICE'], // стоимость заказа в базовой валюте магазина
                    "LOCATION_FROM" => COption::GetOptionInt('sale', 'location'), // местоположение магазина
                    "LOCATION_TO" => $arUserResult['DELIVERY_LOCATION'], // местоположение доставки
                );
                $arReturn = CSaleDeliveryHandler::CalculateFull($arDelivery[0], $arDelivery[1], $arOrder, $currency);
                $arTransit = explode('<span', $arReturn['TRANSIT']);
                $strTransit = '<b>'.$arTransit[0].'<span '.$arTransit[1].'</b><span '.$arTransit[2];
                $arResult['DELIVERY'][$arDelivery[0]]['PROFILES'][$arDelivery[1]]['DESCRIPTION'] .= '<span class="checked_checkout_delivery" data-type="'.$arDelivery[1].'">'.GetMessage('DELIVERY_TIME_NAME').$strTransit.'</span>';
            }
        }

        function getTicket() {
            $settings = TF_CHECKOUT_Settings::GetSettings();
            $cache_id = md5($settings['TF_CHECKOUT_KEY']);
            $obCache = new CPHPCache();
            if ($obCache->InitCache(1800, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                return $vars['TICKET'];
            } else {
                $tuCurl = curl_init();
                curl_setopt($tuCurl, CURLOPT_URL, "http://platform.checkout.ru/service/login/ticket/" . $settings['TF_CHECKOUT_KEY']);
                curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
                curl_setopt($tuCurl, CURLOPT_HEADER, 0);
                curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
                $tuData = curl_exec($tuCurl);
                if(!curl_errno($tuCurl)){
                    $info = curl_getinfo($tuCurl);
                } else {
                    echo 'Curl error: ' . curl_error($tuCurl);
                }
                curl_close($tuCurl);
                $response = json_decode($tuData,true);
                $obCache->StartDataCache();
                $obCache->EndDataCache(array("TICKET" => $response["ticket"]));
                return $response["ticket"];
            }
        }

        function generatePlaceSelect($type, $delivery, $name, $city, $person_type) {
            $deliveries = CDeliveryCheckoutPlatform::GetDelivery();

            if(LANG_CHARSET != 'UTF-8'){
                $checkedPoint = iconv("utf-8", LANG_CHARSET, $delivery[$type]['addresses'][$_REQUEST['checkout_place_'.$type.'_address_id']]);
            }else{
                $checkedPoint = $delivery[$type]['addresses'][$_REQUEST['checkout_place_'.$type.'_address_id']];
            }


            if($_REQUEST['checkout_place_'.$type.'_address'] != $checkedPoint){
                unset($_REQUEST['checkout_place_'.$type.'_address_id']);
                unset($_REQUEST['checkout_place_'.$type.'_address']);
            }

            $select = '';
            $choice_word = GetMessage('EMPTY_SELECT_VALUE');
            $select = '<a href="javascript:;" ';
            $select .= 'class="tf-choise-points" ';
            $select .= 'data-type="'.$type.'" ';
            $select .= 'data-location="'.$city['LOCATION'].'" ';
            $select .= 'data-text="'.$choice_word.' '.$name.'">'.$choice_word.' '.$name.'</a>';

            $select .= "<ul class=\"regions_list ".$type."\" style=\"display:none;\">";
            foreach ($delivery[$type]['addresses'] as $key=>$address) {
                if(LANG_CHARSET != 'UTF-8'){
                    $address = iconv("utf-8", LANG_CHARSET, $address);
                    $delivery[$type]['additionalInfo'][$key] = iconv("utf-8", LANG_CHARSET, $delivery[$type]['additionalInfo'][$key]);
                }
                $select .= '<li class="'.$type.'_select_'.$key.'">';
                $select .= '<a class="tf-choise-point" data-location="'.$city['LOCATION'].'" ';
                $select .= 'data-latitudes="'.$delivery[$type]['latitudes'][$key].'" ';
                $select .= 'data-longitudes="'.$delivery[$type]['longitudes'][$key].'" ';
                $select .= 'data-id-point="'.$key.'" ';
                $select .= 'data-type="'.$type.'" ';
                $select .= 'data-image-point="'.$deliveries[$delivery[$type]['deliveries'][$key]]['IMAGE'].'" ';
                $select .= 'data-name-point="'.htmlspecialcharsEx($address).'" ';
                $select .= 'data-desc-point=\''.self::strip(str_replace(Array('\r\n', '\r', '\n', PHP_EOL), PHP_EOL, str_replace('\'', '', $delivery[$type]['additionalInfo'][$key]))).'\' ';
                $select .= 'data-delivery-price-point="'.$delivery[$type]['costs'][$key].'" ';
                $select .= 'href="javascript:;">'.$address.'</a></li>';
            }
            $select .= "</ul>";

            return $select;
        }

        function GetCityID($LOCATION_ID) {
            $ticket = CDeliveryCheckoutPlatform::getTicket();
            $cache_id = md5($LOCATION_ID);
            $cache_time = 1800;
            $obCache = new CPHPCache();
            if ($cache_time > 0 && $obCache->InitCache($cache_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $city = $vars['CITY'];
            }
            if(!strlen($city['ID'])){
                $city = CSaleLocation::GetByID($LOCATION_ID);

                $locationStr = GetMessage('SUFFIX_CITY').$city['CITY_NAME_LANG'];
                if(LANG_CHARSET != 'UTF-8'){
                    $utfLocation = iconv( LANG_CHARSET, "utf-8", $locationStr);
                }else{
                    $utfLocation = $locationStr;
                }
                $request = 'http://platform.checkout.ru/service/checkout/getPlacesByQuery?ticket='.$ticket.'&place='.urlencode($utfLocation);
                $res = file_get_contents($request);
                $return = json_decode($res, true);
                $cityID = $return['suggestions'][0]['id'];

                $res = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&geocode='.$utfLocation);
                $cityArr = json_decode($res, true);
                $cityLocation = explode(' ', $cityArr['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);

                $city = Array(
                    'ID' => $cityID,
                    'LOCATION' => $cityLocation[1].', '.$cityLocation[0]
                );

                $obCache->StartDataCache();
                $obCache->EndDataCache(array("CITY" => $city));
            }

            return $city;
        }

        function __GetLocationPrice($LOCATION_ID, $arConfig, $arOrder) {
            if(empty($_POST) && $_SERVER['SCRIPT_NAME']!='/bitrix/admin/sale_order_detail.php' && $_SERVER['SCRIPT_NAME']!='/bitrix/admin/sale_order_new.php'){
                include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/twofingers.checkout/classes/TF_frontend.php');
            }
            $settings = TF_CHECKOUT_Settings::GetSettings();
            $exSet = TF_CHECKOUT_Settings::GetSettingsEx();
            $ticket = CDeliveryCheckoutPlatform::getTicket();
            $city = CDeliveryCheckoutPlatform::GetCityID($LOCATION_ID);
            $itemsCount = 0;

            $dbBasketItems = CSaleBasket::GetList(
                array("NAME" => "ASC","ID" => "ASC"),
                array("FUSER_ID" => CSaleBasket::GetBasketUserID(),"LID" => SITE_ID,"ORDER_ID" => "NULL"),
                false,
                false,
                array()
            );
            while($items = $dbBasketItems->GetNext()){
                $orderWeight += $items['QUANTITY'];
                $itemsCount++;
            }

            $TotalWeight = ($arOrder['WEIGHT']>0)?floatval($arOrder['WEIGHT'])/1000:$settings['TF_CHECKOUT_DEFAULT_WEIGHT']/1000*$orderWeight;

            $delivery = self::RequestPricePoints($ticket, $city['ID'], intval($arOrder['PRICE']), intval($arOrder['PRICE']), $TotalWeight, $itemsCount);

            $deliveries = CDeliveryCheckoutPlatform::GetDelivery();
            if (!in_array($_REQUEST['PAY_SYSTEM_ID'], $settings['TF_CHECKOUT_PAY_SYSTEMS'])) {
                $delivery = self::RequestPricePoints($ticket, $city['ID'], 0, intval($arOrder['PRICE']), $TotalWeight, $itemsCount);
//                foreach ($delivery as $type => $params) {
//                    foreach ($deliveries as $id => $values) {
//                        $margin = $arOrder['PRICE']/100*$values['MARGIN'];
//                        if ($margin < $deliveryParams['MIN_MARGIN'])
//                            $margin = $deliveryParams['MIN_MARGIN'];
//                        $deliveries[$id]['MARGIN_PRICE'] = round($margin);
//                    }
//                    if (is_array($delivery[$type])) {
//                        $delivery[$type]['cost'] = $params['cost'] - $deliveries[$params['deliveries'][$params['indexOfTheCheapestTerminal']]]['MARGIN_PRICE'];
//                        if ($type == 'express') {
//                            $delivery[$type]['cost'] = $params['cost'] - $deliveries[$params['deliveryId']]['MARGIN_PRICE'];
//                        }
//                        foreach ($params['costs'] as $key => $cost) {
//                            $delivery[$type]['costs'][$key] = $cost - $deliveries[$params['deliveries'][$key]]['MARGIN_PRICE'];
//                        }
//                    }
//                }
            }

            $return = Array();

            if (isset($delivery['express']['cost'])){
                $return['express'] = Array(
                    'cost' => $delivery['express']['cost'],
                    'from' => $delivery['express']['minDeliveryTerm'],
                    'to' => $delivery['express']['maxDeliveryTerm'],
                );
            }

            if (isset($delivery['mail']['cost'])){
                $return['mail'] = Array(
                    'cost' => $delivery['mail']['cost'],
                    'from' => $delivery['mail']['minDeliveryTerm'],
                    'to' => $delivery['mail']['maxDeliveryTerm'],
                );
            }

            if (isset($delivery['postamat']['cost'])) {
                $select = CDeliveryCheckoutPlatform::generatePlaceSelect('postamat', $delivery, GetMessage('TF_POSTAMAT_NAME'), $city);
                $return['postamat'] = Array(
                    'cost' => $delivery['postamat']['cost'],
                    'from' => $delivery['postamat']['minDeliveryTerm'],
                    'to' => $delivery['postamat']['maxDeliveryTerm'],
                    'select' => $select
                );
                if (isset($_REQUEST['checkout_place_postamat_address_id']) && $_REQUEST['checkout_place_postamat_address_id'] != ""){
                    $return['postamat']['cost'] = $delivery['postamat']['costs'][$_REQUEST['checkout_place_postamat_address_id']];
                }
            }

            if (isset($delivery['pvz']['cost'])) {
                $select = CDeliveryCheckoutPlatform::generatePlaceSelect('pvz', $delivery, GetMessage('TF_PVZ_NAME'), $city);

                $return['pvz'] = Array(
                    'cost' => $delivery['pvz']['cost'],
                    'from' => $delivery['pvz']['minDeliveryTerm'],
                    'to' => $delivery['pvz']['maxDeliveryTerm'],
                    'select' => $select
                );
                if (isset($_REQUEST['checkout_place_pvz_address_id']) && $_REQUEST['checkout_place_pvz_address_id'] != ""){
                    $return['pvz']['cost'] = $delivery['pvz']['costs'][$_REQUEST['checkout_place_pvz_address_id']];
                }
            }


            return $return;
        }

        function Compability($arOrder, $arConfig) {
            $res = CDeliveryCheckoutPlatform::__GetLocationPrice($arOrder["LOCATION_TO"], $arConfig, $arOrder);
            $deliveries = array_keys($res);
            return $deliveries;
        }

        function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false) {
            $arResult = CDeliveryCheckoutPlatform::__GetLocationPrice($arOrder["LOCATION_TO"], $arConfig, $arOrder);
            $return = array(
                "RESULT" => "OK",
                "VALUE" => $arResult[$profile]['cost'],
                'TRANSIT' => $arResult[$profile]['from'].' - '.$arResult[$profile]['to'].'<span class="checkout_block checkout_place_'.$profile.'_address">'.$_REQUEST['checkout_place_'.$profile.'_address'].'<input type="hidden" class="checkout_place_'.$profile.'_address checkout_input_address" value="'.$_REQUEST['checkout_place_'.$profile.'_address'].'" name="checkout_place_'.$profile.'_address"><input type="hidden" class="checkout_place_'.$profile.'_address_id checkout_input_address_id" value="'.$_REQUEST['checkout_place_'.$profile.'_address_id'].'" name="checkout_place_'.$profile.'_address_id"><span class="checkout_place">'.$arResult[$profile]['select'].'</span></span>',
                'TRANSIT_CHECKOUT' => GetMessage('TRANSIT_CHECKOUT_FROM').$arResult[$profile]['from'].GetMessage('TRANSIT_CHECKOUT_TO').$arResult[$profile]['to'],
            );
            return $return;
        }

        function RequestPricePoints($Ticket, $CityId, $TotalSum, $assessedSum, $TotalWeight, $ItemsCount){
            $request = Array(
                'ticket' => $Ticket,
                'placeId' => $CityId,
                'totalSum' => $TotalSum,
                'assessedSum' => $assessedSum,
                'totalWeight' => $TotalWeight,
                'itemsCount' => $ItemsCount,
            );
            
            $cache_id = md5(implode('|',  $request));
            $obCache = new CPHPCache();
            if ($obCache->InitCache(1800, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $delivery = $vars['DELIVERY'];
            } else {
                $params = http_build_query($request);
                $request = 'http://platform.checkout.ru/service/checkout/calculation?'.$params;
//                echo '<pre>'; print_r($request); echo '</pre>';
                $res = file_get_contents($request);
                $delivery = json_decode($res, true);
                $obCache->StartDataCache();
                $obCache->EndDataCache(array("DELIVERY" => $delivery));
            }
            return $delivery;
        }

    }
}