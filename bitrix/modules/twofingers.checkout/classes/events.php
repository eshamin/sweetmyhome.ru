<?
if (!class_exists('TF_CHECKOUT_Events')) {
    class TF_CHECKOUT_Events {
        public static $ENDPOINT = "http://platform.checkout.ru";
        function sendData($ORDER_ID, $arFields, $update = false) {
            CModule::IncludeModule("catalog");
            CModule::IncludeModule("sale");
            CModule::IncludeModule("iblock");
            CModule::IncludeModule("twofingers.checkout");
            $settings = TF_CHECKOUT_Settings::GetSettings();
            $arOrder = CSaleOrder::GetByID($ORDER_ID);
            $dbBasketItems = CSaleBasket::GetList(array("NAME" => "ASC", "ID" => "ASC"), array("ORDER_ID" => $ORDER_ID), false, false, false);

            //d($arOrder);
//			Log::write('ORDER_ID:' . $ORDER_ID);
//			Log::write('ORDER: ');
            ob_start();
//			print_r ($arOrder);
            $value = ob_get_contents();
            ob_end_clean();
//			Log::write($value);

            $arProps = array();
            $arPropKeys = array();
            foreach($settings['TF_CHECKOUT_PERSON_PROPS'][$arOrder['PERSON_TYPE_ID']] as $code => $propId){
                $arPropKeys[$propId] = $code;
            }

            $db_props = CSaleOrderPropsValue::GetOrderProps($arOrder['ID']);
            while ($arProp = $db_props->Fetch()){
                if($arPropKeys[$arProp['ORDER_PROPS_ID']]!=""){
                    $arProps[$arPropKeys[$arProp['ORDER_PROPS_ID']]] = $arProp['VALUE'];
                }
                if ($arProp['IS_LOCATION'] == 'Y') {
                    $locationHashID = $arProp['VALUE'];
                }
            }

//			Log::write('PROPS: ');
            ob_start();
//			print_r ($props);
            $value = ob_get_contents();
            ob_end_clean();
//			Log::write($value);

            if(method_exists('CSaleLocation','isLocationProMigrated')){
                if(CSaleLocation::isLocationProMigrated()){
                    $locID = CSaleLocation::getLocationIDbyCODE($locationHashID);
                }
            }else{
                $locID = $locationHashID;
            }

            $arVal = CSaleLocation::GetByID($locID, LANG);

            $locationID = $arVal['ID'];

            $cache_id = md5($locationID);
            $cache_time = 1800;
            $obCache = new CPHPCache();
            if ($cache_time > 0 && $obCache->InitCache($cache_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $city = $vars['CITY'];
            }
            if(!strlen($city['ID'])){
                $city = CDeliveryCheckoutPlatform::GetCityID($locationID);
            }

            $ticket = CDeliveryCheckoutPlatform::getTicket();
            if (strpos($arOrder['DELIVERY_ID'], 'CheckoutPlatform') !== false){
//				print $props['ADDRESS']['VALUE'];
                preg_match("/:(.*)\[(.*)\]/U", $arProps['ADDRESS'], $pp_arr);

                while ($item = $dbBasketItems->Fetch()) {
                    if(LANG_CHARSET != 'UTF-8'){
                        $item['NAME'] = iconv(LANG_CHARSET, "utf-8", $item['NAME']);
                    }
                    $items[] = Array(
                        'name' => $item['NAME'],
                        'code' => $item['ID'],
                        'variantCode' => null,
                        'quantity' => intval($item['QUANTITY']),
                        'assessedCost' => $item['PRICE'],
                        'payCost' => in_array($arOrder['PAY_SYSTEM_ID'], $settings['TF_CHECKOUT_PAY_SYSTEMS'])?intval($item['PRICE']):0,
                        'weight' => $item['WEIGHT']>0?$item['WEIGHT']/1000:$settings['TF_CHECKOUT_DEFAULT_WEIGHT']/1000,
                    );
                    $ORDER_WEIGHT += $item['WEIGHT']>0?$item['WEIGHT']/1000*$item['QUANTITY']:$settings['TF_CHECKOUT_DEFAULT_WEIGHT']/1000*$item['QUANTITY'];
                }

                $request = Array(
                    'ticket' => $ticket,
                    'placeId' => $city['ID'],
                    'totalSum' => in_array($arOrder['PAY_SYSTEM_ID'], $settings['TF_CHECKOUT_PAY_SYSTEMS'])?intval($arOrder['PRICE']):0,
                    'assessedSum' => intval($arOrder['PRICE']),
                    'totalWeight' => $ORDER_WEIGHT,
                    'itemsCount' => count($items),
                );

                $cache_id = md5(implode('|',  $request));
                $obCache = new CPHPCache();
                if ($obCache->InitCache(3600, $cache_id, "/")) {
                    $vars = $obCache->GetVars();
                    $delivery = $vars['DELIVERY'];
                } else {
                    $params = http_build_query($request);
                    $request = 'http://platform.checkout.ru/service/checkout/calculation?'.$params;
                    //print $request;
                    $res = file_get_contents($request);
                    $delivery = json_decode($res, true);
                    $obCache->StartDataCache();
                    $obCache->EndDataCache(array("DELIVERY" => $delivery));
                }

                if(LANG_CHARSET != 'UTF-8'){
                    foreach($arProps as $code => $val){
                        $arProps[$code] = iconv( LANG_CHARSET, "utf-8", $val);
                    }
                    $pp_arr[1] = iconv(LANG_CHARSET, "utf-8", $pp_arr[1]);
                }


                $ID_ORDER_CHECKOUT = ($settings['TF_CHECKOUT_KEY'] == 'oQZMgKL7m75NUVb1W3O5LR0L') ? $arOrder['ACCOUNT_NUMBER'].'-BTRX-'. substr(md5(date("Y-m-d")), 10, 6) : $arOrder['ACCOUNT_NUMBER'];
                $type = str_replace('CheckoutPlatform:', '', $arOrder['DELIVERY_ID']);
                $data = Array(
                    'apiKey' => $settings['TF_CHECKOUT_KEY'],
                    'order' => Array(
                        'goods' => $items,
                        'delivery' => Array(
                            'deliveryId' => ($type=='express' || $type=='mail')?$delivery[$type]['deliveryId']:$delivery[$type]['deliveries'][$pp_arr[2]],
                            'placeFiasId' => $city['ID'],
                            'courierOptions' => Array(

                            ),
                            'addressExpress' => Array(
                                'postindex' => '',
                                'street' => ($type=='express' || $type=='mail')?$arProps['ADDRESS']:'',
                                'house' => ($type=='express' || $type=='mail')?'-':'',
                                'housing' => '',
                                'building' => '',
                                'appartment' => '',
                            ),
                            'addressPvz' => trim($pp_arr[1]),
                            'type' => $type,
                            'cost' => $arOrder['PRICE_DELIVERY'],
                            'minTerm' => ($type=='express' || $type=='mail')?$delivery[$type]['minDeliveryTerm']:$delivery[$type]['minTerms'][$pp_arr[2]],
                            'maxTerm' => ($type=='express' || $type=='mail')?$delivery[$type]['maxDeliveryTerm']:$delivery[$type]['maxTerms'][$pp_arr[2]],
                        ),
                        'user' => Array(
                            'fullname' => $arProps['NAME'],
                            'email' => $arProps['EMAIL'],
                            'phone' => preg_replace("/\D/","",$arProps['PHONE']),
                        ),
                        'comment' => '',
                        'shopOrderId' => $ID_ORDER_CHECKOUT,
                        'paymentMethod' => in_array($arOrder['PAY_SYSTEM_ID'], $settings['TF_CHECKOUT_PAY_SYSTEMS'])?'cash':'prepay',
                        'forcedCost' => $arOrder['PRICE_DELIVERY'],
                    ),
                );
                $request_path = '/service/order/create';
                if($update){
                    $result_request = file_get_contents(self::$ENDPOINT.'/service/order/statushistory/'.$ID_ORDER_CHECKOUT.'?apiKey='.$settings['TF_CHECKOUT_KEY']);
                    $arResultInfoOrder = json_decode($result_request, true);
                    if($arResultInfoOrder['order']['id']){
                        $request_path = '/service/order/'.$arResultInfoOrder['order']['id'];
                    }
                }
                $res_send = TF_CHECKOUT_Events::postData($request_path, $data);
            }
        }
        function updateData($ORDER_ID, $arFields) {
            self::sendData($ORDER_ID, array(), true);
        }
        function StatusCheck($ID, $arOrder, $arParams){
            $settings = TF_CHECKOUT_Settings::GetSettings();
            if($arOrder['STATUS_ID']==$settings['TF_order_status_send']){
                self::sendData($ID, array());
            }
        }
        function postData($url, $post) {
            $fields = array();
            $post_data = json_encode($post);
            $tuCurl = curl_init();
            curl_setopt($tuCurl,CURLOPT_URL, self::$ENDPOINT . $url);
            curl_setopt($tuCurl,CURLOPT_POST, count($fields));
            curl_setopt($tuCurl,CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($tuCurl, CURLOPT_VERBOSE, false);
            curl_setopt($tuCurl, CURLOPT_HEADER, false);
            curl_setopt($tuCurl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($tuCurl);
            curl_close($tuCurl);
            return $result;
        }
        function StatusChange($ID, $val){
            $settings = TF_CHECKOUT_Settings::GetSettings();
            if($val==$settings['TF_order_status_send']){
                self::sendData($ID, array());
            }
        }
    }
}
?>