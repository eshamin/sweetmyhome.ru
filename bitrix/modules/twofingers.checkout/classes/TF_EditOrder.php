<?
    CJSCore::Init(array('jquery'));
    $settings = TF_CHECKOUT_Settings::GetSettings();
    $address_prop_keys = 'address_prop_keys = {';
    $i = 0;
    foreach($settings['TF_CHECKOUT_PERSON_PROPS'] as $k => $val){
        $i++;
        if($val['ADDRESS']=='empty'){
            $idProp = 0;
        }else{
            $idProp = $val['ADDRESS'];
        }
        $address_prop_keys .= $k.':'.$idProp;
        if(count($settings['TF_CHECKOUT_PERSON_PROPS'])!=$i){
            $address_prop_keys .= ',';
        }
    }
    $address_prop_keys .= '}';
    $GLOBALS['APPLICATION']->AddHeadString('<script type="text/javascript">'.$address_prop_keys.';</script>');
    $mess = array(
        'tf_postamat_name' => GetMessage('TF_POSTAMAT_NAME'),
        'tf_pvz_name' => GetMessage('TF_PVZ_NAME'),
        'tf_delivery_to' => GetMessage('TF_DELIVERY_TO'),
        'tf_delivery_cost' => GetMessage('TF_DELIVERY_COST'),
        'tf_delivery_point_choice' => GetMessage('TF_DELIVERY_POINT_CHOICE'),
    );
    $str = '{';
    foreach($mess as $k => $v){
        $str .= '"'.$k.'":"'.$v.'"';
        if($k != 'tf_delivery_point_choice'){
            $str .= ',';
        }
    }
    $str .= '}';
    $GLOBALS['APPLICATION']->AddHeadString('<script type="text/javascript">BX.message('.$str.');</script>');
    if (COption::GetOptionString('twofingers.checkout', 'TF_ymaps_include') == 'Y') {
        if($_SERVER['REQUEST_SCHEME']!='http'){
            $protocol = 'https';
        }else{
            $protocol = 'http';
        }
        $GLOBALS['APPLICATION']->AddHeadString(
            '<script src="'.$protocol.'://api-maps.yandex.ru/2.1/?lang=ru-RU&load=package.full"></script>'
        );
    }
    if (intval(SM_VERSION) < 15) {
        $GLOBALS['APPLICATION']->AddHeadScript('/bitrix/js/twofingers.checkout/script_admin.js');
    } else {
        $asset = Bitrix\Main\Page\Asset::getInstance();
        $asset->addJs('/bitrix/js/twofingers.checkout/script_admin.js');
    }
    ?>
    <script>global_order_id=<?=$_REQUEST['ID']?></script>
    <div class="TF-box-modal-point"><a onclick="TF_CloseModal(); return false;"
                                       class="TF-box-modal-close" href="javascript:;"></a>

        <div class="locations_select_wrapper">
            <div class="locations_select"><span
                    class="h2"></span></div>
        </div>
        <div style="background:#dddddd; height:1px;"></div>
        <div class="scrollpane"></div>
        <div id="YAmap"></div>
    </div>
    <style>
        .TF-box-modal-point{
            display:none;
            border-radius: 10px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            position: fixed;
            background-color: #fff;
            z-index: 1001;
            padding: 0 0 10px 0;
            width: 75%;
            height: 75%;
            left: 12%;
            top: 12%;
            overflow: hidden;
        }

        .TF-points-popup-blur{
            -webkit-filter: blur(3px);
            -ms-filter: blur(3px);
            -moz-filter: blur(3px);
            -o-filter: blur(3px);
            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='blur'><feGaussianBlur stdDeviation='3'/></filter></svg>#blur");
        }

        .TF-points-popup-overlay{
            display: none;
            position: fixed;
            margin: 0;
            padding: 0;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            background: url('/bitrix/images/twofingers.checkout/pop-up/custom-popup-overlay.png') repeat;
            z-index: 1000;
        }

        .TF-box-modal-close{
            display: block;
            position: absolute;
            right: 20px;
            top: 20px;
            width: 9px;
            height: 9px;
            cursor: pointer;
            background: url('/bitrix/images/twofingers.checkout/pop-up/custom-popup-close.png') no-repeat;
        }

        .locations_select span.h2{
            font-size:30px;
            line-height: 40px;
            margin:5px 20px;
            display: block;
        }

        .scrollpane{
            height: 85%;
            overflow-y: scroll;
            padding:10px;
            width:27%;
            float:left;
        }

        #YAmap{
            width:70%;
            height:85%;
            float:left;
            display:block;
            padding-top: 10px;
        }

        span.checkout_block,
        span.checkout_block span,
        .checked_checkout_delivery{
            display:block;
        }

        .regions_list{
            margin:0;
            padding:0;
        }

        .regions_list li{
            list-style: none;
        }

        .regions_list li:before {
            content: '\2014\a0';
        }

        .regions_list a{
            font-size: 12px;
            color:#1e98ff;
        }

        .regions_list li.selected a{
            color:#000;
        }

        .postamat_info img{
            width:150px;
        }
        p span.checked_checkout_delivery{
            text-align: right;
            color:#000;
            margin-top: -10px;
        }
        p span.checked_checkout_delivery a{
            color:#12a9e1;
        }
    </style>