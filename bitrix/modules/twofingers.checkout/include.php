<? 
CModule::AddAutoloadClasses(
    'twofingers.checkout',
    array(
        'TF_CHECKOUT_Settings' => 'classes/settings.php',
        'TF_CHECKOUT_Events' => 'classes/events.php',
        'CDeliveryCheckoutPlatform' => 'classes/platform.php',
        'TF_CHECKOUT_admin'=>'classes/admin_classes.php'
   )
);
?>