BX.addCustomEvent('onAjaxSuccess', GetSelectstoChoise);

$(document).ready(function(){
    TF_collection=[];
    TF_balloon_objects=[];
    GetSelectstoChoise();
    $('.TF-box-modal-point').on('click', '.tf-choise-point', function(e){
        e.preventDefault();
        $(this).parent().addClass('selected').siblings().removeClass('selected');
        var type = $(this).attr('data-type');
        var point_id = $(this).attr('data-id-point');
        var location = $(this).attr('data-location');
        var payment_id = $('#order_edit_info_form').find('#PAY_SYSTEM_ID option:selected').val()
        TF_map.panTo([[$(this).attr('data-latitudes'), $(this).attr('data-longitudes')]], {flying:true, duration:2000}).then(function(){
            TF_map.setZoom(15);
            TF_balloon_objects[type+'_'+location+'_'+point_id+'_'+payment_id].balloon.open()
        });
    });

//    $('#order_edit_info_form').on('change', '#DELIVERY_ID, #PAY_SYSTEM_ID', function(){
//        $('#order_edit_info_form').find('#point_block').html('');
//    });

    $('#order_edit_info_form').on('click', '.tf-choise-points', function(){
        TF_ShowPointModal($(this).attr('data-type'), $(this).attr('data-location'), $(this).attr('data-text'));
    });

    $(".TF-box-modal-point").on('click', '.check_point', function(){
        var type_delivery = $(this).data('delivery-type');
        var price = $(this).data('delivery-price');
        var address = $(this).closest('.'+type_delivery+'_info').find('b.name').text();
        var data_id = $(this).closest('.'+type_delivery+'_info').data('id');
        var text = BX.message('tf_delivery_to')+address+' ['+data_id+']';
        $('input.checkout_place_'+type_delivery+'_address').val(address);
        $('input.checkout_place_'+type_delivery+'_address_id').val(data_id);
        $.each(address_prop_keys, function(index, val){
            if($('#ORDER_PROP_'+val).length>0){
                $('#ORDER_PROP_'+val).val(text);
            }
        });
        $('#DELIVERY_ID_PRICE').val(price);
        TF_CloseModal();
    });
});

function GetOrderProps(){
    var selectedDelivery = $('#DELIVERY_ID option:selected').val();
    var arDeliverySelected = selectedDelivery.split(':');
    var selectedPayment = $('#PAY_SYSTEM_ID option:selected').val();
    var BuyerTypeID = $('#buyer_type_id option:selected').val();
    var OrderObj = {
        delivery:arDeliverySelected[0],
        delyveryProfile:arDeliverySelected[1],
        payment_id:selectedPayment,
        order_id:global_order_id,
        BuyerTypeID:BuyerTypeID
    };
    return OrderObj;
}

function GetSelectstoChoise(){
    $('#order_edit_info_form').find('#point_block').remove();
    $('#DELIVERY_ID').after('<div id="point_block"></div>');
    var OrderObj = GetOrderProps();
    if(OrderObj.delyveryProfile == 'pvz' || OrderObj.delyveryProfile == 'postamat'){
        OrderObj.getPostomatLinks = "Y";
        $.get('/bitrix/tools/twofingers.checkout/ajax_checkout.php',
            OrderObj,
            function(res){
                $('#order_edit_info_form').find('#point_block').html(res);
            }
        );
    }
}

function TF_ShowPointModal(type, location, modal_name){
    $('.regions_list li').removeClass('selected');
    $('.locations_select span.h2').html(modal_name);
    list = $('.regions_list.'+type).clone();
    $('.scrollpane').html(list.show());
    TF_OverlayCreate();
    $('.TF-box-modal-point').show('fast', function(){
        ArLoc = location.split(',');
        var payment_id = $('#order_edit_info_form').find('#PAY_SYSTEM_ID option:selected').val();
        console.log(payment_id);
        if(typeof TF_map !== 'undefined'){
            TF_map.setCenter(ArLoc, 10);
            var Colection = TF_GenerateCollection(type, location, payment_id);
            TF_BaloonsAdd(Colection);
        }else{
            ymaps.ready(function(){
                var Colection = TF_GenerateCollection(type, location, payment_id);
                TF_Map_Init(ArLoc, Colection);
            })
        }
    });
}

function TF_Map_Init(location, collection){
    TF_map = new ymaps.Map(
        'YAmap',
        {
            center:location,
            zoom:10,
            controls: ['zoomControl']
        }
    );
    TF_BaloonsAdd(collection);
};

function TF_GenerateCollection(type, location, payment_id){
    if(typeof TF_collection[type+'_'+location+'_'+payment_id] !="undefined"){
        return TF_collection[type+'_'+location+'_'+payment_id];
    }
    TF_collection[type+'_'+location+'_'+payment_id] = new ymaps.Clusterer({ clusterDisableClickZoom: true });
    $('.TF-box-modal-point .regions_list.'+type+' a').each(function(index, el){
        var id = $(el).attr('data-id-point');
        var name = $(el).text();
        var price = $(el).attr('data-delivery-price-point');
        var lat = $(el).attr('data-latitudes');
        var lon = $(el).attr('data-longitudes');
        var img_src = $(el).attr('data-image-point');
        var desc =  $(el).attr('data-desc-point');
        var desc_formated = '<div class="'+type+'_info" data-id="'+id+'">' +
            '<b class="name">'+name+'</b><img style="float:right" src="'+img_src+'"><br>' +
            '<span style="font-size:11px; line-height:12px">'+desc+'</span>' +
            '<div><b>'+BX.message('tf_delivery_cost')+price+'p.</b></div>' +
            '<div style="height:23px; padding:20px 0">' +
            '<a class="check_point select_'+type+'" data-delivery-price="'+price+'" data-delivery-type="'+type+'" href="javascript:;">' +
            BX.message('tf_delivery_point_choice')+BX.message('tf_'+type+'_name')+'</a></div>' +
            '</div>';

        var balloon_object = new ymaps.GeoObject(
            {
                geometry: {
                    type: "Point",
                    coordinates: [lat, lon]
                },
                properties: {
                    clusterCaption: name,
                    balloonContentBody: desc_formated
                }
            }
        );
        TF_balloon_objects[type+'_'+location+'_'+id+'_'+payment_id] = balloon_object;
        TF_collection[type+'_'+location+'_'+payment_id].add(balloon_object);
    });
    return TF_collection[type+'_'+location+'_'+payment_id];
}

function TF_BaloonsAdd(collection){
    TF_map.geoObjects.removeAll();
    TF_map.geoObjects.add(collection);
}

function TF_OverlayCreate(){
    $('body').append('<div onclick="TF_CloseModal()" class="TF-points-popup-overlay"></div>');
    $('.TF-points-popup-overlay').show();
}

function TF_OverlayDesctroy(){
    list.remove();
    $('.TF-points-popup-overlay').remove();
}

function TF_CloseModal(){
    TF_OverlayDesctroy();
    $('body').find('.TF-box-modal-point').hide();
}