<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('twofingers.checkout');
$settings = TF_CHECKOUT_Settings::GetSettings();

if($_REQUEST['getPostomatLinks']=='Y' && intval($_REQUEST['order_id'])){
    $arOrder = CSaleOrder::GetByID($_REQUEST['order_id']);
    $arOrder['PRICE'] = $arOrder['PRICE']-$arOrder['PRICE_DELIVERY'];
    $resLocationPropId = CSaleOrderProps::GetList(
        array(),
        array('PERSON_TYPE_ID'=>intval($_REQUEST['BuyerTypeID']), "TYPE"=>"LOCATION", "IS_LOCATION"=>'Y'),
        false,
        false,
        array()
    );
    if($arLocationPropID = $resLocationPropId->GetNext()){
        $resProp = CSaleOrderPropsValue::GetList(array(), array("ORDER_ID"=>$_REQUEST['order_id'], 'ORDER_PROPS_ID'=>$arLocationPropID['ID']));
        if($arProp = $resProp->GetNext()){
            if(method_exists('CSaleLocation','isLocationProMigrated')){
                if(CSaleLocation::isLocationProMigrated()){
                    $locID = CSaleLocation::getLocationIDbyCODE($arProp['VALUE']);
                }
            }else{
                $locID = $arProp['VALUE'];
            }
            $arOrder['LOCATION_TO'] = $locID;
            $select = TF_CHECKOUT_admin::GetLinkPointsToShowModal($_REQUEST['delyveryProfile'], array(), $arOrder, $_REQUEST['order_id']);
            echo $select;
        }
    }
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>