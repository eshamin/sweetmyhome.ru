<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

class twofingers_checkout extends CModule{
    var $MODULE_ID = 'twofingers.checkout';
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;

    function twofingers_checkout(){
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = GetMessage('INSTALL_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('INSTALL_DESCRIPTION');
        $this->PARTNER_NAME = GetMessage("PARTNER");
        $this->PARTNER_URI = GetMessage("PARTNER_URI");
    }

    function InstallDB()
    {
        return true;
    }


    function UnInstallDB()
    {
        return true;
    }

    function InstallEvents(){
        RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepDelivery", $this->MODULE_ID, "CDeliveryCheckoutPlatform", "CheckedDeliveryCalculate");
        RegisterModuleDependences("sale", "onSaleDeliveryHandlersBuildList", $this->MODULE_ID, "CDeliveryCheckoutPlatform", "Init");
        RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepFinal", $this->MODULE_ID, "TF_CHECKOUT_Events", "StatusCheck");
        RegisterModuleDependences("sale", "OnOrderUpdate", $this->MODULE_ID, "TF_CHECKOUT_Events", "updateData");
        RegisterModuleDependences("sale", "OnSaleStatusOrder", $this->MODULE_ID, "TF_CHECKOUT_Events", "StatusChange");
        RegisterModuleDependences("sale", "onSaleDeliveryHandlerCalculate", $this->MODULE_ID, "TF_CHECKOUT_admin", "CalculateDeliveryOnEditPageAdmin");

        return true;
    }

    function UnInstallEvents(){
        CModule::IncludeModule('sale');
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepDelivery", $this->MODULE_ID, "CDeliveryCheckoutPlatform", "CheckedDeliveryCalculate");
        UnRegisterModuleDependences("sale", "onSaleDeliveryHandlersBuildList", $this->MODULE_ID, "CDeliveryCheckoutPlatform", "Init");
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepFinal", $this->MODULE_ID, "TF_CHECKOUT_Events", "StatusCheck");
        UnRegisterModuleDependences("sale", "OnOrderUpdate", $this->MODULE_ID, "TF_CHECKOUT_Events", "updateData");
        UnRegisterModuleDependences("sale", "OnSaleStatusOrder", $this->MODULE_ID, "TF_CHECKOUT_Events", "StatusChange");
        UnRegisterModuleDependences("sale", "onSaleDeliveryHandlerCalculate", $this->MODULE_ID, "TF_CHECKOUT_admin", "CalculateDeliveryOnEditPageAdmin");
        CSaleDeliveryHandler::Reset('CheckoutPlatform');
        return true;
    }

    function InstallFiles(){
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/js/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js/".$this->MODULE_ID, true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/images/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/".$this->MODULE_ID, true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/ajax/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools/".$this->MODULE_ID, true, true);
        return true;
    }

    function UnInstallFiles(){
        DeleteDirFilesEx("/bitrix/js/".$this->MODULE_ID);
        DeleteDirFilesEx("/bitrix/images/".$this->MODULE_ID);
        DeleteDirFilesEx("/bitrix/tools/".$this->MODULE_ID);
        return true;
    }

    function DoInstall(){

        global $DB, $DBType, $APPLICATION;
        $this->errors = false;

        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallFiles();

        RegisterModule($this->MODULE_ID);

//        \Bitrix\Main\Loader::includeModule('sale');
//
//        $db_ptype = CSalePersonType::GetList($arOrder, Array("ACTIVE"=>"Y"));
//        while ($ptype = $db_ptype->Fetch())
//        {
//            $arPersonTypeIDs[] = $ptype['ID'];
//            $arPersonTypeProps[$ptype['ID']] =  array('NAME'=>$ptype['NAME'], 'PROPS'=>array(), 'LIDS'=>$ptype['LIDS']);
//        }
//
//        $arSetPropsJson = COption::GetOptionString($this->MODULE_ID, "TF_CHECKOUT_PERSON_PROPS");
//
//        $arSettingProps = json_decode($arSetPropsJson, true);
//
//        $db_props = CSaleOrderProps::GetList(
//            array("SORT" => "ASC"),
//            array(
//                "PERSON_TYPE_ID" => $arPersonTypeIDs,
//                "TYPE"           => array('LOCATION')
//            ),
//            false,
//            false,
//            array('ID', 'NAME', 'PERSON_TYPE_ID')
//        );
//        while($arProps = $db_props->GetNext()){
//            $arSettingProps[$arProps['PERSON_TYPE_ID']]['LOCATION'] = $arProps['ID'];
//        }

        $settings = Array(
            'TF_CHECKOUT_DEFAULT_WEIGHT' => '200',
            'TF_ymaps_include' => 'Y',
//            'TF_CHECKOUT_PERSON_PROPS' => json_encode($arSettingProps),
        );

        foreach($settings as $key=>$value){
            COption::SetOptionString($this->MODULE_ID, $key, $value, SITE_ID);
        }

        LocalRedirect('/bitrix/admin/settings.php?lang=ru&mid=twofingers.checkout&mid_menu=1');

    }

    function DoUninstall(){

        global $DB, $DBType, $APPLICATION;
        $this->errors = false;

        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnInstallEvents();

        UnRegisterModule($this->MODULE_ID);

    }
}
?>