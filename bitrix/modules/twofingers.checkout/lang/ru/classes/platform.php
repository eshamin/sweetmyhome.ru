<?
$MESS['EMPTY_SELECT_VALUE'] = 'Выберите';
$MESS['TF_POSTAMAT_NAME'] = 'постамат';
$MESS['TF_PVZ_NAME'] = 'пункт';
$MESS['TF_DELIVERY_TO'] = 'Доставить по адресу: ';
$MESS['TF_DELIVERY_COST'] = 'Стоимость доставки: ';
$MESS['TF_DELIVERY_POINT_CHOICE'] = 'Выбрать этот ';
$MESS['COURIER_DELIVERY'] = 'Курьерская доставка';
$MESS['COURIER_DELIVERY_DESC'] = 'Срок доставки до 3 дней';
$MESS['POSTAMAT_DELIVERY'] = 'Доставка в постамат';
$MESS['POSTAMAT_DELIVERY_DESC'] = 'Срок доставки до 3 дней';
$MESS['PVZ_DELIVERY'] = 'Доставка в ПВЗ';
$MESS['PVZ_DELIVERY_DESC'] = 'Срок доставки до 3 дней';
$MESS['MAIL_DELIVERY'] = 'Доставка Почтой';
$MESS['MAIL_DELIVERY_DESC'] = 'Срок доставки до 3 дней';
$MESS['OWN_DELIVERY'] = 'Собственная доставка';
$MESS['OWN_DELIVERY_DESC'] = 'Срок доставки до 3 дней';
$MESS['SPSR_NAME'] = 'СПСР';
$MESS['SUFFIX_CITY'] = 'г. ';
$MESS['TRANSIT_CHECKOUT_FROM'] = 'от ';
$MESS['TRANSIT_CHECKOUT_TO'] = ' до ';
$MESS['DELIVERY_TIME_NAME'] = 'Срок доставки (дней): ';
?>