BX.addCustomEvent('onAjaxSuccess', afterFormReload);

function afterFormReload(){
    if($('.checked_checkout_delivery').length>0){
        var type = $('.checked_checkout_delivery').attr('data-type');
        elem = $('.checked_checkout_delivery').detach();
        $('label[for="ID_DELIVERY_CheckoutPlatform_'+type+'"]').find('.bx_result_price').append(elem);
        $.each(address_prop_keys, function(index, val){
            if($('#ORDER_PROP_'+val).length>0){
                $('#ORDER_PROP_'+val).attr('readonly', 'readonly');
            }
        });
    }
    var CheckedDel = $('#ORDER_FORM').find('input[name="DELIVERY_ID"]:checked').val().split(':');
    if(CheckedDel[0]=='CheckoutPlatform'){
        if(CheckedDel[1]=='pvz' || CheckedDel[1]=='postamat'){
            var address = $('input.checkout_place_'+CheckedDel[1]+'_address').val();
            var address_id = $('.checkout_place_'+CheckedDel[1]+'_address_id').val();
            if(address_id.length==0){
                $('#ORDER_FORM').find('input[name="DELIVERY_ID"]').removeAttr("checked");
            }
        }
    }
}

$(document).ready(function(){
    if (typeof submitForm === "function") {
        submitForm();
    }
    TF_collection=[];
    TF_balloon_objects=[];
    $(".TF-box-modal-point").on('click', '.check_point', function(){
        var type_delivery = $(this).data('delivery-type');
        var address = $(this).closest('.'+type_delivery+'_info').find('b.name').text();
        var data_id = $(this).closest('.'+type_delivery+'_info').data('id');
        var text = BX.message('tf_delivery_to')+address+' ['+data_id+']';
        $('#ID_DELIVERY_CheckoutPlatform_'+type_delivery).click();
//        $('.checkout_input_address').val('');
//        $('.checkout_input_address_id').val('');
        $('input.checkout_place_'+type_delivery+'_address').val(address);
        $('input.checkout_place_'+type_delivery+'_address_id').val(data_id);
        $.each(address_prop_keys, function(index, val){
            if($('#ORDER_PROP_'+val).length>0){
                $('#ORDER_PROP_'+val).val(text);
            }
        });
        TF_CloseModal();
    });

    $('.TF-box-modal-point').on('click', '.tf-choise-point', function(e){
        e.preventDefault();
        $(this).parent().addClass('selected').siblings().removeClass('selected');
        var type = $(this).attr('data-type');
        var point_id = $(this).attr('data-id-point');
        var location = $(this).attr('data-location');
        var payment_id = $('#ORDER_FORM').find('input[name="PAY_SYSTEM_ID"]:checked').val()
        TF_map.panTo([[$(this).attr('data-latitudes'), $(this).attr('data-longitudes')]], {flying:true, duration:2000}).then(function(){
            TF_map.setZoom(15);
            TF_balloon_objects[type+'_'+location+'_'+point_id+'_'+payment_id].balloon.open()
        });
    });

    $('#ORDER_FORM').on('click', '.tf-choise-points', function(){
        TF_ShowPointModal($(this).attr('data-type'), $(this).attr('data-location'), $(this).attr('data-text'));
    });

    $('#ORDER_FORM').on('click', '[name="DELIVERY_ID"]', function(){
        var CheckedDelVal = $(this).val().split(':');
        if(CheckedDelVal[0]=='CheckoutPlatform'){
            if(CheckedDelVal[1]=='pvz' || CheckedDelVal[1]=='postamat'){
                if($('.tf-choise-points[data-type="'+CheckedDelVal[1]+'"]').length>0){
                    var address = $('input.checkout_place_'+CheckedDelVal[1]+'_address').val();
                    var address_id = $('.checkout_place_'+CheckedDelVal[1]+'_address_id').val();
                    var inputpropid = false;
                    $.each(address_prop_keys, function(index, val){
                        if($('#ORDER_PROP_'+val).length>0){
                            $('#ORDER_PROP_'+val).removeAttr('readonly');
                            inputpropid = val;
                        }
                    });
                    if(address_id.length>0 && inputpropid!==false){
                        $('#ORDER_PROP_'+inputpropid).val(BX.message('tf_delivery_to')+address+' ['+address_id+']');
                    }
                    if(address_id.length==0 && inputpropid!==false){
                        $('.tf-choise-points[data-type="'+CheckedDelVal[1]+'"]').trigger( "click" );
                    }
                }else{
                    $('#ORDER_FORM').find('#delivery_info_CheckoutPlatform_'+CheckedDelVal[1]+' a').click();
                }
            }else{
//                $('.checkout_input_address').val('');
//                $('.checkout_input_address_id').val('');
            }
        }else{
//            $('.checkout_input_address').val('');
//            $('.checkout_input_address_id').val('');
        }
    });
})

function TF_ShowPointModal(type, location, modal_name){
    $('.regions_list li').removeClass('selected');
    $('.locations_select span.h2').html(modal_name);
    list = $('.regions_list.'+type).clone();
    $('.scrollpane').html(list.show());
    TF_OverlayCreate();
    $('.TF-box-modal-point').show('fast', function(){
        ArLoc = location.split(',');
        var payment_id = $('#ORDER_FORM').find('input[name="PAY_SYSTEM_ID"]:checked').val();
        if(typeof TF_map !== 'undefined'){
            TF_map.setCenter(ArLoc, 10);
            var Colection = TF_GenerateCollection(type, location, payment_id);
            TF_BaloonsAdd(Colection);
        }else{
            ymaps.ready(function(){
                var Colection = TF_GenerateCollection(type, location, payment_id);
                TF_Map_Init(ArLoc, Colection);
            })
        }
    });
}

function TF_Map_Init(location, collection){
    TF_map = new ymaps.Map(
        'YAmap',
        {
            center:location,
            zoom:10,
            controls: ['zoomControl']
        }
    );
    TF_BaloonsAdd(collection);
};

function TF_GenerateCollection(type, location, payment_id){
    if(typeof TF_collection[type+'_'+location+'_'+payment_id] !="undefined"){
        return TF_collection[type+'_'+location+'_'+payment_id];
    }
    TF_collection[type+'_'+location+'_'+payment_id] = new ymaps.Clusterer({ clusterDisableClickZoom: true });
    $('.TF-box-modal-point .regions_list.'+type+' a').each(function(index, el){
        var id = $(el).attr('data-id-point');
        var name = $(el).text();
        var price = $(el).attr('data-delivery-price-point');
        var lat = $(el).attr('data-latitudes');
        var lon = $(el).attr('data-longitudes');
        var img_src = $(el).attr('data-image-point');
        var desc =  $(el).attr('data-desc-point');
        var desc_formated = '<div class="'+type+'_info" data-id="'+id+'">' +
            '<b class="name">'+name+'</b><img style="float:right" src="'+img_src+'"><br>' +
            '<span style="font-size:11px; line-height:12px">'+desc+'</span>' +
            '<div><b>'+BX.message('tf_delivery_cost')+price+'p.</b></div>' +
            '<div style="height:23px; padding:20px 0">' +
            '<a class="check_point select_'+type+'" data-delivery-type="'+type+'" href="javascript:;">' +
            BX.message('tf_delivery_point_choice')+BX.message('tf_'+type+'_name')+'</a></div>' +
            '</div>';

        var balloon_object = new ymaps.GeoObject(
            {
                geometry: {
                    type: "Point",
                    coordinates: [lat, lon]
                },
                properties: {
                    clusterCaption: name,
                    balloonContentBody: desc_formated
                }
            }
        );
        TF_balloon_objects[type+'_'+location+'_'+id+'_'+payment_id] = balloon_object;
        TF_collection[type+'_'+location+'_'+payment_id].add(balloon_object);
    });
    return TF_collection[type+'_'+location+'_'+payment_id];
}

function TF_BaloonsAdd(collection){
    TF_map.geoObjects.removeAll();
    TF_map.geoObjects.add(collection);
}

function TF_OverlayCreate(){
    $('body').append('<div onclick="TF_CloseModal()" class="TF-points-popup-overlay"></div>');
    $('.TF-points-popup-overlay').show();
}

function TF_OverlayDesctroy(){
    list.remove();
    $('.TF-points-popup-overlay').remove();
}

function TF_CloseModal(){
    TF_OverlayDesctroy();
    $('body').find('.TF-box-modal-point').hide();
}